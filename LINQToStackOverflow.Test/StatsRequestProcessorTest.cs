﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class StatsRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidValueQueryResponse = @"{""statistics"":[{""total_questions"":631769,""total_unanswered"":94038,""total_answers"":1814922,""total_comments"":2497378,""total_votes"":6341597,""total_badges"":647133,""total_users"":222406,""questions_per_minute"":1.62,""answers_per_minute"":3.40,""badges_per_minute"":1.31,""api_version"":{""version"":""0.7"",""revision"":""1.0.1234.5678""}}]}";

        [TestMethod]
        public void BuildURLTest()
        {
            StatsRequestProcessor target = new StatsRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/";
            target.APIKey = "key";
            string url = target.BuildURL(null);
            Assert.AreEqual(url, target.BaseUrl + "stats?key=" + target.APIKey);
        }

        [TestMethod]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            StatsRequestProcessor target = new StatsRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var stat = actual.Cast<Statistics>().ToList().FirstOrDefault();
        }

        [TestMethod]

        public void ProcessResultsForValidValueTest()
        {
            StatsRequestProcessor target = new StatsRequestProcessor();
            string xmlResonse = TestHelpers.ConvertResponseToXml(m_testValidValueQueryResponse);
            XElement soResponse = XElement.Parse(xmlResonse);
            IList actual = target.ProcessResults(soResponse);
            var stat = actual.Cast<Statistics>().ToList().FirstOrDefault();

            Assert.AreEqual(stat.TotalQuestions, 631769);
        }




    }
}
