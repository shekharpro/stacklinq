﻿using LINQToStackOverflow;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;



namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class UserRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidValueQueryResponse = @"{""users"":[{""user_id"":22656,""user_type"":""registered"",""creation_date"":1222430705,""display_name"":""Jon Skeet"",""reputation"":170389,""email_hash"":""6d8ebb117e8d83d74ea95fbdd0f87e13"",""age"":33,""last_access_date"":1272102190,""website_url"":""http://pobox.com/~skeet/csharp"",""location"":""Reading, UK"",""about_me"":""<p>\r\nAuthor of <a href=\""http://www.amazon.com/dp/1933988363\"" rel=\""nofollow\"">C# in Depth</a>.<br>\r\nCurrently a software engineer at Google, London.<br>\r\nMicrosoft MVP (C#, 2003 onwards)\r\n</p>\r\nSites:\r\n<ul>\r\n<li><a href=\""http://csharpindepth.com\"" rel=\""nofollow\"">C# in Depth</a>\r\n<li><a href=\""http://msmvps.com/jon.skeet\"" rel=\""nofollow\"">Coding blog</a>\r\n<li><a href=\""http://pobox.com/~skeet/csharp\"" rel=\""nofollow\"">C# articles</a>\r\n<li><a href=\""http://twitter.com/jonskeet\"" rel=\""nofollow\"">Twitter updates</a>\r\n</ul>\r\nEmail: skeet@pobox.com"",""question_count"":22,""answer_count"":7960,""view_count"":114628,""up_vote_count"":5665,""down_vote_count"":255,""accept_rate"":86,""user_questions_url"":""/users/22656/questions"",""user_answers_url"":""/users/22656/answers"",""user_favorites_url"":""/users/22656/favorites"",""user_tags_url"":""/users/22656/tags"",""user_badges_url"":""/users/22656/badges"",""user_timeline_url"":""/users/22656/timeline"",""user_mentioned_url"":""/users/22656/mentioned"",""user_comments_url"":""/users/22656/comments"",""user_reputation_url"":""/users/22656/reputation"",""badge_counts"":{""gold"":32,""silver"":604,""bronze"":1173}},{""user_id"":23354,""user_type"":""moderator"",""creation_date"":1222667162,""display_name"":""Marc Gravell"",""reputation"":138278,""email_hash"":""b4ae7f8b50a2c32287e456394570679e"",""age"":31,""last_access_date"":1272097500,""website_url"":""http://marcgravell.blogspot.com/"",""location"":""UK"",""about_me"":""<p>e-mail: marc.gravell@gmail.com<br/>\r\nblog: http://marcgravell.blogspot.com</p>\r\n<p>C# programmer and MVP, with a keen interest in all things code.\r\n<p>Timezone: GMT</p>\r\nParticular areas:<ul>\r\n<li>C# language specifics\r\n<li>LINQ (including EF, L2S, ADO.NET Data Services)\r\n<li><a href=\""http://code.google.com/p/protobuf-net/\"" rel=\""nofollow\"">Protocol Buffers</a>\r\n<li>Expression (the LINQ one, not Blend)\r\n<li>TSQL / SQL Server general\r\n<li>xslt\r\n<li>ASP.NET MVC (but not regular ASP.NET)\r\n<li>WinForms\r\n<li>Bespoke runtime object models (TypeDescriptor)\r\n</ul></p>\r\n<p>All original code samples I post here are given freely to the public domain on an \""as is\"" basis, but no warranty is given (implied or assumed) etc - you know the drill...</p>"",""question_count"":20,""answer_count"":5393,""view_count"":27475,""up_vote_count"":8196,""down_vote_count"":441,""accept_rate"":94,""user_questions_url"":""/users/23354/questions"",""user_answers_url"":""/users/23354/answers"",""user_favorites_url"":""/users/23354/favorites"",""user_tags_url"":""/users/23354/tags"",""user_badges_url"":""/users/23354/badges"",""user_timeline_url"":""/users/23354/timeline"",""user_mentioned_url"":""/users/23354/mentioned"",""user_comments_url"":""/users/23354/comments"",""user_reputation_url"":""/users/23354/reputation"",""badge_counts"":{""gold"":9,""silver"":216,""bronze"":371}},{""user_id"":18393,""user_type"":""registered"",""creation_date"":1221783887,""display_name"":""cletus"",""reputation"":106970,""email_hash"":""2f364c2e36b52bc80296cbf23da8b231"",""age"":37,""last_access_date"":1272078662,""website_url"":""http://www.cforcoding.com"",""location"":""AU"",""about_me"":""<p>I am a software developer from Perth, Western Australia with roughly 13 years experience in developing end-to-end solutions in <b>Java, C#, C, C++, Perl, PHP and HTML/CSS/Javascript</b>.</p>\r\n\r\n<p>I have experience in developing user interfaces (Web and desktop), database design and development (Oracle, SQL Server, MySQL), data modelling, software design, data conversion, Web application development and high-performance high-availability computing.</p>\r\n\r\n<p>I have recently started a blog I call <a href=\""http://www.cforcoding.com\"" rel=\""nofollow\"">C for Coding</a>. Come check it out!</p>\r\n"",""question_count"":62,""answer_count"":3053,""view_count"":11734,""up_vote_count"":1976,""down_vote_count"":378,""accept_rate"":81,""user_questions_url"":""/users/18393/questions"",""user_answers_url"":""/users/18393/answers"",""user_favorites_url"":""/users/18393/favorites"",""user_tags_url"":""/users/18393/tags"",""user_badges_url"":""/users/18393/badges"",""user_timeline_url"":""/users/18393/timeline"",""user_mentioned_url"":""/users/18393/mentioned"",""user_comments_url"":""/users/18393/comments"",""user_reputation_url"":""/users/18393/reputation"",""badge_counts"":{""gold"":14,""silver"":147,""bronze"":296}},{""user_id"":12950,""user_type"":""registered"",""creation_date"":1221582134,""display_name"":""tvanfosson"",""reputation"":103913,""email_hash"":""fb599bac7c1f602a2ea455951d6da209"",""age"":47,""last_access_date"":1272071652,""website_url"":""http://farm-fresh-code.blogspot.com"",""location"":""Iowa City, IA"",""about_me"":""<p>\r\nWeb developer using primarily .NET/C#, though I've also done some Java, Ruby, C++ as well.  Sysadmin\r\nfor nearly 20 years before my current gig doing\r\na fair amount of coding, though.\r\n</p>\r\n<p>\r\n<a href=\""http://farm-fresh-code.blogspot.com\"" rel=\""nofollow\"">Blog</a><br />\r\n<a href=\""http://careers.stackoverflow.com/tvanfosson\"" rel=\""nofollow\"">CV</a>\r\n(no recruiters please)<br/>\r\n<a href=\""http://myweb.uiowa.edu/timv\"" rel=\""nofollow\"">Academic Page</a></p>\r\n<p>\r\nPlease do refer to me as <b>tv</b> when replying to comments, etc.</p>\r\n\r\n<p><em>The competent programmer is fully aware of the strictly limited size of his own skull; therefore he approaches the programming task in full humility, and among other things he avoids clever tricks like the plague.</em> -- E.W. Dijkstra, <a href=\""http://www.cs.utexas.edu/~EWD/transcriptions/EWD03xx/EWD340.html\"" rel=\""nofollow\"">The Humble Programmer</a>\r\n</p>\r\n"",""question_count"":43,""answer_count"":4147,""view_count"":10069,""up_vote_count"":3618,""down_vote_count"":261,""accept_rate"":92,""user_questions_url"":""/users/12950/questions"",""user_answers_url"":""/users/12950/answers"",""user_favorites_url"":""/users/12950/favorites"",""user_tags_url"":""/users/12950/tags"",""user_badges_url"":""/users/12950/badges"",""user_timeline_url"":""/users/12950/timeline"",""user_mentioned_url"":""/users/12950/mentioned"",""user_comments_url"":""/users/12950/comments"",""user_reputation_url"":""/users/12950/reputation"",""badge_counts"":{""gold"":7,""silver"":72,""bronze"":171}},{""user_id"":95810,""user_type"":""registered"",""creation_date"":1240625354,""display_name"":""Alex Martelli"",""reputation"":97289,""email_hash"":""e8d5fe90f1fe2148bf130cccd4dc311c"",""age"":54,""last_access_date"":1272090720,""website_url"":""http://www.aleax.it"",""location"":""Palo Alto, CA"",""about_me"":""Lots of info about me is on my <a href=\""http://www.google.com/profiles/aleaxit\"" rel=\""nofollow\"">Google Profile</a> and links therefrom, and some on my <a href=\""http://wiki.python.org/moin/AlexMartelli\"" rel=\""nofollow\"">Python Wiki Homepage</a>.\r\n<p>\r\nI spotted a recent SO-related <a href=\""http://twitter.com/justinlilly/status/9431196596\"" rel=\""nofollow\"">tweet</a> about me at Pycon. The \""is a machine\"" bit there is <a href=\""http://www.google.com/search?q=martellibot\"" rel=\""nofollow\"">not new</a>, but I think it's the first time that comment has been elicited by observing me in real life, as opposed to reading my posts &c;-).\r\n\r\n\r\n"",""question_count"":5,""answer_count"":4109,""view_count"":12483,""up_vote_count"":489,""down_vote_count"":29,""accept_rate"":100,""user_questions_url"":""/users/95810/questions"",""user_answers_url"":""/users/95810/answers"",""user_favorites_url"":""/users/95810/favorites"",""user_tags_url"":""/users/95810/tags"",""user_badges_url"":""/users/95810/badges"",""user_timeline_url"":""/users/95810/timeline"",""user_mentioned_url"":""/users/95810/mentioned"",""user_comments_url"":""/users/95810/comments"",""user_reputation_url"":""/users/95810/reputation"",""badge_counts"":{""gold"":3,""silver"":87,""bronze"":228}}]}";
        private string m_testSingleUserQueryResponse = @"{""users"":[{""user_id"":34796,""user_type"":""registered"",""creation_date"":1225910519,""display_name"":""Dave Swersky"",""reputation"":11391,""email_hash"":""384019c7e3c678bdcf27803abb865e42"",""age"":36,""last_access_date"":1272105161,""website_url"":""http://www.daveswersky.com"",""location"":""Cleveland, OH"",""about_me"":""I have been writing software on the .NET platform since about 2001.  Since that time I've worked on lots of systems including web portals, asset management, ERP, CRM, content management, email, and SOA.  I credit my love of research and the bleeding edge with four years at Microsoft Product Support, where I learned how to learn about technology. \r\n"",""question_count"":26,""answer_count"":708,""view_count"":826,""up_vote_count"":736,""down_vote_count"":50,""accept_rate"":92,""user_questions_url"":""/users/34796/questions"",""user_answers_url"":""/users/34796/answers"",""user_favorites_url"":""/users/34796/favorites"",""user_tags_url"":""/users/34796/tags"",""user_badges_url"":""/users/34796/badges"",""user_timeline_url"":""/users/34796/timeline"",""user_mentioned_url"":""/users/34796/mentioned"",""user_comments_url"":""/users/34796/comments"",""user_reputation_url"":""/users/34796/reputation"",""badge_counts"":{""gold"":0,""silver"":6,""bronze"":26}}]}";

        [TestMethod]
        public void GetParameters()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            Expression<Func<User, bool>> expression = u => u.UserID == 34796;
            LambdaExpression lambdaExpression = expression as LambdaExpression;
            var queryParams = target.GetParameters(lambdaExpression);
            Assert.IsTrue(queryParams.Contains(new KeyValuePair<string, string>("UserID", "34796")));

        }

        [TestMethod()]
        public void BuildBaseURLTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            string url = target.BuildURL(null);
            Assert.AreEqual(url, target.BaseUrl + "users?key=" + target.APIKey);
        }

        [TestMethod]
        public void BuildUserURLTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { {"UserID","34796"}};
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users/34796?key=" + target.APIKey);
        }

        [TestMethod]
        public void BuildPagedURLTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "Page", "1" },{"PageSize","30"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users?key=" + target.APIKey + "&page=1&pagesize=30");
        }

        [TestMethod]
        public void BuildFilteredURLTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { {"Filter", "bob" }};
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users?key=" + target.APIKey + "&filter=bob");
        }

        [TestMethod]
        public void BuildSortedURLTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "SortBy", "Creation" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users?key=" + target.APIKey + "&sort=creation");
        }

        [TestMethod]
        public void BuildOrderedURLTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "Order", "Descending" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users?key=" + target.APIKey + "&order=desc");
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var user = actual.Cast<Answer>().ToList().FirstOrDefault();
        }

        [TestMethod]
        public void ProcessResultsForValidValueTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testValidValueQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var users = actual.Cast<User>().ToList();
            Assert.AreEqual(users.Count, 5);
        }

        [TestMethod]
        public void ProcessResultsForSingleUserValueTest()
        {
            UserRequestProcessor target = new UserRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testSingleUserQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var user = actual.Cast<User>().ToList().FirstOrDefault();
            Assert.AreEqual("Dave Swersky", user.DisplayName);
        }



    }
}
