﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace LINQToStackOverflow.Test
{
    class TestHelpers
    {
        public static string ConvertResponseToXml(string s)
        {
            string respXml = null;
            var stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(Convert.ToString(s)));
            XmlDictionaryReader reader = JsonReaderWriterFactory.CreateJsonReader(stream, System.Text.Encoding.UTF8, XmlDictionaryReaderQuotas.Max, null);
            var doc = (XmlDocument)JsonConvert.DeserializeXmlNode(@"{""root"": " + s + "}");
            respXml = doc.OuterXml;
            return respXml;
        }
    }
}
