﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class TagRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidValueQueryResponse = @"{""tags"":[{""name"":""c#"",""count"":76538},{""name"":""java"",""count"":44726},{""name"":"".net"",""count"":39473},{""name"":""php"",""count"":36971},{""name"":""asp.net"",""count"":34975},{""name"":""javascript"",""count"":32586},{""name"":""c++"",""count"":28365},{""name"":""jquery"",""count"":25705},{""name"":""python"",""count"":22862},{""name"":""iphone"",""count"":22819},{""name"":""sql"",""count"":18964},{""name"":""mysql"",""count"":16311},{""name"":""html"",""count"":15494},{""name"":""sql-server"",""count"":14640},{""name"":""ruby-on-rails"",""count"":12359},{""name"":""c"",""count"":12353},{""name"":""asp.net-mvc"",""count"":11928},{""name"":""wpf"",""count"":11686},{""name"":""css"",""count"":11621},{""name"":""objective-c"",""count"":10887},{""name"":""windows"",""count"":10098},{""name"":""xml"",""count"":9149},{""name"":""ruby"",""count"":9106},{""name"":""best-practices"",""count"":8647},{""name"":""database"",""count"":8475},{""name"":""vb.net"",""count"":8085},{""name"":""visual-studio"",""count"":7836},{""name"":""regex"",""count"":7303},{""name"":""winforms"",""count"":7276},{""name"":""ajax"",""count"":7266},{""name"":""linux"",""count"":7261},{""name"":""django"",""count"":6903},{""name"":""android"",""count"":6821},{""name"":""visual-studio-2008"",""count"":6491},{""name"":""iphone-sdk"",""count"":6392},{""name"":""subjective"",""count"":6085},{""name"":""beginner"",""count"":5962},{""name"":""web-development"",""count"":5946},{""name"":""flex"",""count"":5764},{""name"":""flash"",""count"":5510},{""name"":""linq"",""count"":5489},{""name"":""wcf"",""count"":5314},{""name"":""cocoa"",""count"":5025},{""name"":""silverlight"",""count"":4982},{""name"":""sql-server-2005"",""count"":4858},{""name"":""performance"",""count"":4794},{""name"":""cocoa-touch"",""count"":4791},{""name"":""perl"",""count"":4641},{""name"":""oracle"",""count"":4640},{""name"":""algorithm"",""count"":4586},{""name"":""svn"",""count"":4535},{""name"":""eclipse"",""count"":4513},{""name"":""web-services"",""count"":4510},{""name"":""security"",""count"":4454},{""name"":""sharepoint"",""count"":4426},{""name"":""delphi"",""count"":4409},{""name"":""multithreading"",""count"":4400},{""name"":""actionscript-3"",""count"":4268},{""name"":""arrays"",""count"":4039},{""name"":""unit-testing"",""count"":3923},{""name"":""nhibernate"",""count"":3841},{""name"":""linq-to-sql"",""count"":3822},{""name"":""tsql"",""count"":3603},{""name"":""apache"",""count"":3500},{""name"":""string"",""count"":3388},{""name"":""excel"",""count"":3302},{""name"":""homework"",""count"":3246},{""name"":""design"",""count"":3245},{""name"":""mvc"",""count"":3045},{""name"":""xcode"",""count"":2981}]}";

        [TestMethod]
        public void GetParametersTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            Expression<Func<Tag, bool>> expression = t => t.UserID == 34796;
            LambdaExpression lambdaExpression = expression as LambdaExpression;

            var queryParams = target.GetParameters(lambdaExpression);

            Assert.IsTrue(queryParams.Contains(
                new KeyValuePair<string, string>("UserID", "34796")));
        
        }

        [TestMethod]
        public void BuildBaseURLTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            string url = target.BuildURL(null);
            Assert.AreEqual(url, target.BaseUrl + "tags" + "?key=" + target.APIKey);
        }

        [TestMethod]
        public void BuildUserURLTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"UserID","34796"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users/34796/tags?key=" + target.APIKey);
        }

        [TestMethod]
        public void BuildPagedURLTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "Page", "1" }, {"PageSize","30"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "tags" + "?key=" + target.APIKey + "&page=1&pagesize=30");
        }

        [TestMethod]
        public void BuildSortedURLTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "SortBy", "Activity" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "tags" + "?key=" + target.APIKey + "&sort=activity");
        }

        [TestMethod]
        public void BuildOrderedURLTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "Order", "Descending" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "tags" + "?key=" + target.APIKey + "&order=desc");
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var tag = actual.Cast<Tag>().ToList().FirstOrDefault();
        }

        [TestMethod]
        public void ProcessResultsForValidValueTest()
        {
            TagRequestProcessor target = new TagRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testValidValueQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var tags = actual.Cast<Tag>().ToList();

            Assert.AreEqual(tags.Count, 70);
        }



    }
}
