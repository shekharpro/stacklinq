﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Xml.Linq;
using System.Collections;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class CommentRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidByUserQueryResponse = @"{""comments"":[{""comment_id"":2714595,""creation_date"":1271957463,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2693022,""post_type"":""question"",""score"":0,""edit_count"":1,""body"":""Code would help explain your question more clearly.  It looks like RedirectToAction() might help in your case.""},{""comment_id"":2713141,""creation_date"":1271947997,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""reply_to_user_id"":208809,""post_id"":2691507,""post_type"":""question"",""score"":0,""edit_count"":1,""body"":""@Gordon lol hopefully not... ow my eyes hurt...""},{""comment_id"":2712696,""creation_date"":1271945425,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2691507,""post_type"":""question"",""score"":0,""body"":""If you want to add emphasis, <b>use bold.</b>""},{""comment_id"":2711903,""creation_date"":1271940611,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2555456,""post_type"":""answer"",""score"":0,""body"":""You can make it less frustrating by looking into T4 templates.  T4 templates can cook up ViewModels based on your Entity Model.""},{""comment_id"":2705806,""creation_date"":1271875168,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2685542,""post_type"":""question"",""score"":0,""body"":""What happens if you try to navigate directly to the CSS files from your browser?""},{""comment_id"":2699601,""creation_date"":1271818374,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""reply_to_user_id"":60761,""post_id"":2663829,""post_type"":""answer"",""score"":0,""body"":""@Henk, @Ben: So the upvoted and accepted answer says essentially the same thing as mine and @kyoryu's answers... which get downvoted?""},{""comment_id"":2693363,""creation_date"":1271763118,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2668993,""post_type"":""answer"",""score"":0,""body"":""Good- just know that this method only makes it inconvenient, but far from impossible, to decode.""},{""comment_id"":2654189,""creation_date"":1271267863,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""reply_to_user_id"":34813,""post_id"":2639505,""post_type"":""question"",""score"":0,""body"":""@voyager- agreed, perhaps I should be less worried about newer users.  I'm not so worried about rep- considering I had no intention of attempting to answer this question, I was honestly just pointing out the OP's lack of accepted answers.""},{""comment_id"":2653936,""creation_date"":1271265904,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2639505,""post_type"":""question"",""score"":0,""body"":""You have accepted no answers to your questions- that makes future answers unlikely.""},{""comment_id"":2645732,""creation_date"":1271185094,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2632346,""post_type"":""answer"",""score"":4,""body"":""+1 browser-hosted javascript should never have that much visibility into a machine.  The universe would collapse under its own dark-matter weight.""},{""comment_id"":2644602,""creation_date"":1271176410,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2630953,""post_type"":""answer"",""score"":0,""edit_count"":1,""body"":""I think you should reconsider AOP- it sounds like a good option based on your requirements.  PostSharp actually does what you're looking for- it performs post-build operations, injecting the advice directly into the IL.""},{""comment_id"":2644275,""creation_date"":1271174065,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2631033,""post_type"":""question"",""score"":1,""body"":""What's the question?""},{""comment_id"":2643146,""creation_date"":1271166960,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2629782,""post_type"":""answer"",""score"":0,""body"":""The Skip method must be in there somewhere... search your code for &quot;Skip&quot;""},{""comment_id"":2637000,""creation_date"":1271096348,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2624310,""post_type"":""question"",""score"":0,""body"":""If you just want a traffic analyzer that will do that, look into Google Analytics.""},{""comment_id"":2636863,""creation_date"":1271095177,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2623841,""post_type"":""answer"",""score"":0,""edit_count"":1,""body"":""&quot;StackOverflow moves pretty fast, if you don't slow down once in a while, you might miss it.&quot; -Ferris Bueller (paraphrasing ;)""},{""comment_id"":2636164,""creation_date"":1271089474,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2623416,""post_type"":""answer"",""score"":1,""body"":""The form that comes up when you're adding a record is part of jqGrid, I don't think there is a way to bypass that.  You can &quot;stub&quot; the call by pointing to a URL that simply returns a success code no matter what you pass to it...""},{""comment_id"":2635403,""creation_date"":1271084295,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2622681,""post_type"":""answer"",""score"":0,""edit_count"":1,""body"":""I don't personally hold to the idea that .NET is somehow deficient because it doesn't &quot;natively&quot; support AOP, so I wouldn't limit consideration of AOP frameworks only to Unity.  The .NET remoting bits were never originally designed to support AOP.  AOP is, in most cases, a programming technique/convention layered on top of frameworks (like .NET) that are capable of supporting it.""},{""comment_id"":2635395,""creation_date"":1271084235,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2622917,""post_type"":""answer"",""score"":0,""edit_count"":1,""body"":""+1 This is true.  ""},{""comment_id"":2634987,""creation_date"":1271081064,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2622559,""post_type"":""answer"",""score"":0,""edit_count"":1,""body"":""You can still &quot;jump&quot; to an Action using the Start Action, such as <a href=\""http://mysite.com/someController/someAction/someParameter\"" rel=\""nofollow\"">mysite.com/someController/someAction/&hellip;</a>\nJust don't include a page name.""},{""comment_id"":2634610,""creation_date"":1271077908,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2622164,""post_type"":""answer"",""score"":0,""edit_count"":2,""body"":""Are you setting an id on each form field?  You also need to make sure the button is of type=submit, and the Action needs to be able to accept a POST (decorate the Action method with [HttpPost]""},{""comment_id"":2620394,""creation_date"":1270838207,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2609927,""post_type"":""question"",""score"":1,""body"":""Do you get an error if you don't encode the parameter with the umlaut?""},{""comment_id"":2619117,""creation_date"":1270827788,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2608275,""post_type"":""answer"",""score"":0,""body"":""That's a good idea for an app.  I suggest you think about the entities you'd want to track- Song, Artist, Album, etc.  Use your collection to design a schema, then read the data off the files and populate a database.  Take a look at TagLibSharp: <a href=\""http://developer.novell.com/wiki/index.php/TagLib_Sharp\"" rel=\""nofollow\"">developer.novell.com/wiki/index.php/TagLib_Sharp</a>""},{""comment_id"":2618804,""creation_date"":1270825862,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2608541,""post_type"":""question"",""score"":1,""body"":""Could there be a problem with the key itself?  Odd characters, problem between UNICODE and UTF-8?""},{""comment_id"":2610029,""creation_date"":1270738622,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2600155,""post_type"":""answer"",""score"":0,""body"":""There is no way using the Ajax.BeginForm to populate the values from your form, because the BeginForm helper is called when the page is rendered.  You can get the values of the form fields using Request.Form.""},{""comment_id"":2605520,""creation_date"":1270691825,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2596964,""post_type"":""answer"",""score"":1,""body"":""There's no latency when running local, so the socket opens and closes within milliseconds.""},{""comment_id"":2605366,""creation_date"":1270689348,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2596869,""post_type"":""answer"",""score"":0,""body"":""One could argue that the webcam support had to be figured out first, and that video compression/streaming is outside the scope Silverlight.  Microsoft is clearly planning to support videoconferencing capability, but they will probably leave the plumbing to third party providers.""},{""comment_id"":2605357,""creation_date"":1270689068,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2596929,""post_type"":""question"",""score"":1,""body"":""Can I have one??? :)""},{""comment_id"":2604070,""creation_date"":1270673712,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2595833,""post_type"":""answer"",""score"":0,""body"":""+1: This would be a lower-impact, less-code solution than checking the session with every request. ""},{""comment_id"":2602074,""creation_date"":1270658327,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2594188,""post_type"":""question"",""score"":2,""body"":""Can you try the Windows Live client from the server?  Have you tried to telnet from your desktop to port 25?""},{""comment_id"":2602054,""creation_date"":1270658209,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""post_id"":2588301,""post_type"":""answer"",""score"":0,""body"":""In cases where javascript is unavailable, you rely on the server to handle the validation, yes.  I still wouldn't call the validation you link to in your question &quot;business rules.&quot;  They're data validation rules, which are much simpler.  You can't afford to allow your actual business logic degrade, gracefully or not.""}]}";

        [TestMethod()]
        public void GetParametersTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            Expression<Func<Comment, bool>> expression = c => c.CommentID == 123456;
            LambdaExpression lambdaExpression = expression as LambdaExpression;

            var queryParams = target.GetParameters(lambdaExpression);

            Assert.IsTrue(queryParams.Contains(
                new KeyValuePair<string, string>("CommentID", "123456")));
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void NullParametersTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = null;
            string url = target.BuildURL(parms);
            Assert.Fail("Shouldn't make it here.");
            
        }

        [TestMethod()]
        public void BuildBaseURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "CommentID", "123456" } };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "comments/123456?key=key");
        }

        [TestMethod()]
        public void BuildOwnerURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" } };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/comments?key=key");
        }

        [TestMethod()]
        public void BuildPagedURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" }, {"Page", "1"}, {"PageSize", "30" }};

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/comments?key=key&page=1&pagesize=30");
        }


        [TestMethod()]
        public void BuildDateRangeURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" }, 
                { "FromDate", "3/31/2010" }, { "ToDate", "4/30/2010" } };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/comments?key=key&fromdate=1269993600&todate=1272585600");
        }

        [TestMethod()]
        public void BuildSortedURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" }, 
                {"SortBy", "Creation"} };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/comments?key=key&sort=creation");
        }

        [TestMethod()]
        public void BuildAscendingOrderedURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" }, 
                {"SortBy", "Creation"}, {"Order", "Ascending"} };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/comments?key=key&sort=creation&order=asc");
        }

        [TestMethod()]
        public void BuildDescendingOrderedURLTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" }, 
                {"SortBy", "Creation"}, {"Order", "Descending"} };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/comments?key=key&sort=creation&order=desc");
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var comment = actual.Cast<Comment>().ToList().FirstOrDefault();
        }

        [TestMethod()]
        public void ProcessResultsForValidQueryByUserTest()
        {
            CommentRequestProcessor target = new CommentRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testValidByUserQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            //var comments = actual.Cast<Comment>().ToList();

            //Assert.AreEqual(comments.Count, 30);
        }
    }
}
