﻿using LINQToStackOverflow;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Xml.Linq;
using System.Collections;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class BadgeRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidBadgesQueryResponse = @"{""badges"":[{""badge_id"":9,""rank"":""bronze"",""name"":""Autobiographer"",""description"":""Completed all user profile fields"",""award_count"":26498,""tag_based"":false},{""badge_id"":30,""rank"":""silver"",""name"":""Beta"",""description"":""Actively participated in the Stack Overflow private beta"",""award_count"":2612,""tag_based"":false},{""badge_id"":8,""rank"":""bronze"",""name"":""Citizen Patrol"",""description"":""First flagged post"",""award_count"":5401,""tag_based"":false},{""badge_id"":32,""rank"":""silver"",""name"":""Civic Duty"",""description"":""Voted 300 times"",""award_count"":4406,""tag_based"":false},{""badge_id"":4,""rank"":""bronze"",""name"":""Cleanup"",""description"":""First rollback"",""award_count"":3620,""tag_based"":false},{""badge_id"":31,""rank"":""bronze"",""name"":""Commentator"",""description"":""Left 10 comments"",""award_count"":28866,""tag_based"":false},{""badge_id"":7,""rank"":""bronze"",""name"":""Critic"",""description"":""First down vote"",""award_count"":24116,""tag_based"":false},{""badge_id"":37,""rank"":""bronze"",""name"":""Disciplined"",""description"":""Deleted own post with score of 3 or higher"",""award_count"":1572,""tag_based"":false},{""badge_id"":3,""rank"":""bronze"",""name"":""Editor"",""description"":""First edit"",""award_count"":58341,""tag_based"":false},{""badge_id"":155,""rank"":""gold"",""name"":""Electorate"",""description"":""Voted on 600 questions and 25% or more of total votes are on questions"",""award_count"":223,""tag_based"":false},{""badge_id"":19,""rank"":""silver"",""name"":""Enlightened"",""description"":""First answer was accepted with at least 10 up votes"",""award_count"":10596,""tag_based"":false},{""badge_id"":71,""rank"":""silver"",""name"":""Enthusiast"",""description"":""Visited the site each day for 30 consecutive days"",""award_count"":6482,""tag_based"":false},{""badge_id"":145,""rank"":""silver"",""name"":""Epic"",""description"":""Hit the daily reputation cap on 50 days"",""award_count"":79,""tag_based"":false},{""badge_id"":28,""rank"":""gold"",""name"":""Famous Question"",""description"":""Asked a question with 10,000 views"",""award_count"":852,""tag_based"":false},{""badge_id"":83,""rank"":""gold"",""name"":""Fanatic"",""description"":""Visited the site each day for 100 consecutive days"",""award_count"":855,""tag_based"":false},{""badge_id"":33,""rank"":""silver"",""name"":""Favorite Question"",""description"":""Question favorited by 25 users"",""award_count"":1004,""tag_based"":false},{""badge_id"":15,""rank"":""silver"",""name"":""Generalist"",""description"":""Active in many different tags"",""award_count"":0,""tag_based"":false},{""badge_id"":24,""rank"":""silver"",""name"":""Good Answer"",""description"":""Answer voted up 25 times"",""award_count"":8474,""tag_based"":false},{""badge_id"":21,""rank"":""silver"",""name"":""Good Question"",""description"":""Question voted up 25 times"",""award_count"":1967,""tag_based"":false},{""badge_id"":25,""rank"":""gold"",""name"":""Great Answer"",""description"":""Answer voted up 100 times"",""award_count"":957,""tag_based"":false},{""badge_id"":22,""rank"":""gold"",""name"":""Great Question"",""description"":""Question voted up 100 times"",""award_count"":171,""tag_based"":false},{""badge_id"":18,""rank"":""silver"",""name"":""Guru"",""description"":""Accepted answer and voted up 40 times"",""award_count"":1020,""tag_based"":false},{""badge_id"":146,""rank"":""gold"",""name"":""Legendary"",""description"":""Hit the daily reputation cap on 150 days"",""award_count"":13,""tag_based"":false},{""badge_id"":144,""rank"":""bronze"",""name"":""Mortarboard"",""description"":""Hit the daily reputation cap for the first time"",""award_count"":3932,""tag_based"":false},{""badge_id"":17,""rank"":""silver"",""name"":""Necromancer"",""description"":""Answered a question more than 60 days later with at least 5 votes"",""award_count"":4300,""tag_based"":false},{""badge_id"":23,""rank"":""bronze"",""name"":""Nice Answer"",""description"":""Answer voted up 10 times"",""award_count"":52217,""tag_based"":false},{""badge_id"":20,""rank"":""bronze"",""name"":""Nice Question"",""description"":""Question voted up 10 times"",""award_count"":12007,""tag_based"":false},{""badge_id"":27,""rank"":""silver"",""name"":""Notable Question"",""description"":""Asked a question with 2,500 views"",""award_count"":12065,""tag_based"":false},{""badge_id"":5,""rank"":""bronze"",""name"":""Organizer"",""description"":""First retag"",""award_count"":11633,""tag_based"":false},{""badge_id"":38,""rank"":""bronze"",""name"":""Peer Pressure"",""description"":""Deleted own post with score of -3 or lower"",""award_count"":2836,""tag_based"":false},{""badge_id"":26,""rank"":""bronze"",""name"":""Popular Question"",""description"":""Asked a question with 1,000 views"",""award_count"":46313,""tag_based"":false},{""badge_id"":62,""rank"":""gold"",""name"":""Populist"",""description"":""Provided an answer that outscored an accepted answer with 10 votes by 2x"",""award_count"":310,""tag_based"":false},{""badge_id"":94,""rank"":""silver"",""name"":""Pundit"",""description"":""Left 10 comments with score of 10 or more"",""award_count"":70,""tag_based"":false},{""badge_id"":95,""rank"":""gold"",""name"":""Reversal"",""description"":""Provided answer of +20 score to a question of -5 score"",""award_count"":37,""tag_based"":false},{""badge_id"":10,""rank"":""bronze"",""name"":""Scholar"",""description"":""Asked a question and accepted an answer"",""award_count"":63949,""tag_based"":false},{""badge_id"":14,""rank"":""bronze"",""name"":""Self-Learner"",""description"":""Answered your own question with at least 3 up votes"",""award_count"":3553,""tag_based"":false},{""badge_id"":36,""rank"":""gold"",""name"":""Stellar Question"",""description"":""Question favorited by 100 users"",""award_count"":143,""tag_based"":false},{""badge_id"":12,""rank"":""silver"",""name"":""Strunk & White"",""description"":""Edited 100 entries"",""award_count"":608,""tag_based"":false},{""badge_id"":2,""rank"":""bronze"",""name"":""Student"",""description"":""Asked first question with at least one up vote"",""award_count"":77862,""tag_based"":false},{""badge_id"":6,""rank"":""bronze"",""name"":""Supporter"",""description"":""First up vote"",""award_count"":57775,""tag_based"":false},{""badge_id"":11,""rank"":""silver"",""name"":""Taxonomist"",""description"":""Created a tag used by 50 questions"",""award_count"":1956,""tag_based"":false},{""badge_id"":1,""rank"":""bronze"",""name"":""Teacher"",""description"":""Answered first question with at least one up vote"",""award_count"":66691,""tag_based"":false},{""badge_id"":63,""rank"":""bronze"",""name"":""Tumbleweed"",""description"":""Asked a question with no answers, no comments, and low views for a week"",""award_count"":19838,""tag_based"":false},{""badge_id"":13,""rank"":""silver"",""name"":""Yearling"",""description"":""Active member for a year, earning at least 200 reputation"",""award_count"":17937,""tag_based"":false}]}";
        private string m_testValidBadgesByUserQueryResponse = @"{""badges"":[{""badge_id"":9,""rank"":""bronze"",""name"":""Autobiographer"",""description"":""Completed all user profile fields"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":8,""rank"":""bronze"",""name"":""Citizen Patrol"",""description"":""First flagged post"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":32,""rank"":""silver"",""name"":""Civic Duty"",""description"":""Voted 300 times"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":31,""rank"":""bronze"",""name"":""Commentator"",""description"":""Left 10 comments"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":7,""rank"":""bronze"",""name"":""Critic"",""description"":""First down vote"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":3,""rank"":""bronze"",""name"":""Editor"",""description"":""First edit"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":71,""rank"":""silver"",""name"":""Enthusiast"",""description"":""Visited the site each day for 30 consecutive days"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":24,""rank"":""silver"",""name"":""Good Answer"",""description"":""Answer voted up 25 times"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":144,""rank"":""bronze"",""name"":""Mortarboard"",""description"":""Hit the daily reputation cap for the first time"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":17,""rank"":""silver"",""name"":""Necromancer"",""description"":""Answered a question more than 60 days later with at least 5 votes"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":23,""rank"":""bronze"",""name"":""Nice Answer"",""description"":""Answer voted up 10 times"",""award_count"":11,""tag_based"":false,""user_id"":34796},{""badge_id"":27,""rank"":""silver"",""name"":""Notable Question"",""description"":""Asked a question with 2,500 views"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":5,""rank"":""bronze"",""name"":""Organizer"",""description"":""First retag"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":26,""rank"":""bronze"",""name"":""Popular Question"",""description"":""Asked a question with 1,000 views"",""award_count"":3,""tag_based"":false,""user_id"":34796},{""badge_id"":10,""rank"":""bronze"",""name"":""Scholar"",""description"":""Asked a question and accepted an answer"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":2,""rank"":""bronze"",""name"":""Student"",""description"":""Asked first question with at least one up vote"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":6,""rank"":""bronze"",""name"":""Supporter"",""description"":""First up vote"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":1,""rank"":""bronze"",""name"":""Teacher"",""description"":""Answered first question with at least one up vote"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":63,""rank"":""bronze"",""name"":""Tumbleweed"",""description"":""Asked a question with no answers, no comments, and low views for a week"",""award_count"":1,""tag_based"":false,""user_id"":34796},{""badge_id"":13,""rank"":""silver"",""name"":""Yearling"",""description"":""Active member for a year, earning at least 200 reputation"",""award_count"":1,""tag_based"":false,""user_id"":34796}]}";

        [TestMethod()]
        public void GetParametersTest()
        {
            BadgeRequestProcessor target = new BadgeRequestProcessor();
            Expression<Func<Badge, bool>> expression = b => b.UserID == 123456;
            LambdaExpression lambdaExpression = expression as LambdaExpression;

            var queryParams = target.GetParameters(lambdaExpression);

            Assert.IsTrue(queryParams.Contains(
                new KeyValuePair<string, string>("UserID", "123456")));
        }


        [TestMethod()]
        public void BuildURLTest()
        {
            BadgeRequestProcessor target = new BadgeRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = null;
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl+ "/badges?key=key");
        }

        [TestMethod()]
        public void BuildURLWithUserIDTest()
        {
            BadgeRequestProcessor target = new BadgeRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"UserID", "123456"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users/123456/badges?key=key");
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            BadgeRequestProcessor target = new BadgeRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var badge = actual.Cast<Badge>().ToList().FirstOrDefault();
        }

        [TestMethod()]
        public void ProcessResultsForAllBadgesRequest()
        {
            BadgeRequestProcessor target = new BadgeRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testValidBadgesQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var badges = actual.Cast<Badge>().ToList();

            Assert.AreEqual(badges.Count, 44);
        }

        [TestMethod()]
        public void ProcessResultsForBadgesByUser()
        {
            BadgeRequestProcessor target = new BadgeRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testValidBadgesByUserQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var badges = actual.Cast<Badge>().ToList();

            Assert.AreEqual(badges.Count, 20);
        }

    }
}
