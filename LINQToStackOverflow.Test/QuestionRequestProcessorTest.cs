﻿using LINQToStackOverflow;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class QuestionRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testSingleQuestionQueryResponse = @"{""questions"":[{""tags"":[""c++"",""ifstream""],""answer_count"":5,""answers"":[{""answer_id"":798825,""accepted"":true,""owner_email_hash"":""fc763c6ff6c160ddad05741e87e517b6"",""answer_comments_url"":""/answers/798825/comments"",""question_id"":798798,""owner_user_id"":1288,""owner_display_name"":""Bill the Lizard"",""creation_date"":1240936756,""last_edit_date"":1240941495,""last_activity_date"":1240941495,""up_vote_count"":7,""down_vote_count"":0,""view_count"":909,""score"":7,""community_owned"":false,""title"":""ifstream Open function not working""},{""answer_id"":798829,""accepted"":false,""owner_email_hash"":""5ac7f2fab7e2e84903aebbe0c9818661"",""answer_comments_url"":""/answers/798829/comments"",""question_id"":798798,""owner_user_id"":63309,""owner_display_name"":""Tom"",""creation_date"":1240936852,""last_activity_date"":1240936852,""up_vote_count"":0,""down_vote_count"":0,""view_count"":909,""score"":0,""community_owned"":false,""title"":""ifstream Open function not working""},{""answer_id"":798830,""accepted"":false,""owner_email_hash"":""d282519414468e930a7a12ae2d18d6a2"",""answer_comments_url"":""/answers/798830/comments"",""question_id"":798798,""owner_user_id"":66692,""owner_display_name"":""dirkgently"",""creation_date"":1240936866,""last_edit_date"":1240938999,""last_activity_date"":1240938999,""up_vote_count"":0,""down_vote_count"":0,""view_count"":909,""score"":0,""community_owned"":false,""title"":""ifstream Open function not working""},{""answer_id"":798859,""accepted"":false,""owner_email_hash"":""bb8dfdd27002c8e6423981ed146f964e"",""answer_comments_url"":""/answers/798859/comments"",""question_id"":798798,""owner_user_id"":46603,""owner_display_name"":""3DH"",""creation_date"":1240937140,""last_edit_date"":1240938913,""last_activity_date"":1240938913,""up_vote_count"":1,""down_vote_count"":0,""view_count"":909,""score"":1,""community_owned"":false,""title"":""ifstream Open function not working""},{""answer_id"":2661380,""accepted"":false,""owner_email_hash"":""da22cfe3f579fd9dfd84eb0c78e9bff2"",""answer_comments_url"":""/answers/2661380/comments"",""question_id"":798798,""owner_user_id"":319562,""owner_display_name"":""sama"",""creation_date"":1271573308,""last_activity_date"":1271573308,""up_vote_count"":0,""down_vote_count"":0,""view_count"":909,""score"":0,""community_owned"":false,""title"":""ifstream Open function not working""}],""accepted_answer_id"":798825,""favorite_count"":0,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_timeline_url"":""/questions/798798/timeline"",""question_comments_url"":""/questions/798798/comments"",""question_answers_url"":""/questions/798798/answers"",""question_id"":798798,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1240936350,""last_edit_date"":1240948731,""last_activity_date"":1271573308,""up_vote_count"":0,""down_vote_count"":0,""view_count"":909,""score"":0,""community_owned"":false,""title"":""ifstream Open function not working""}]}";
        private string m_testMultipleQuestionQueryResponse = @"{""questions"":[{""tags"":[""java"",""downloading""],""answer_count"":0,""favorite_count"":0,""owner_email_hash"":""2a506aa136b39988288ae7a941feab18"",""question_timeline_url"":""/questions/2701305/timeline"",""question_comments_url"":""/questions/2701305/comments"",""question_answers_url"":""/questions/2701305/answers"",""question_id"":2701305,""owner_user_id"":24046,""owner_display_name"":""nkr1pt"",""creation_date"":1272049777,""last_activity_date"":1272049777,""up_vote_count"":0,""down_vote_count"":0,""view_count"":5,""score"":0,""community_owned"":false,""title"":""Java: downloading issue using BufferedInputStream, BufferedOutputStream""},{""tags"":[""django"",""django-forms""],""answer_count"":0,""favorite_count"":0,""owner_email_hash"":""dd89a4e075fc0c9e394ce845b90edc62"",""question_timeline_url"":""/questions/2701303/timeline"",""question_comments_url"":""/questions/2701303/comments"",""question_answers_url"":""/questions/2701303/answers"",""question_id"":2701303,""owner_user_id"":23031,""owner_display_name"":""andrew"",""creation_date"":1272049762,""last_activity_date"":1272049762,""up_vote_count"":0,""down_vote_count"":0,""view_count"":3,""score"":0,""community_owned"":false,""title"":""MultipleHiddenInput doesn't encode properly over POST?""},{""tags"":[""currency"",""iphone-sdk"",""localization"",""locale""],""answer_count"":0,""favorite_count"":0,""owner_email_hash"":""dd562137e109a2528ed15d7d1ac34bec"",""question_timeline_url"":""/questions/2701301/timeline"",""question_comments_url"":""/questions/2701301/comments"",""question_answers_url"":""/questions/2701301/answers"",""question_id"":2701301,""owner_user_id"":277678,""owner_display_name"":""Bonnie"",""creation_date"":1272049720,""last_activity_date"":1272049720,""up_vote_count"":0,""down_vote_count"":0,""view_count"":1,""score"":0,""community_owned"":false,""title"":""Various counties \""currency decimal places width\"" in the iPhone-SDK""},{""tags"":[""linq"",""linq-to-entities""],""answer_count"":0,""favorite_count"":0,""owner_email_hash"":""42b3dc81fa533cf24949a1c17c8dcf88"",""question_timeline_url"":""/questions/2701300/timeline"",""question_comments_url"":""/questions/2701300/comments"",""question_answers_url"":""/questions/2701300/answers"",""question_id"":2701300,""owner_user_id"":152467,""owner_display_name"":""Billy Logan"",""creation_date"":1272049686,""last_activity_date"":1272049686,""up_vote_count"":1,""down_vote_count"":0,""view_count"":6,""score"":1,""community_owned"":false,""title"":""Linq - Orderby not ordering""},{""tags"":[""extjs""],""answer_count"":0,""favorite_count"":0,""owner_email_hash"":""b09b23e2c1d78a98c3946c2fec466a79"",""question_timeline_url"":""/questions/2701299/timeline"",""question_comments_url"":""/questions/2701299/comments"",""question_answers_url"":""/questions/2701299/answers"",""question_id"":2701299,""owner_user_id"":185064,""owner_display_name"":""TomH"",""creation_date"":1272049660,""last_activity_date"":1272049660,""up_vote_count"":0,""down_vote_count"":0,""view_count"":1,""score"":0,""community_owned"":false,""title"":""ExtJS: Combobox in EditorGridPanel not selecting the desired item (with test case)""}]}";

        [TestMethod()]
        public void GetParametersTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "Owner_User_ID", "123456" } };
            Expression<Func<Question, bool>> expression = q => q.OwnerID == 123456;
            LambdaExpression lambdaExpression = expression as LambdaExpression;

            var queryParams = target.GetParameters(lambdaExpression);

            Assert.IsTrue(queryParams.Contains(
                new KeyValuePair<string, string>("Owner_User_ID", "123456")));
        }

        [TestMethod()]
        public void BuildBaseURLTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            string url = target.BuildURL(null);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey);
        }

        [TestMethod()]
        public void BuildURLForDateRangeQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"FromDate", "3/31/2010"},{"ToDate","4/30/2010"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey + "&fromdate=1269993600&todate=1272585600");
        }

        [TestMethod()]
        public void BuildURLForSortedQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"SortBy", "Active"}};
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey + "&sort=active");
        }


        [TestMethod()]
        public void BuildURLForPagedQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"Page","1"},{"PageSize","30"}};
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey + "&page=1&pagesize=30");
        }

        [TestMethod()]
        public void BuildURLForOrderedQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"Order","Ascending"}};
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey + "&order=asc");
        }

        [TestMethod()]
        public void BuildURLForIncludeBodyQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "IncludeBody", "True" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey + "&body=true");
        }

        [TestMethod()]
        public void BuildURLForIncludeCommentsQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { {"IncludeComments","True"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "questions?key=" + target.APIKey + "&comments=true");
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var question = actual.Cast<Question>().ToList().FirstOrDefault();
        }

        [TestMethod()]
        public void ProcessResultsForSingleQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testSingleQuestionQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var question = actual.Cast<Question>().ToList().FirstOrDefault();

            Assert.AreEqual(question.QuestionID, 798798);
        }

        [TestMethod()]
        public void ProcessResultsForMultipleQuestionTest()
        {
            QuestionRequestProcessor target = new QuestionRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testMultipleQuestionQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var question = actual.Cast<Question>().ToList();

            Assert.AreEqual(question.Count, 5);
        }



        



    }
}
