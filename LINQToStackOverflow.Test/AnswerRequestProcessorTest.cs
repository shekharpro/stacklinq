﻿using LINQToStackOverflow;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class AnswerRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testSingleAnswerQueryResponse = @"{""answers"":[{""answer_id"":266238,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":265722,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1225912337,""last_edit_date"":1225912679,""last_activity_date"":1225912679,""up_vote_count"":0,""down_vote_count"":0,""view_count"":2100,""score"":0,""community_owned"":false,""title"":""ASP.NET cannot connect to SQL Server 2005""}]}";
        private string m_testMultipleAnswerByUserQueryResponse = @"{""answers"":[{""answer_id"":2663829,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2663813,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271622665,""last_edit_date"":1271848699,""last_activity_date"":1271848699,""up_vote_count"":0,""down_vote_count"":1,""view_count"":198,""score"":-1,""community_owned"":false,""title"":""Should I Make These Vectors Classes or Structs in C#""},{""answer_id"":2670235,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2670197,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271705639,""last_activity_date"":1271705639,""up_vote_count"":0,""down_vote_count"":0,""view_count"":67,""score"":0,""community_owned"":false,""title"":""What's open-source commenting system are available?""},{""answer_id"":2669543,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2669512,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271698547,""last_activity_date"":1271698547,""up_vote_count"":1,""down_vote_count"":0,""view_count"":41,""score"":1,""community_owned"":false,""title"":""How can I implement a voting system (UP/DOWN) or (THUMP UP/DOWN) like StackOverFlow in .NET ?""},{""answer_id"":2668993,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2668983,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271693565,""last_activity_date"":1271693565,""up_vote_count"":2,""down_vote_count"":0,""view_count"":61,""score"":2,""community_owned"":false,""title"":""How to simply encode and decode a string variable with javascript.""},{""answer_id"":2663805,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2663732,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271622098,""last_activity_date"":1271622098,""up_vote_count"":1,""down_vote_count"":0,""view_count"":23,""score"":1,""community_owned"":false,""title"":""Creating Composite primary keys in a many-to-many relationship in Entity Framework""},{""answer_id"":2663735,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2663717,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271620837,""last_activity_date"":1271620837,""up_vote_count"":0,""down_vote_count"":0,""view_count"":33,""score"":0,""community_owned"":false,""title"":""Good source for studying professional software development with Visual Studio 2010?""},{""answer_id"":2645791,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2645755,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271339149,""last_activity_date"":1271339149,""up_vote_count"":2,""down_vote_count"":0,""view_count"":37,""score"":2,""community_owned"":false,""title"":""connecting twitter""},{""answer_id"":2645763,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2645735,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271338967,""last_activity_date"":1271338967,""up_vote_count"":1,""down_vote_count"":0,""view_count"":87,""score"":1,""community_owned"":false,""title"":""Visual Studio 2010 Download?""},{""answer_id"":2640561,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2640536,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271274894,""last_activity_date"":1271274894,""up_vote_count"":0,""down_vote_count"":0,""view_count"":18,""score"":0,""community_owned"":false,""title"":""How to build a tracing log in asp.net mvc?""},{""answer_id"":2639717,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2639696,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271267447,""last_activity_date"":1271267447,""up_vote_count"":0,""down_vote_count"":0,""view_count"":17,""score"":0,""community_owned"":false,""title"":""Is there a way to remove the source domain from the window title in an out-of-browser Silverlight 3 app?""},{""answer_id"":2639698,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2639625,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271267261,""last_activity_date"":1271267261,""up_vote_count"":0,""down_vote_count"":0,""view_count"":33,""score"":0,""community_owned"":false,""title"":""How to Expression.Invoke an arbitrary LINQ 2 SQL Query""},{""answer_id"":2638561,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2638529,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271257912,""last_activity_date"":1271257912,""up_vote_count"":2,""down_vote_count"":0,""view_count"":75,""score"":2,""community_owned"":false,""title"":""Sample MS application for ASP.NET MVC?""},{""answer_id"":2638510,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2638442,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271257615,""last_activity_date"":1271257615,""up_vote_count"":0,""down_vote_count"":0,""view_count"":44,""score"":0,""community_owned"":false,""title"":""LINQ and SQL performance issue when working with Membership""},{""answer_id"":2631897,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2631862,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271181165,""last_activity_date"":1271181165,""up_vote_count"":0,""down_vote_count"":0,""view_count"":48,""score"":0,""community_owned"":false,""title"":""textbox not getting refreshed ""},{""answer_id"":2631320,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2631270,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271176300,""last_activity_date"":1271176300,""up_vote_count"":0,""down_vote_count"":0,""view_count"":12,""score"":0,""community_owned"":false,""title"":""Creating a framework for ASP.NET web forms similar to Flex states.""},{""answer_id"":2631177,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2631098,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271175032,""last_activity_date"":1271175032,""up_vote_count"":0,""down_vote_count"":0,""view_count"":63,""score"":0,""community_owned"":false,""title"":""How does one add an \""id\"" attribute to Html.LabelFor() in ASP.NET MVC2?""},{""answer_id"":2631041,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2631007,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271173948,""last_activity_date"":1271173948,""up_vote_count"":0,""down_vote_count"":0,""view_count"":47,""score"":0,""community_owned"":false,""title"":""Where to put my $(document).ready(function(){ When I have methods in a .js""},{""answer_id"":2630998,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2630966,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271173602,""last_activity_date"":1271173602,""up_vote_count"":0,""down_vote_count"":0,""view_count"":38,""score"":0,""community_owned"":false,""title"":""JQuery: is it possible to instantiate a class on client and pass it to $.ajax to post?""},{""answer_id"":2630973,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2630945,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271173325,""last_activity_date"":1271173325,""up_vote_count"":1,""down_vote_count"":0,""view_count"":41,""score"":1,""community_owned"":false,""title"":""Is it possible to write a SQL query to return specific rows, but then join some columns of those rows with another table?""},{""answer_id"":2630953,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2630911,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271173196,""last_activity_date"":1271173196,""up_vote_count"":1,""down_vote_count"":0,""view_count"":60,""score"":1,""community_owned"":false,""title"":""Is there a way in .NET to access the bytecode/IL/CLR that is currently running?""},{""answer_id"":2630930,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2630892,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271173075,""last_activity_date"":1271173075,""up_vote_count"":1,""down_vote_count"":0,""view_count"":101,""score"":1,""community_owned"":false,""title"":""Beginning with SQL""},{""answer_id"":2629782,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2629742,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271164581,""last_activity_date"":1271164581,""up_vote_count"":0,""down_vote_count"":0,""view_count"":27,""score"":0,""community_owned"":false,""title"":""Error while adding dynamic data to an existing web site - The method 'Skip' is only supported for sorted input in LINQ to Entities. The method 'OrderBy' must be called before the method 'Skip'.""},{""answer_id"":2623841,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2623825,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271091636,""last_activity_date"":1271091636,""up_vote_count"":2,""down_vote_count"":0,""view_count"":22,""score"":2,""community_owned"":false,""title"":""How can I access form elements when using an ASP.NET MVC Ajax form?""},{""answer_id"":2623716,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2623697,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271090603,""last_activity_date"":1271090603,""up_vote_count"":2,""down_vote_count"":0,""view_count"":146,""score"":2,""community_owned"":false,""title"":""Should a setter return immediately if assigned the same value?""},{""answer_id"":2623586,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2623557,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271089326,""last_activity_date"":1271089326,""up_vote_count"":0,""down_vote_count"":0,""view_count"":72,""score"":0,""community_owned"":false,""title"":""Entity Framework - Insert with foreign key""},{""answer_id"":2623416,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2623314,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271087936,""last_activity_date"":1271087936,""up_vote_count"":1,""down_vote_count"":0,""view_count"":25,""score"":1,""community_owned"":false,""title"":""Using jqGrid with ArrayData and using the add, edit and delete buttons""},{""answer_id"":2622681,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2622614,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271081594,""last_activity_date"":1271081594,""up_vote_count"":3,""down_vote_count"":0,""view_count"":62,""score"":3,""community_owned"":false,""title"":""AOP and .NET: Is Contexts an obsolete concept?""},{""answer_id"":2622559,""accepted"":true,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2622522,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271080587,""last_activity_date"":1271080587,""up_vote_count"":0,""down_vote_count"":0,""view_count"":49,""score"":0,""community_owned"":false,""title"":""Cannot debug views in MVC2 project, getting \""The resource cannot be found\"" error""},{""answer_id"":2622422,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2622397,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271079279,""last_activity_date"":1271079279,""up_vote_count"":0,""down_vote_count"":0,""view_count"":40,""score"":0,""community_owned"":false,""title"":""Html.ActionLink showing query url instead of pretty url.""},{""answer_id"":2622164,""accepted"":false,""owner_email_hash"":""384019c7e3c678bdcf27803abb865e42"",""question_id"":2622112,""owner_user_id"":34796,""owner_display_name"":""Dave Swersky"",""creation_date"":1271076770,""last_activity_date"":1271076770,""up_vote_count"":1,""down_vote_count"":0,""view_count"":48,""score"":1,""community_owned"":false,""title"":""Add controls dynamically on button click in asp.net mvc""}]}";


        [TestMethod]
        public void GetParametersTest()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor() {BaseUrl = "http://api.stackoverflow.com" };
            Expression<Func<Answer, bool>> expression = a => a.AnswerID == 123456;
            LambdaExpression lambdaExpression = expression as LambdaExpression;

            var queryParams = target.GetParameters(lambdaExpression);

            Assert.IsTrue(queryParams.Contains(
                new KeyValuePair<string, string>("AnswerID", "123456")));
        }

        [TestMethod()]
        public void BuildBaseURLTest()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "AnswerID", "123456" } };

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "answers/123456?key=key");
        }

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void BuildURLWithUserIDandOwnerID()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "AnswerID", "123456" }, {"OwnerID", "987654"} };

            string url = target.BuildURL(parms);

            Assert.Fail("Shouldn't make it here.");
        }

        [TestMethod()]
        public void BuildURLWithOwnerID()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{ "OwnerID", "987654" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "users/987654/answers" + "?key=key");
        }

        [TestMethod()]
        public void BuildURLWithUserID()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "AnswerID", "123456" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "answers/123456?key=key");
        }

        [TestMethod()]
        public void BuildURLIncludeComments()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "AnswerID", "123456" }, {"IncludeComments", "True"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "answers/123456?key=key&comments=true");
        }

        [TestMethod()]
        public void BuildURLIncludeBody()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "AnswerID", "123456" }, { "IncludeBody", "True" } };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "answers/123456?key=key&body=true");
        }

        [TestMethod()]
        public void BuildURLPaged()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "AnswerID", "123456" }, { "Page", "1" }, {"PageSize", "30"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(url, target.BaseUrl + "answers/123456?key=key&page=1&pagesize=30");
        }
        

        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var answer = actual.Cast<Answer>().ToList().FirstOrDefault();
        }

        [TestMethod()]
        public void ProcessResultsForValidSingleAnswer()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testSingleAnswerQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList acutal = target.ProcessResults(soResponse);
            var answer = acutal.Cast<Answer>().ToList().FirstOrDefault();

            Assert.AreEqual(answer.AnswerID, 266238);
        }

        [TestMethod()]
        public void ProcessResultsForValidAnswerByUser()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testMultipleAnswerByUserQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList acutal = target.ProcessResults(soResponse);
            var answer = acutal.Cast<Answer>().ToList();

            Assert.AreEqual(answer.Count, 30);
        }


        [TestMethod()]
        public void NullParametersTest()
        {
            AnswerRequestProcessor target = new AnswerRequestProcessor() { BaseUrl = "http://api.stackoverflow.com/" };
            Dictionary<string, string> parameters = null;
            string actual;
            try
            {
                actual = target.BuildURL(parameters);
                Assert.Fail("Expected ArgumentException.");
            }
            catch (LINQToStackOverflowQueryException ae)
            {
                Assert.AreEqual<string>("At least one selection parameter (AnswerID or OwnerID) required.", ae.Message);
            }
        }



    }
}
