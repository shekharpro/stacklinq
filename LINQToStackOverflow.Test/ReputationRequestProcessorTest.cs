﻿using LINQToStackOverflow;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections;
using System.Linq.Expressions;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class ReputationRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidReputationResponse = @"{""rep_changes"":[{""user_id"":34796,""post_id"":2623416,""post_type"":""answer"",""title"":""Using jqGrid with ArrayData and using the add, edit and delete buttons"",""positive_rep"":25,""negative_rep"":0,""on_date"":1271838903},{""user_id"":34796,""post_id"":2663829,""post_type"":""answer"",""title"":""Should I Make These Vectors Classes or Structs in C#"",""positive_rep"":0,""negative_rep"":2,""on_date"":1271801902},{""user_id"":34796,""post_id"":2669543,""post_type"":""answer"",""title"":""How can I implement a voting system (UP/DOWN) or (THUMP UP/DOWN) like StackOverFlow in .NET ?"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271698635},{""user_id"":34796,""post_id"":2668993,""post_type"":""answer"",""title"":""How to simply encode and decode a string variable with javascript."",""positive_rep"":35,""negative_rep"":0,""on_date"":1271695460},{""user_id"":34796,""post_id"":2663805,""post_type"":""answer"",""title"":""Creating Composite primary keys in a many-to-many relationship in Entity Framework"",""positive_rep"":25,""negative_rep"":0,""on_date"":1271685645},{""user_id"":34796,""post_id"":2503504,""post_type"":""answer"",""title"":""How do i learn Cognos by myself without data to work on ?"",""positive_rep"":35,""negative_rep"":0,""on_date"":1271670595},{""user_id"":34796,""post_id"":1025311,""post_type"":""answer"",""title"":""how to set Page.IsValid in asp.net"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271522254},{""user_id"":34796,""post_id"":2630930,""post_type"":""answer"",""title"":""Beginning with SQL"",""positive_rep"":25,""negative_rep"":0,""on_date"":1271449353},{""user_id"":34796,""post_id"":2601623,""post_type"":""answer"",""title"":""With Silverlight what do I need ASP.Net for?"",""positive_rep"":30,""negative_rep"":0,""on_date"":1271429587},{""user_id"":34796,""post_id"":2645763,""post_type"":""answer"",""title"":""Visual Studio 2010 Download?"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271341794},{""user_id"":34796,""post_id"":2645791,""post_type"":""answer"",""title"":""connecting twitter"",""positive_rep"":20,""negative_rep"":0,""on_date"":1271339465},{""user_id"":34796,""post_id"":2555839,""post_type"":""answer"",""title"":""Choosing the MVC view engine"",""positive_rep"":21,""negative_rep"":0,""on_date"":1271329677},{""user_id"":34796,""post_id"":2596964,""post_type"":""answer"",""title"":""Why does my Windows Form app timeout when repeatedly hitting IIS 7 externally? "",""positive_rep"":35,""negative_rep"":0,""on_date"":1271306236},{""user_id"":34796,""post_id"":2638561,""post_type"":""answer"",""title"":""Sample MS application for ASP.NET MVC?"",""positive_rep"":20,""negative_rep"":0,""on_date"":1271258463},{""user_id"":34796,""post_id"":2630953,""post_type"":""answer"",""title"":""Is there a way in .NET to access the bytecode/IL/CLR that is currently running?"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271173430},{""user_id"":34796,""post_id"":2630973,""post_type"":""answer"",""title"":""Is it possible to write a SQL query to return specific rows, but then join some columns of those rows with another table?"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271173398},{""user_id"":34796,""post_id"":386946,""post_type"":""answer"",""title"":""best compression algorithm with the following features"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271163140},{""user_id"":34796,""post_id"":2623841,""post_type"":""answer"",""title"":""How can I access form elements when using an ASP.NET MVC Ajax form?"",""positive_rep"":35,""negative_rep"":0,""on_date"":1271160664},{""user_id"":34796,""post_id"":2622681,""post_type"":""answer"",""title"":""AOP and .NET: Is Contexts an obsolete concept?"",""positive_rep"":45,""negative_rep"":0,""on_date"":1271094851},{""user_id"":34796,""post_id"":2623716,""post_type"":""answer"",""title"":""Should a setter return immediately if assigned the same value?"",""positive_rep"":20,""negative_rep"":0,""on_date"":1271091352},{""user_id"":34796,""post_id"":2622559,""post_type"":""answer"",""title"":""Cannot debug views in MVC2 project, getting &ldquo;The resource cannot be found&rdquo; error"",""positive_rep"":15,""negative_rep"":0,""on_date"":1271081505},{""user_id"":34796,""post_id"":2622164,""post_type"":""answer"",""title"":""Add controls dynamically on button click in asp.net mvc"",""positive_rep"":10,""negative_rep"":0,""on_date"":1271077887},{""user_id"":34796,""post_id"":2609605,""post_type"":""answer"",""title"":""How to continuously scroll content within a DIV using jQuery?"",""positive_rep"":10,""negative_rep"":0,""on_date"":1270883938},{""user_id"":34796,""post_id"":2608213,""post_type"":""answer"",""title"":""Validation is more sensible at which Level Views Level or Model level in asp.net MVC"",""positive_rep"":35,""negative_rep"":0,""on_date"":1270829856},{""user_id"":34796,""post_id"":2608275,""post_type"":""answer"",""title"":""What&rsquo;s a good starting point, tutorial, or project, to learn database programming?"",""positive_rep"":55,""negative_rep"":0,""on_date"":1270827452},{""user_id"":34796,""post_id"":2608358,""post_type"":""answer"",""title"":""How can I pass parameters to the OnSuccess function of the AjaxOptions class in ASP.NET MVC?"",""positive_rep"":25,""negative_rep"":0,""on_date"":1270824998},{""user_id"":34796,""post_id"":2600155,""post_type"":""answer"",""title"":""asp.net mvc Ajax.BeginForm"",""positive_rep"":15,""negative_rep"":0,""on_date"":1270752845},{""user_id"":34796,""post_id"":2601744,""post_type"":""answer"",""title"":""SDK jar file downloading taking time in production"",""positive_rep"":10,""negative_rep"":0,""on_date"":1270744956},{""user_id"":34796,""post_id"":2595866,""post_type"":""answer"",""title"":""Passing multiple parameters in an MVC Ajax.ActionLink"",""positive_rep"":35,""negative_rep"":0,""on_date"":1270676079},{""user_id"":34796,""post_id"":2595825,""post_type"":""answer"",""title"":""ASP.NET MVC Session Expiration"",""positive_rep"":10,""negative_rep"":0,""on_date"":1270673509}]}";

        [TestMethod()]
        public void GetParametersTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor();
            Expression<Func<Reputation, bool>> expression = rep => rep.UserID == 123456;
            LambdaExpression lambdaExpression = expression as LambdaExpression;

            var queryParams = target.GetParameters(lambdaExpression);

            Assert.IsTrue(queryParams.Contains(
                new KeyValuePair<string, string>("UserID", "123456")));
        }


        [TestMethod()]
        public void BuildBaseURLTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"UserID", "123456"}};

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/reputation?key=" + target.APIKey);
        }


        [TestMethod()]
        public void BuildPagedURLTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() 
            { { "UserID", "123456" }, {"Page", "1"}, {"PageSize", "30"}};

            string url = target.BuildURL(parms);

            Assert.AreEqual(url, target.BaseUrl + "users/123456/reputation?key=" + target.APIKey + "&page=1&pagesize=30");
        }

        [TestMethod()]
        public void BuildDateRangeURLTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor();
            target.APIKey = "key";
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            Dictionary<string, string> parms = new Dictionary<string, string>() { 
                { "UserID", "123456" }, { "FromDate", "3/31/2010 00:00:00" }, { "ToDate", "4/30/2010" } };

            string url = target.BuildURL(parms);

            Assert.AreEqual<string>(url, target.BaseUrl + "users/123456/reputation?key=" + target.APIKey + "&fromdate=1269993600&todate=1272585600");
        }


        /// <summary>
        ///A test for ProcessResults
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var rep = actual.Cast<Reputation>().ToList().FirstOrDefault();
        }

        [TestMethod()]
        public void ProcessResultsForValidResponseTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor();
            var responseXml = TestHelpers.ConvertResponseToXml(m_testValidReputationResponse);
            var response = XElement.Parse(responseXml);
            IList actual = target.ProcessResults(response);
            var rep = actual.Cast<Reputation>().ToList();
            Assert.IsNotNull(rep);
        }


        [TestMethod()]
        public void NullParametersTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor() { BaseUrl = "http://api.stackoverflow.com/" };
            Dictionary<string, string> parameters = null;
            string actual;
            try
            {
                actual = target.BuildURL(parameters);
                Assert.Fail("Expected ArgumentException.");
            }
            catch (LINQToStackOverflowQueryException ae)
            {
                Assert.AreEqual<string>("UserID required for Reputation query", ae.Message);
            }
        }

        [TestMethod()]
        public void MissingUserIDTest()
        {
            ReputationRequestProcessor target = new ReputationRequestProcessor() { BaseUrl = "http://api.stackoverflow.com/" };
            Dictionary<string, string> parameters = new Dictionary<string, string>() {{"Foo", "Bar"}};
            string actual;
            try
            {
                actual = target.BuildURL(parameters);
                Assert.Fail("Expected ArgumentException.");
            }
            catch (LINQToStackOverflowQueryException ae)
            {
                Assert.AreEqual<string>("UserID required for Reputation query", ae.Message);
            }
        }
    }
}
