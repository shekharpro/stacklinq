﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Xml.Linq;
using System.Collections;


namespace LINQToStackOverflow.Test
{
    [TestClass]
    public class UserMentionsRequestProcessorTest
    {
        private string m_testInvalidValueQueryResponse = @"<invalidValue></invalidValue>";
        private string m_testValidValueQueryResponse = @"{""comments"":[{""comment_id"":2056821,""creation_date"":1264188364,""owner_user_id"":23354,""owner_display_name"":""Marc Gravell"",""reply_to_user_id"":34796,""post_id"":233505,""post_type"":""answer"",""score"":0,""body"":""@Dave - you need to start with <code>IQueryable&lt;T&gt;</code>, so if you have something like <code>List&lt;T&gt;</code> (which is <code>IEnumerable&lt;T&gt;</code>) you may need to use <code>AsQueryable()</code> - for example <code>var sorted = someList.AsQueryable().OrderBy(&quot;Foo.Bar&quot;);</code>""},{""comment_id"":2101815,""creation_date"":1264714855,""owner_user_id"":89586,""owner_display_name"":""Matthew Whited"",""reply_to_user_id"":34796,""post_id"":2158105,""post_type"":""answer"",""score"":0,""body"":""@Dave Swersky: paychecks are different then stock options.  A pay check just slow you down from jumping ship.  Stock options can make you point and laught at the guy that doesn't get them *assuming the stock is worth more then the paper it was printed on&quot;""},{""comment_id"":2385652,""creation_date"":1268090482,""owner_user_id"":10202,""owner_display_name"":""Charlino"",""reply_to_user_id"":34796,""post_id"":2405481,""post_type"":""answer"",""score"":0,""body"":""@Dave Swersky - I'm no expert on jQuery but I'm pretty sure passing in <code>{Id: 20}</code> doesn't mean that you'll get JSON in the POST values... it means that you'll have a key value pair in your post values. So therefore, ASP.NET MVC will handle it and you'll get it in your action parameter.""},{""comment_id"":2452717,""creation_date"":1268838846,""owner_user_id"":40650,""owner_display_name"":""BenAlabaster"",""reply_to_user_id"":34796,""post_id"":2463362,""post_type"":""answer"",""score"":0,""edit_count"":2,""body"":""@Dave Swersky - Yes it must, however millions of businesses have legacy internal web applications that run in IE6 some of which have no upgrade path. Consequently there will be a need for it until these applications can be upgraded/replaced allowing for newer browsers to be rolled out.""},{""comment_id"":2542216,""creation_date"":1269899596,""owner_user_id"":167149,""owner_display_name"":""Elisha"",""reply_to_user_id"":34796,""post_id"":2525565,""post_type"":""answer"",""score"":0,""body"":""@Dave Swersky, I like the Solution.IsOpen, it saves lots of code :)""},{""comment_id"":2559066,""creation_date"":1270068291,""owner_user_id"":76337,""owner_display_name"":""John Saunders"",""reply_to_user_id"":34796,""post_id"":2555626,""post_type"":""answer"",""score"":2,""body"":""@Dave: your link is the .NET 1.1 link. Please don't post those except in answer to .NET 1.1 questions. The current link is <a href=\""http://msdn.microsoft.com/en-us/library/kwdt6w2k.aspx\"" rel=\""nofollow\"">msdn.microsoft.com/en-us/library/kwdt6w2k.aspx</a>, which says, &quot;This topic is specific to a legacy technology that is retained for backward compatibility with existing applications and is not recommended for new development. Distributed applications should now be developed using the Windows Communication Foundation (WCF). Link=<a href=\""http://go.microsoft.com/fwlink/?LinkID=127777&quot\"" rel=\""nofollow\"">go.microsoft.com/fwlink/?LinkID=127777&quot</a>;""},{""comment_id"":2564328,""creation_date"":1270132698,""owner_user_id"":302918,""owner_display_name"":""juharr"",""reply_to_user_id"":34796,""post_id"":2561119,""post_type"":""answer"",""score"":0,""body"":""@Dave Sounds like the similar, but less enjoyable &quot;The 13th Floor&quot;""},{""comment_id"":2568934,""creation_date"":1270175026,""owner_user_id"":78374,""owner_display_name"":""Stefano Borini"",""reply_to_user_id"":34796,""post_id"":2561119,""post_type"":""answer"",""score"":0,""body"":""@Dave Swersky : every geek out there hoped that the end of matrix 2, with Neo killing squids with his thoughts, was a sign that even the &quot;real world&quot; of Zion was, in fact, a simulation, an additional sandbox to give free humans the illusion of being free, while instead they were still under control. The hope that Neo would have been the first one to actually escape this second sandbox was so cool that watching the third movie, with its disappointing religious implications, and its &quot;gun down everything in sight&quot; plot, was the strongest delusion I've ever had. But that's what weak minds wanted.""},{""comment_id"":2569344,""creation_date"":1270182792,""owner_user_id"":78374,""owner_display_name"":""Stefano Borini"",""reply_to_user_id"":34796,""post_id"":2561119,""post_type"":""answer"",""score"":0,""body"":""@Dave Swersky: <a href=\""http://xkcd.com/566/\"" rel=\""nofollow\"">xkcd.com/566</a> last row.""},{""comment_id"":2588877,""creation_date"":1270511547,""owner_user_id"":1574,""owner_display_name"":""Kevin Pang"",""reply_to_user_id"":34796,""post_id"":2581670,""post_type"":""question"",""score"":2,""body"":""@Dave Swersky\nBecause it's best practice to always use transactions regardless of whether you're reading or saving. See <a href=\""http://nhprof.com/Learn/Alerts/DoNotUseImplicitTransactions\"" rel=\""nofollow\"">nhprof.com/Learn/Alerts/&hellip;</a> for more information.""},{""comment_id"":2601494,""creation_date"":1270653945,""owner_user_id"":291955,""owner_display_name"":""Ryan"",""reply_to_user_id"":34796,""post_id"":2589012,""post_type"":""answer"",""score"":0,""body"":""@Dave It's possible to write an integration test that attempts to compile all of your views which would catch that error. Selenium is overkill unless you're testing behavior. I drive all my view behavior (buttons, output formatting, etc) off of conventions. If the conventions are tested, your view is tested as far as I'm concerned.""},{""comment_id"":2605577,""creation_date"":1270692788,""owner_user_id"":62653,""owner_display_name"":""andy"",""reply_to_user_id"":34796,""post_id"":2596964,""post_type"":""answer"",""score"":0,""body"":""@Dave: cool, thanks dave. So essentially, in my app, I should monitor when it reaches 10, and then wait until responses are complete before making any further requests?""},{""comment_id"":2653966,""creation_date"":1271266068,""owner_user_id"":34813,""owner_display_name"":""voyager"",""reply_to_user_id"":34796,""post_id"":2639505,""post_type"":""question"",""score"":0,""body"":""@Dave, he's got only 5 questions. You shouldn't avoid answering low acceptance rate users' questions just because of that. Keep focused on the merits of the Question on its own. Furthermore, rep primarily  comes from upvotes, not accepted answers.""},{""comment_id"":2655350,""creation_date"":1271276903,""owner_user_id"":34813,""owner_display_name"":""voyager"",""reply_to_user_id"":34796,""post_id"":2639505,""post_type"":""question"",""score"":0,""body"":""@Dave, don't worry ;)""},{""comment_id"":2712731,""creation_date"":1271945602,""owner_user_id"":208809,""owner_display_name"":""Gordon"",""reply_to_user_id"":34796,""post_id"":2691507,""post_type"":""question"",""score"":0,""edit_count"":3,""body"":""@Dave you mean like <b>ATTENDANCE MANAGEMENT SYSTEM</b>? ;)""}]}";

        [TestMethod]
        public void GetParameters()
        {
            UserMentionsRequestProcessor target = new UserMentionsRequestProcessor();
            Expression<Func<UserMentions, bool>> expression = u => u.OwnerID == 123456;
            LambdaExpression lambdaExpression = expression as LambdaExpression;
            Dictionary<string, string> parms = target.GetParameters(lambdaExpression);

            Assert.IsTrue(parms.Contains(new KeyValuePair<string, string>("OwnerID", "123456")));
        }

        [TestMethod]
        public void NullParameters()
        {
            UserMentionsRequestProcessor target = new UserMentionsRequestProcessor();
            Dictionary<string, string> parameters = null;
            string actual;
            try
            {
                actual = target.BuildURL(parameters);
                Assert.Fail("Expected ArgumentException.");
            }
            catch (LINQToStackOverflowQueryException ae)
            {
                Assert.AreEqual<string>("OwnerID required for User Mentions query", ae.Message);
            }
        }

        [TestMethod]
        public void BuildBaseURL()
        {
            //http://api.stackoverflow.com/{version}/users/{id}/mentioned
            UserMentionsRequestProcessor target = new UserMentionsRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() {{"OwnerID","123456"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(target.BaseUrl + "users/" + parms["OwnerID"] + "/mentioned?key=" + target.APIKey , url);
        }

        [TestMethod]
        public void BuildDateRangeURL()
        {
            UserMentionsRequestProcessor target = new UserMentionsRequestProcessor();
            target.BaseUrl = "http://api.stackoverflow.com/v/";
            target.APIKey = "key";
            Dictionary<string, string> parms = new Dictionary<string, string>() { { "OwnerID", "123456" }, {"FromDate", "3/31/2010"},{"ToDate","4/30/2010"} };
            string url = target.BuildURL(parms);
            Assert.AreEqual(target.BaseUrl + "users/" + parms["OwnerID"] + "/mentioned?key=" + target.APIKey + "&fromdate=1269993600&todate=1272585600", url);
        }

        [TestMethod]
        [ExpectedException(typeof(LINQToStackOverflowQueryException))]
        public void ProcessResultsForInvalidValueTest()
        {
            UserMentionsRequestProcessor target = new UserMentionsRequestProcessor();
            XElement soResponse = XElement.Parse(m_testInvalidValueQueryResponse);
            IList actual = target.ProcessResults(soResponse);
            var userMention = actual.Cast<UserMentions>().ToList().FirstOrDefault();
        }

        [TestMethod]
        public void ProcessResultsForValidValueTest()
        {
            UserMentionsRequestProcessor target = new UserMentionsRequestProcessor();
            string xmlResponse = TestHelpers.ConvertResponseToXml(m_testValidValueQueryResponse);
            XElement soResponse = XElement.Parse(xmlResponse);
            IList actual = target.ProcessResults(soResponse);
            var userMentions = actual.Cast<UserMentions>().ToList();

            Assert.AreEqual(15, userMentions.Count);


        }


    }
}
