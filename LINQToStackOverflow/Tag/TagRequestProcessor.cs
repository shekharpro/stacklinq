﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    public class TagRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public TagSortType SortBy { get; set; }
        public int UserID { get; set; }
        public OrderType Order { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<Tag>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "UserID",
                       "Page",
                       "PageSize",
                       "SortBy",
                       "Order"
                   })
                   .Parameters;
            
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = BaseUrl + "tags" + "?key=" + APIKey;

            if (parameters != null)
            {
                //route: /users/{id}/tags
                if (parameters.ContainsKey("UserID"))
                {
                    UserID = int.Parse(parameters["UserID"]);
                    url = BaseUrl + "users/" + parameters["UserID"] + "/tags?key=" + APIKey;
                }

                if (parameters.ContainsKey("SortBy"))
                {
                    SortBy = RequestProcessorHelper.ParseQueryEnumType<TagSortType>(parameters["SortBy"]);
                    switch (SortBy)
                    { 
                        case TagSortType.Activity:
                            url += "&sort=activity";
                            break;

                        case TagSortType.Name:
                            url += "&sort=name";
                            break;

                        case TagSortType.Popular:
                            url += "&sort=popular";
                            break;
                    }
                }

                if (parameters.ContainsKey("Page"))
                {
                    Page = int.Parse(parameters["Page"]);
                    url += "&page=" + Page.ToString();
                }

                if (parameters.ContainsKey("PageSize"))
                {
                    PageSize = int.Parse(parameters["PageSize"]);
                    url += "&pagesize=" + PageSize.ToString();
                }

                if (parameters.ContainsKey("Order"))
                {
                    Order = RequestProcessorHelper.ParseQueryEnumType<OrderType>(parameters["Order"]);
                    switch (Order)
                    { 
                        case OrderType.Ascending:
                            url += "&order=asc";
                            break;

                        case OrderType.Descending:
                            url += "&order=desc";
                            break;
                    }
                }

            }

            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("tags") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response.");
            }
            var tags = stackOverflowResponse.Elements("tags");
            var tagList = (from t in tags
                           select new Tag().CreateTag(t)).ToList();

            tagList.ForEach(tag =>
                {
                    tag.Page = Page;
                    tag.PageSize = PageSize;
                    tag.SortBy = SortBy;
                    tag.Order = Order;
                });
            return tagList;
        }

        public IList EmptyResult()
        {
            return new List<Tag>();
        }

        #endregion
    }
}
