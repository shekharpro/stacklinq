﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Tag
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public TagSortType SortBy { get; set; }
        public int UserID { get; set; }
        public OrderType Order { get; set; }
        
        public Tag CreateTag(XElement tag)
        {

            var name = tag.Element("name");
            var count = tag.Element("count");


            return new Tag() {Name= name.Value, Count = int.Parse(count.Value) };
        
        }
    }
}
