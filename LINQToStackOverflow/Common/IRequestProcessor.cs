﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Linq.Expressions;
using System.Xml.Linq;


namespace LINQToStackOverflow
{
    public interface IRequestProcessor
    {
        string BaseUrl { get; set; }
        string APIKey { get; set; }
        Dictionary<string, string> GetParameters(LambdaExpression lambdaExpression);
        string BuildURL(Dictionary<string, string> parameters);
        IList ProcessResults(XElement stackOverflowResponse);
        IList EmptyResult();
    }
}
