﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    public class StatsRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }

        public string APIKey { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return null;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = BaseUrl + "stats?key=" + APIKey;
            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("statistics") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response.");
            }

            var stats = stackOverflowResponse.Elements();
            var statsList = (from s in stats
                             select new Statistics().CreateStatistics(s)).ToList();
            return statsList;   
        }

        public IList EmptyResult()
        {
            return new List<Statistics>();
        }

        #endregion
    }
}
