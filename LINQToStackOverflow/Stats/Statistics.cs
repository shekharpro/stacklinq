﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Statistics
    {
        public int TotalQuestions { get; set; }
        public int TotalUnanswered { get; set; }
        public int TotalAnswers { get; set; }
        public int TotalComments { get; set; }
        public int TotalVotes { get; set; }
        public int TotalBadges { get; set; }
        public int TotalUsers { get; set; }
        public double QuestionsPerMinute { get; set; }
        public double AnswersPerMinute { get; set; }
        public double BadgesPerMinute { get; set; }
        public string APIVersion { get; set; }
    
        //"total_questions":596073,
        //"total_unanswered":85816,
        //"total_answers":1741460,
        //"total_comments":2362848,
        //"total_votes":6063508,
        //"total_badges":617442,
        //"total_users":207385,
        //"questions_per_minute":0.70,
        //"answers_per_minute":1.52,
        //"badges_per_minute":0.6,
        //"api_version":
        //{"version":"{version}",
        // "revision":"1234"}

        public Statistics CreateStatistics(XElement stats)
        {
            var totalQuestions = stats.Element("total_questions");
            var totalUnanswered = stats.Element("total_unanswered");
            var totalAnswers = stats.Element("total_answers");
            var totalComments = stats.Element("total_comments");
            var totalVotes = stats.Element("total_votes");
            var totalBadges = stats.Element("total_badges");
            var totalUsers = stats.Element("total_users");
            var questionsPerMinute = stats.Element("questions_per_minute");
            var answersPerMinute = stats.Element("answers_per_minute");
            var badgesPerMinute = stats.Element("badges_per_minute");
            var apiVersion = stats.Element("api_version");

            return new Statistics() 
            {
                TotalQuestions = int.Parse(totalQuestions.Value),
                TotalUnanswered = int.Parse(totalUnanswered.Value),
                TotalAnswers = int.Parse(totalAnswers.Value),
                TotalComments = int.Parse(totalComments.Value),
                TotalVotes = int.Parse(totalVotes.Value),
                TotalBadges = int.Parse(totalBadges.Value),
                TotalUsers = int.Parse(totalUsers.Value),
                QuestionsPerMinute = double.Parse(questionsPerMinute.Value),
                AnswersPerMinute = double.Parse(answersPerMinute.Value),
                BadgesPerMinute = double.Parse(badgesPerMinute.Value),
                APIVersion = apiVersion.Value
            };

        }

    }
}
