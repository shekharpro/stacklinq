﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQToStackOverflow
{
    public enum QuestionSortType
    {
        /// <summary>
        /// Valid for general question queries
        /// </summary>
        Active,
        /// <summary>
        /// Valid for general question queries
        /// </summary>
        Newest,
        /// <summary>
        /// Valid for general question queries
        /// </summary>
        Featured,
        /// <summary>
        /// Valid for general question queries and queries by User
        /// </summary>
        Votes,
        /// <summary>
        /// Valid for general question queries 
        /// </summary>
        Hot,
        /// <summary>
        /// Valid for general question queries
        /// </summary>
        Week,
        /// <summary>
        /// Valid for general question queries
        /// </summary>
        Month,
        /// <summary>
        /// Valid for question queries by User
        /// </summary>
        UserActivity,
        /// <summary>
        /// Valid for question queries by User
        /// </summary>
        UserViews,
        /// <summary>
        /// Valid for question queries by User
        /// </summary>
        UserCreation,
        /// <summary>
        /// Valid for question Favorite queries 
        /// </summary>
        Added

     
    }
}
