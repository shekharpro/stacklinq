﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    class QuestionTimelineRequestProcessor : IRequestProcessor
    {


        #region IRequestProcessor Members
        public virtual string BaseUrl { get; set; }
        public virtual string APIKey { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }


        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<QuestionTimeline>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "PostID",
                       "PostIDList",
                       "FromDate",
                       "ToDate",
                       "Page",
                       "PageSize"
                   })
                   .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = null;

            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("PostID is required for Question Timeline.");
            }

            if (!parameters.ContainsKey("PostID") && !parameters.ContainsKey("PostIDList"))
            {
                throw new LINQToStackOverflowQueryException("PostID is required for Question Timeline.");
            }

            //route: /questions/{id}/timeline
            if (parameters.ContainsKey("PostID"))
            {
                url = BaseUrl + "questions/" + parameters["PostID"] + "/timeline?key=" + APIKey;
            }
            if (parameters.ContainsKey("PostIDList"))
            {
                url = BaseUrl + "questions/" + parameters["PostIDList"] + "/timeline?key=" + APIKey;
            }

            if (parameters.ContainsKey("Page"))
            {
                Page = int.Parse(parameters["Page"]);
                url += "&page=" + parameters["Page"];
            }
            if (parameters.ContainsKey("PageSize"))
            {
                PageSize = int.Parse(parameters["PageSize"]);
                url += "&pagesize=" + parameters["PageSize"];
            }

            if (parameters.ContainsKey("FromDate"))
            {
                FromDate = DateTime.Parse(parameters["FromDate"]);
                double seconds = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(FromDate);
                url += "&fromdate=" + seconds;
            }
            if (parameters.ContainsKey("ToDate"))
            {
                ToDate = DateTime.Parse(parameters["ToDate"]);
                double seconds = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(ToDate);
                url += "&todate=" + seconds;
            }
            
            
            return url;

        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("post_timelines") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response.");
            }

            var responseItems = stackOverflowResponse.Elements("post_timelines");
            var questionTimelines = from xmlElement in responseItems
                                    select new QuestionTimeline().CreateQuestionTimeline(xmlElement);
            var questionTimelineList = questionTimelines.ToList();

            questionTimelineList.ForEach(questiontimeLine =>
                { 
                    questiontimeLine.FromDate = FromDate;
                    questiontimeLine.ToDate = ToDate;
                    questiontimeLine.Page = Page;
                    questiontimeLine.PageSize = PageSize;
                });
            
            return questionTimelineList;
        
        }

        public IList EmptyResult()
        {
            return new List<QuestionTimeline>();
        }

        #endregion
    }
}
