﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQToStackOverflow
{
    public enum QuestionTimelineType
    {
        Question, 
        Answer, 
        Comment, 
        Revision, 
        Votes
    }
}
