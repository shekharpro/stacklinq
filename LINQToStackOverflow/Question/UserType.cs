﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQToStackOverflow
{
    public enum UserType
    {
        anonymous,
        registered,
        unregistered,
        moderator
    }
}
