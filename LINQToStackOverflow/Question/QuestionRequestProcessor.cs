﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Xml.Linq;


namespace LINQToStackOverflow
{
    public class QuestionRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public virtual string BaseUrl { get; set; }
        public virtual string APIKey { get; set; }

        public QuestionSortType SortBy { get; set; }
        public int OwnerID { get; set; }
        public int[] OwnerIDList { get; set; }
        public bool Favorite { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public bool Unanswered { get; set; }


        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<Question>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "OwnerID",
                       "OwnerIDList",
                       "QuestionID",
                       "QuestionIDList",
                       "SortBy",
                       "Favorite",
                       "Page",
                       "PageSize",
                       "IncludeComments",
                       "IncludeBody",
                       "Added",
                       "Unanswered",
                       "FromDate",
                       "ToDate",
                       "Creation_Date",
                       "Order",
                       "Tags",
                       "Max",
                       "Min"
                   })
                   .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = null;
            //base questions query
            //route: /questions
            url = BaseUrl + "questions" + "?key=" + APIKey;
            Unanswered = false;
            Favorite = false;

            if (parameters != null)
            {
                if (parameters.ContainsKey("QuestionID") && parameters.ContainsKey("OwnerID"))
                {
                    throw new LINQToStackOverflowQueryException("QuestionID and UserID are mutually exclusive and cannot both be used in a single query.");
                }

                if (parameters.ContainsKey("Favorite") && !parameters.ContainsKey("OwnerID"))
                {
                    throw new LINQToStackOverflowQueryException("UserID must be included to query for Favorite");
                }

                //route: /questions/unanswered
                if (parameters.ContainsKey("Unanswered"))
                {
                    if (parameters["Unanswered"] == "True")
                    {
                        Unanswered = true;
                        url = BaseUrl + "questions/unanswered" + "?key=" + APIKey;
                    }
                }

                //route: /questions/{id}
                if (parameters.ContainsKey("QuestionID"))
                {
                    //single-question query
                    url = BaseUrl + "questions/" + parameters["QuestionID"] +"?key=" + APIKey;
                }
                if (parameters.ContainsKey("QuestionIDList"))
                {
                    //single-question query
                    url = BaseUrl + "questions/" + parameters["QuestionIDList"] + "?key=" + APIKey;
                }

                //Questions asked by User
                //route: /users/{id}/questions
                if ((parameters.ContainsKey("OwnerID") || parameters.ContainsKey("OwnerIDList")) && !parameters.ContainsKey("Favorite"))
                {
                    if (parameters.ContainsKey("OwnerID"))
                    {
                        OwnerID = int.Parse(parameters["OwnerID"]);
                        url = BaseUrl + "users/" + parameters["OwnerID"] + "/questions?key=" + APIKey;
                    }
                    if (parameters.ContainsKey("OwnerIDList"))
                    {
                        OwnerID = int.Parse(parameters["OwnerIDList"]);
                        url = BaseUrl + "users/" + parameters["OwnerIDList"] + "/questions?key=" + APIKey;
                    }                   
                }

                //Questions Favorited by User
                if ((parameters.ContainsKey("OwnerID") || parameters.ContainsKey("OwnerIDList")) && parameters.ContainsKey("Favorite"))
                {
                    string ownerParameter = null;
                    if (parameters.ContainsKey("OwnerID"))
                    {
                        ownerParameter = parameters["OwnerID"];
                    }
                    if (parameters.ContainsKey("OwnerIDList"))
                    {
                        ownerParameter = parameters["OwnerIDList"];
                    }

                    //route: /users/{id}/favorites
                    if (parameters["Favorite"] == "True")
                    {
                        Favorite = true;
                        url = BaseUrl + "users/" + ownerParameter + "/favorites?key=" + APIKey;
                    }
                    else
                    {
                        //Favorite explicitly set to False
                        //route: /users/{id}/questions
                        url = BaseUrl + "users/" + ownerParameter + "/questions?key=" + APIKey;
                    }   
                }

                //TODO: tagged questions API has changed
                if (parameters.ContainsKey("Tags"))
                {
                    url += "&tagged=" + parameters["Tags"];
                }

                if (parameters.ContainsKey("FromDate"))
                {
                    DateTime fromDate = DateTime.Parse(parameters["FromDate"]);
                    double seconds = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(fromDate);
                    url += "&fromdate=" + seconds;
                }

                if (parameters.ContainsKey("Max"))
                {
                    DateTime maxDate = DateTime.Parse(parameters["Max"]);
                    double seconds = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(maxDate);
                    url += "&max=" + seconds;
                }

                if (parameters.ContainsKey("Min"))
                {
                    DateTime minDate = DateTime.Parse(parameters["Min"]);
                    double seconds = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(minDate);
                    url += "&min=" + seconds;
                }

                if (parameters.ContainsKey("ToDate"))
                {
                    DateTime toDate = DateTime.Parse(parameters["ToDate"]);
                    double seconds = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(toDate);
                    url += "&todate=" + seconds;
                }

                if (parameters.ContainsKey("Order"))
                {
                    string order = RequestProcessorHelper.ParseQueryEnumType<OrderType>(parameters["Order"]).ToString();
                    switch (order)
                    { 
                        case "Ascending":
                            url += "&order=asc";
                            break;

                        case "Descending":
                            url += "&order=desc";
                            break;
                    }
                }

                //If we get here and the url has stayed the same, we're only sorting
                if (parameters.ContainsKey("SortBy"))
                {
                    SortBy = RequestProcessorHelper.ParseQueryEnumType<QuestionSortType>(parameters["SortBy"]);

                    switch (SortBy)
                    {
                        case QuestionSortType.Active:
                            url += "&sort=active";
                            break;

                        case QuestionSortType.Featured:
                            url += "&sort=featured";
                            break;

                        case QuestionSortType.Hot:
                            url += "&sort=hot";
                            break;

                        case QuestionSortType.Month:
                            url += "&sort=month";
                            break;

                        case QuestionSortType.Newest:
                            url += "&sort=newest";
                            break;

                        case QuestionSortType.Votes:
                            url += "&sort=votes";
                            break;

                        case QuestionSortType.Week:
                            url += "&sort=week";
                            break;

                        case QuestionSortType.UserActivity:
                            url += "&sort=activity";
                            break;

                        case QuestionSortType.UserCreation:
                            url += "&sort=creation";
                            break;

                        case QuestionSortType.UserViews:
                            url += "&sort=views";
                            break;

                        case QuestionSortType.Added:
                            url += "&sort=added";
                            break;
                    }
                }

                if (parameters.ContainsKey("Page"))
                {
                    Page = int.Parse(parameters["Page"]);
                    url += "&page=" + parameters["Page"];
                }

                if (parameters.ContainsKey("PageSize"))
                {
                    PageSize = int.Parse(parameters["PageSize"]);
                    url += "&pagesize=" + parameters["PageSize"];
                }

                if (parameters.ContainsKey("IncludeBody"))
                {
                    if (parameters["IncludeBody"] == "True")
                    {
                        url += "&body=true";
                    }
                }

                if (parameters.ContainsKey("IncludeComments"))
                { 
                    if (parameters["IncludeComments"] == "True")
                    {
                        url += "&comments=true";
                    }
                }
            }


            return url;
        }

        public IList ProcessResults(XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("questions") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response.");
            }
            var responseItems = stackOverflowResponse.Elements("questions");

            var questions = from xmlElement in responseItems
                            select new Question().CreateQuestion(xmlElement);
            
            var questionList = questions.ToList();

            questionList.ForEach(
                question =>
                {   question.SortBy = SortBy;
                    question.Page = Page;
                    question.PageSize = PageSize;
                    question.Favorite = Favorite;
                    question.Unanswered = Unanswered;
                });
            return questionList;
          
        }

        public IList EmptyResult()
        {
            return new List<Question>();
        }

        #endregion
    }
}
