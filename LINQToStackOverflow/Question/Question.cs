﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Question
    {
        public int AnswerCount { get; set; }
        /// <summary>
        /// Set this property to retrieve a single Question
        /// </summary>
        public int QuestionID { get; set; }
        public int[] QuestionIDList { get; set; }
        /// <summary>
        /// Set this property to retrieve all questions asked by a single User
        /// </summary>
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int OwnerID { get; set; }
        public int[] UserIDList { get; set; }
        public string Owner_Display_Name { get; set; }
        public DateTime Creation_Date { get; set; }
        public DateTime Last_Edit_Date { get; set; }
        public DateTime Last_Activity_Date { get; set; }
        public DateTime Bounty_Closes_Date { get; set; }
        public int Up_Vote_Count { get; set; }
        public int Down_Vote_Count { get; set; }
        public int Favorite_Count { get; set; }
        public int View_Count { get; set; }
        public int Score { get; set; }
        public int Accepted_Answer_ID { get; set; }
        public bool Community_Owned { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public List<Comment> Comments { get; set; }
        /// <summary>
        /// To query on Tags: q.Tags = new List(of string)() {"C#","VB",".net"};
        /// </summary>
        public string[] Tags { get; set; }
        public QuestionSortType SortBy { get; set; }
        public List<Answer> Answers { get; set; }
        public int BountyAmount { get; set; }
        public DateTime ClosedDate { get; set; }
        public string ClosedReason { get; set; }
        public DateTime LockedDate { get; set; }
        /// <summary>
        /// Set to True to return only unanswered Questions.
        /// Sort by Votes or Newest
        /// </summary>
        public bool Unanswered { get; set; }
        /// <summary>
        /// Set this property to a numerical value to retrieve the nth page of results
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Set this property to a numerical value to set the number of results retrieved in a single query.
        /// Maximum is 100.
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Set this property to true to retrieve the Question and/or Answer body
        /// </summary>
        public bool IncludeBody { get; set; }
        /// <summary>
        /// Set this property to true to retrieve the Comments
        /// </summary>
        public bool IncludeComments { get; set; }
        /// <summary>
        /// Set this property to True to retrieve questions Favorited
        /// by the User defined by OwnerID
        /// </summary>
        public bool Favorite { get; set; }
        public OrderType Order { get; set; }
        /// <summary>
        /// Minimum date range according to sort
        /// </summary>
        public DateTime Min { get; set; }
        /// <summary>
        /// Maximum date range according to sort
        /// </summary>
        public DateTime Max { get; set; }
        
        public Question CreateQuestion(XElement question)
        {
            var title = question.Element("title");
            var answerCount = question.Element("answer_count");
            var acceptedAnswerID = question.Element("accepted_answer_id") == null ? 0 : int.Parse(question.Element("accepted_answer_id").Value) ;
            var favoriteCount = question.Element("favorite_count");
            var questionID = question.Element("question_id");
            var ownerUserID = question.Element("owner_user_id") == null ? 0 : int.Parse(question.Element("owner_user_id").Value);
            var ownerDisplayName = question.Element("owner_display_name") == null ? "" : question.Element("owner_display_name").Value;
            var creationDate = long.Parse(question.Element("creation_date").Value);
            var lastEditDate = question.Element("last_edit_date") == null ? DateTime.MinValue : LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(question.Element("last_edit_date").Value));
            var lastActivityDate = long.Parse(question.Element("last_activity_date").Value);
            var upvoteCount = question.Element("up_vote_count");
            var downvoteCount = question.Element("down_vote_count");
            var viewCount = question.Element("view_count");
            var score = question.Element("score");
            var communityOwned = question.Element("community_owned");
            var body = question.Element("body") == null ? string.Empty : question.Element("body").Value;
            var bountyclosesDate = 
                question.Element("bounty_closes_date") == null 
                ? DateTime.MinValue
                : LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(question.Element("bounty_closes_date").Value));
            var bountyAmount = question.Element("bounty_amount") == null ? 0 : int.Parse(question.Element("bounty_amount").Value);
            var closedDate = question.Element("closed_date") == null ? DateTime.MinValue : LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(question.Element("closed_date").Value));
            var closedReason = question.Element("closed_reason") == null ? string.Empty : question.Element("closed_reason").Value;
            var lockedDate = question.Element("locked_date") == null ? DateTime.MinValue : LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(question.Element("locked_date").Value));

            List<Comment> comments = null;
            if (question.Elements("comments") != null)
            {
                var commentsList = question.Elements("comments");
                comments = (from c in commentsList
                           select new Comment().CreateComment(c)).ToList();
            }
            string[] tags = null;
            if (question.Elements("tags") != null)
            {
                var tagsList = question.Elements("tags");
                tags = (from t in tagsList
                        select t.Value).ToArray();
            }
            List<Answer> answers = null;
            if (question.Elements("answers") != null)
            {
                var answersList = question.Elements("answers");
                answers = (from a in answersList
                           select new Answer().CreateAnswer(a)).ToList();
            }

            return new Question() {
                Title=title.Value,
                AnswerCount = int.Parse(answerCount.Value),
                Accepted_Answer_ID = acceptedAnswerID,
                Favorite_Count = int.Parse(favoriteCount.Value),
                QuestionID = int.Parse(questionID.Value),
                OwnerID = ownerUserID,
                Owner_Display_Name = ownerDisplayName,
                Creation_Date = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(creationDate),
                Last_Edit_Date = lastEditDate,
                Last_Activity_Date = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(lastActivityDate),
                Up_Vote_Count = int.Parse(upvoteCount.Value),
                Down_Vote_Count = int.Parse(downvoteCount.Value),
                View_Count = int.Parse(viewCount.Value),
                Score = int.Parse(score.Value),
                Community_Owned = bool.Parse(communityOwned.Value),
                Body = body,
                Comments = comments,
                Tags = tags,
                Bounty_Closes_Date = bountyclosesDate,
                BountyAmount = bountyAmount,
                ClosedDate = closedDate,
                ClosedReason = closedReason,
                LockedDate = lockedDate,
                Answers = answers
            };
        }
    }
}
