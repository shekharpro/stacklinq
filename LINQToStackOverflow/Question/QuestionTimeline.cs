﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class QuestionTimeline
    {
        public QuestionTimelineType TimelineType { get; set; }
        public int PostID { get; set; }
        public int QuestionID { get; set; }
        public int[] PostIDList { get; set; }
        public int CommentID { get; set; }
        public string RevisionGuid { get; set; }
        public DateTime CreationDate { get; set; }
        public User Actor { get; set; }
        public User Owner { get; set; }
        public string Action { get; set; }
        public string Post_Revision_Url { get; set; }
        public string Post_Url { get; set; }
        public string Post_Comment_Url { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public QuestionTimeline CreateQuestionTimeline(XElement questionTimeline)
        {
            var postID = questionTimeline.Element("post_id");
            var questionID = int.Parse(questionTimeline.Element("question_id").Value);
            var commentID = questionTimeline.Element("comment_id") == null ? 0 : int.Parse(questionTimeline.Element("comment_id").Value);
            var timelineType = (QuestionTimelineType)Enum.Parse(typeof(QuestionTimelineType), questionTimeline.Element("timeline_type").Value, true);
            var revisionGuid = questionTimeline.Element("revision_guid") == null ? string.Empty : questionTimeline.Element("revision_guid").Value;
            var creationDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(questionTimeline.Element("creation_date").Value));
            User actor = null;

            if (questionTimeline.Element("user") != null)
            {
                actor = new User()
                {
                    UserID = int.Parse(questionTimeline.Element("user").Element("user_id").Value),
                    UserType = (UserType)Enum.Parse(typeof(UserType), questionTimeline.Element("user").Element("user_type").Value, true),
                    DisplayName = questionTimeline.Element("user").Element("display_name").Value,
                    Reputation = int.Parse(questionTimeline.Element("user").Element("reputation").Value),
                    EmailHash = questionTimeline.Element("user").Element("email_hash").Value
                };
            }

            User owner = null;
            if (questionTimeline.Element("owner") != null)
            {
                owner = new User()
                {
                    UserID = int.Parse(questionTimeline.Element("owner").Element("user_id").Value),
                    UserType = (UserType)Enum.Parse(typeof(UserType), questionTimeline.Element("owner").Element("user_type").Value, true),
                    DisplayName = questionTimeline.Element("owner").Element("display_name").Value,
                    Reputation = int.Parse(questionTimeline.Element("owner").Element("reputation").Value),
                    EmailHash = questionTimeline.Element("owner").Element("email_hash").Value
                };
            }
            
            var action = questionTimeline.Element("action").Value;
            var postRevisionUrl = questionTimeline.Element("post_revision_url") == null ? string.Empty : questionTimeline.Element("post_revision_url").Value;
            var postUrl = questionTimeline.Element("post_url") == null ? string.Empty : questionTimeline.Element("post_url").Value;
            var postcommentUrl = questionTimeline.Element("post_comment_url") == null ? string.Empty : questionTimeline.Element("post_comment_url").Value;


            return new QuestionTimeline() 
            {
                PostID = int.Parse(postID.Value),
                QuestionID = questionID,
                TimelineType = timelineType,
                CommentID = commentID,
                RevisionGuid = revisionGuid,
                CreationDate = creationDate,
                Actor = actor,
                Owner = owner,
                Action = action,
                Post_Revision_Url = postRevisionUrl,
                Post_Url = postUrl,
                Post_Comment_Url = postcommentUrl       
     
            };
     
        }
    }
}
