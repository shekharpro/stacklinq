﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Reputation
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int UserID { get; set; }
        public int[] UserIDList { get; set; }
        public int PostID { get; set; }
        public PostType PostType { get; set; }
        public string Title { get; set; }
        public int PositiveRep { get; set; }
        public int NegativeRep { get; set; }
        public DateTime OnDate { get; set; }

        //"post_id": 958997,
        //"post_type": "question",
        //"title": "Frame Buster Buster &hellip; buster code needed",
        //"positive_rep": 30,
        //"negative_rep": 0,
        //"on_date": 1270183901



        public Reputation CreateReputation(XElement reputation)
        {
            var postID = reputation.Element("post_id");
            var postType = reputation.Element("post_type");
            var title = reputation.Element("title");
            var posRep = reputation.Element("positive_rep");
            var negRep = reputation.Element("negative_rep");
            var onDate = reputation.Element("on_date");
            var userID = reputation.Element("user_id");

            return new Reputation() {
                PostID = int.Parse(postID.Value),
                UserID = int.Parse(userID.Value),
                PostType = (PostType)Enum.Parse(typeof(PostType), postType.Value, true),
                Title = title.Value,
                PositiveRep = int.Parse(posRep.Value),
                NegativeRep = int.Parse(negRep.Value),
                OnDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(double.Parse(onDate.Value))
            };
        
        }

    }
}
