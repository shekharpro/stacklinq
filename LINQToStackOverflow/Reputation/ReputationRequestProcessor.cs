﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    public class ReputationRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
       

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<Reputation>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "UserID",
                       "UserIDList",
                       "Page",
                       "PageSize",
                       "FromDate",
                       "ToDate"
                   })
                   .Parameters;
            
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("UserID required for Reputation query");
            }

            if (!parameters.ContainsKey("UserID") && !parameters.ContainsKey("UserIDList"))
            {
                throw new LINQToStackOverflowQueryException("UserID required for Reputation query");
            }


            string url = null;
            //route: /users/{id}/reputation
            if (parameters.ContainsKey("UserID"))
            {
                url = BaseUrl + "users/" + parameters["UserID"] + "/reputation?key=" + APIKey;
            }
            if (parameters.ContainsKey("UserIDList"))
            {
                url = BaseUrl + "users/" + parameters["UserIDList"] + "/reputation?key=" + APIKey;
            }

            if (parameters.ContainsKey("Page"))
            {
                Page = int.Parse(parameters["Page"]);
                url += "&page=" + parameters["Page"];
            }
            else
            { Page = 1; }

            if (parameters.ContainsKey("PageSize"))
            {
                PageSize = int.Parse(parameters["PageSize"]);
                url += "&pagesize=" + parameters["PageSize"];
            }
            

            if (parameters.ContainsKey("FromDate"))
            {
                FromDate = DateTime.Parse(parameters["FromDate"]);
                url += "&fromdate=" + LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(FromDate).ToString();
            }

            if (parameters.ContainsKey("ToDate"))
            {
                ToDate = DateTime.Parse(parameters["ToDate"]);
                url += "&todate=" + LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(ToDate).ToString();
            }

            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("rep_changes") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid response.");            
            }

            var reputations = stackOverflowResponse.Elements("rep_changes");

            var repList = (from r in reputations
                           select new Reputation().CreateReputation(r)).ToList();

            repList.ForEach(rep =>
                {
                    rep.Page = Page;
                    rep.PageSize = PageSize;
                    FromDate = FromDate;
                    ToDate = ToDate;
                });

            return repList;
        }

        public IList EmptyResult()
        {
            return new List<Reputation>();
        }

        #endregion
    }
}
