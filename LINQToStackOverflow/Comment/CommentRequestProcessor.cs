﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections;


namespace LINQToStackOverflow
{
    public class CommentRequestProcessor : IRequestProcessor
    {

        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public CommentSortType SortBy { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<Comment>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "CommentID",
                       "CommentIDList",
                       "QuestionID",
                       "QuestionIDList",
                       "PostIDList",
                       "AnswerID",
                       "AnswerIDList",
                       "ToID",
                       "OwnerID",
                       "OwnerIDList",
                       "Page",
                       "PageSize",
                       "FromDate",
                       "ToDate",
                       "SortBy",
                       "OrderBy"
                   })
                   .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = null;

            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("UserID or CommentID required for Comments query");
            }

            if (parameters != null)
            {
                //route: /users/{id}/comments
                if (parameters.ContainsKey("OwnerID"))
                {
                    url = BaseUrl + "users/" + parameters["OwnerID"] + "/comments";
                    //Comments directed at another user (@username)
                    //route: /users/{id}/comments/{toid}
                    if (parameters.ContainsKey("ToID"))
                    {
                        url += "/" + parameters["ToID"];
                    }
                }
                if (parameters.ContainsKey("OwnerIDList"))
                {
                    url = BaseUrl + "users/" + parameters["OwnerIDList"] + "/comments";
                    //Comments directed at another user (@username)
                    //route: /users/{id}/comments/{toid}
                    if (parameters.ContainsKey("ToID"))
                    {
                        url += "/" + parameters["ToID"];
                    }
                }

                //route: /answers/{id}/comments
                if (parameters.ContainsKey("AnswerID"))
                {
                    url = BaseUrl + "answers/" + parameters["AnswerID"] + "/comments";
                }
                if (parameters.ContainsKey("AnswerIDList"))
                {
                    url = BaseUrl + "answers/" + parameters["AnswerIDList"] + "/comments";
                }

                //route: /comments/{id}
                if (parameters.ContainsKey("CommentID"))
                {
                    url = BaseUrl + "comments/" + parameters["CommentID"];
                }
                if (parameters.ContainsKey("CommentIDList"))
                {
                    url = BaseUrl + "comments/" + parameters["CommentIDList"];
                }

                //route: /questions/{id}/comments
                if (parameters.ContainsKey("QuestionID"))
                {
                    url = BaseUrl + "questions/" + parameters["QuestionID"] + "/comments";
                }
                if (parameters.ContainsKey("QuestionIDList"))
                {
                    url = BaseUrl + "questions/" + parameters["QuestionIDList"] + "/comments";
                }

                //route: /posts/{id}/comments
                if (parameters.ContainsKey("PostIDList"))
                { 
                    url = BaseUrl + "posts/" + parameters["PostIDList"] + "/comments";
                }

                url += "?key=" + APIKey;

                if (parameters.ContainsKey("Page"))
                { 
                    Page = int.Parse(parameters["Page"]);
                    url += "&page=" + parameters["Page"];
                }

                if (parameters.ContainsKey("PageSize"))
                {
                    PageSize = int.Parse(parameters["PageSize"]);
                    url += "&pagesize=" + parameters["PageSize"];
                }

                if (parameters.ContainsKey("FromDate"))
                {
                    FromDate = DateTime.Parse(parameters["FromDate"]);
                    url += "&fromdate=" + LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(FromDate);
                }

                if (parameters.ContainsKey("ToDate"))
                {
                    ToDate = DateTime.Parse(parameters["ToDate"]);
                    url += "&todate=" + LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(ToDate);
                }

                if (parameters.ContainsKey("SortBy"))
                {
                    SortBy = RequestProcessorHelper.ParseQueryEnumType<CommentSortType>(parameters["SortBy"]);

                    switch (SortBy)
                    { 
                        case CommentSortType.Creation:
                            url += "&sort=creation";
                            break;

                        case CommentSortType.Votes:
                            url += "&sort=votes";
                            break;
                    }
                }

                if (parameters.ContainsKey("Order"))
                {
                    string order = RequestProcessorHelper.ParseQueryEnumType<OrderType>(parameters["Order"]).ToString();
                    switch (order)
                    {
                        case "Ascending":
                            url += "&order=asc";
                            break;

                        case "Descending":
                            url += "&order=desc";
                            break;
                    }
                }
            }
            return url;
        }

        public IList ProcessResults(XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("comments") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response.");
            }
            
            var comments = stackOverflowResponse.Elements("comments");

            var commentsList = (from c in comments
                                select new Comment().CreateComment(c)).ToList();

            commentsList.ForEach(comment =>
                {
                    comment.Page = Page;
                    comment.PageSize = PageSize;
                    comment.FromDate = FromDate;
                    comment.ToDate = ToDate;                
                });

            return commentsList;
            
        }

        public IList EmptyResult()
        {
            return new List<Comment>();
        }

        #endregion
    }
}
