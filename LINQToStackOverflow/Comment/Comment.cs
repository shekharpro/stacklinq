﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Comment
    {
        public int CommentID { get; set; }
        public int[] CommentIDList { get; set; }
        public int QuestionID { get; set; }
        public int[] QuestionIDList { get; set; }
        public int AnswerID { get; set; }
        public int[] AnswerIDList { get; set; }
        /// <summary>
        /// List of post IDs (questions or answers) from which to retrieve associated comments
        /// </summary>
        public int[] PostIDList { get; set; }
        public DateTime CreationDate { get; set; }
        public int PostID { get; set; }
        public string PostType { get; set; }
        public int Score { get; set; }
        public string Body { get; set; }
        /// <summary>
        /// The UserID for whom to return comments
        /// </summary>
        public int OwnerID { get; set; }
        /// <summary>
        /// List of associated Users for which to retrieve owned comments
        /// </summary>
        public int[] OwnerIDList { get; set; }
        public string OwnerName { get; set; }
        /// <summary>
        /// If the ToID value is set, the returned comments
        /// are comments from UserID "to" ToID in the "@User" format
        /// </summary>
        public int ToID { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public CommentSortType SortBy { get; set; }
        public int EditCount { get; set; }
        public OrderType Order { get; set; }
        public string OwnerUserType { get; set; }
        public string OwnerEmailHash { get; set; }
        public int OwnerReputation { get; set; }

        //<owner>
        //    <user_id>34796</user_id>
        //    <user_type>registered</user_type>
        //    <display_name>Dave Swersky</display_name>
        //    <reputation>12345</reputation>
        //    <email_hash>384019c7e3c678bdcf27803abb865e42</email_hash>
        //</owner>
        public Comment CreateComment(XElement comment)
        {
            var commentID = comment.Element("comment_id");
            var creationDate = comment.Element("creation_date");
            var ownerID = comment.Element("owner").Element("user_id");
            var ownerName = comment.Element("owner").Element("display_name");
            var userType = comment.Element("owner").Element("user_type");
            var userRep = comment.Element("owner").Element("reputation");
            var emailHash = comment.Element("owner").Element("email_hash");
            var postID = comment.Element("post_id");
            var postType = comment.Element("post_type");
            var score = comment.Element("score");
            var body = comment.Element("body");
            var toID = comment.Element("reply_to_user_id") == null ? 0 : int.Parse(comment.Element("reply_to_user_id").Value) ;
            var editCount = comment.Element("edit_count") == null ? 0 : int.Parse(comment.Element("edit_count").Value);

            return new Comment() {
                CommentID = int.Parse(commentID.Value),
                OwnerID = int.Parse(ownerID.Value),
                OwnerName = ownerName.Value,
                CreationDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(creationDate.Value)),
                PostID = int.Parse(postID.Value),
                PostType = postType.Value,
                Score = int.Parse(score.Value),
                Body = body.Value,
                ToID = toID,
                EditCount = editCount,
                OwnerEmailHash = emailHash.Value,
                OwnerUserType = userType.Value,
                OwnerReputation = int.Parse(userRep.Value)               
            };
        }
    }
}
