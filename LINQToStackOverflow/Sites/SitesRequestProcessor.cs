﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQToStackOverflow
{
    public class SitesRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl
        {
            get;
            set;
        }

        public string APIKey
        {
            get;
            set;
        }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return new Dictionary<string, string>();
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = "http://stackauth.com/1.0/sites";
            return url;
        }

        public System.Collections.IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            var sites = stackOverflowResponse.Elements("api_sites");
            var sitesList = (from s in sites
                             select new Site().CreateSite(s)).ToList();
            return sitesList;

        }

        public System.Collections.IList EmptyResult()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
