﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Styling
    {
        public string Link_Color {get;set;}
        public string Tag_Foreground_Color {get;set;}
        public string Tag_Background_Color {get;set;}
    }

    [Serializable]
    public class Site
    {
        #region ReturnFormat
        //"name": "Stack Overflow",
        //"logo_url": "http://sstatic.net/so/img/logo.png",
        //"api_endpoint": "http://api.stackoverflow.com",
        //"site_url": "http://stackoverflow.com",
        //"description": "Q&A for programmers",
        //"icon_url": "http://sstatic.net/so/apple-touch-icon.png",
        //"state": "normal",
        //"styling": {
        //  "link_color": "#0077CC",
        //  "tag_foreground_color": "#3E6D8E",
        //  "tag_background_color": "#E0EAF1"
        #endregion

        public string Name { get; set; }
        public string Logo_Url { get; set; }
        public string Api_Endpoint { get; set; }
        public string Site_Url { get; set; }
        public string Description { get; set; }
        public string Icon_Url { get; set; }
        public string State { get; set; }
        public Styling Styling { get; set; }

        public Site CreateSite(XElement site)
        {
            return new Site()
            {
                Name = site.Element("name").Value,
                Logo_Url = site.Element("logo_url").Value,
                Api_Endpoint = site.Element("api_endpoint").Value,
                Site_Url = site.Element("site_url").Value,
                Description = site.Element("description").Value,
                Icon_Url = site.Element("icon_url").Value,
                State = site.Element("state").Value,
                Styling = new Styling() { 
                    Link_Color = site.Element("styling").Element("link_color").Value,
                    Tag_Foreground_Color = site.Element("styling").Element("tag_foreground_color").Value,
                    Tag_Background_Color = site.Element("styling").Element("tag_background_color").Value 
                }
            };
        }
    }
}
