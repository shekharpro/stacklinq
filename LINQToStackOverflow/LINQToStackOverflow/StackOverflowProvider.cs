﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace LINQToStackOverflow
{
    [Serializable]
    public class StackOverflowProvider : IQueryProvider
    {
        public StackOverflowContext Context { get; set; }
        #region IQueryProvider Members

        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            return new StackOverflowQueryable<TResult>(this, expression);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            Type elementType = TypeSystem.GetElementType(expression.Type);
            try
            {
                return (IQueryable)Activator.CreateInstance(
                    typeof(StackOverflowQueryable<>)
                        .MakeGenericType(elementType),
                    new object[] { this, expression });
            }
            catch (TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        TResult IQueryProvider.Execute<TResult>(System.Linq.Expressions.Expression expression)
        {
            bool isEnumerable =
               typeof(TResult).Name == "IEnumerable`1" ||
               typeof(TResult).Name == "IEnumerable";

            return (TResult)Context.Execute<TResult>(expression, isEnumerable);
        }

        public object Execute(Expression expression)
        {
            Type elementType = TypeSystem.GetElementType(expression.Type);

            return GetType()
                .GetMethod("Execute", new Type[] { elementType })
                .Invoke(this, new object[] { expression });
        }

        #endregion
    }
}
