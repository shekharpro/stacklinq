﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace LINQToStackOverflow
{
    
    public interface IPagedResponse<TResponse> where TResponse : Response, new()
    {
        int Page { get; set; }
        int Pagesize { get; set; }
        int Total { get; set; }
        List<TResponse> Items { get; set; }
    }

    public class Response : INotifyPropertyChanged
    {
        public Response()
        {

        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
    
}
