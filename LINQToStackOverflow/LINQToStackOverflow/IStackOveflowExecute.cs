﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace LINQToStackOverflow
{
    public interface IStackOveflowExecute
    {
        /// <summary>
        /// makes HTTP call to StackOverflow API
        /// </summary>
        /// <param name="url">URL with all query info</param>
        /// <returns>List of objects to return</returns>
        IList QueryStackOverflow(string url, IRequestProcessor requestProcessor);
        int GetTotal();
        int GetPage();
        int GetPageSize();

    }
}
