﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    public class LINQToStackOverflowQueryException : Exception
    {
        public int ErrorCode { get; set; }

        public LINQToStackOverflowQueryException()
        { }

        public LINQToStackOverflowQueryException(string Message)
            : base(Message)
        { }

        public LINQToStackOverflowQueryException(string Message, Exception innerException)
            : base(Message, innerException)
        { }

        public LINQToStackOverflowQueryException CreateException(XElement ex)
        {
            var error = ex.Element("error");
            var errorCode = error.Element("code");
            var message = error.Element("message");

            return new LINQToStackOverflowQueryException("Server returned error: " + message.Value)
            {
                ErrorCode = int.Parse(errorCode.Value)
            };    

        }

    }
}
