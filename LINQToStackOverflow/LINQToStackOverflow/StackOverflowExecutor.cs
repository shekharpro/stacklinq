﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Net;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Json;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.IO.Compression;


namespace LINQToStackOverflow
{
    internal class StackOverflowExecutor : IStackOveflowExecute
    {
        public StackOverflowClient Client { get; set; }
        internal int Total { get; set; }
        internal int PageSize { get; set; }
        internal int Page { get; set; }
        private bool totalReturned = false;

        public StackOverflowExecutor(StackOverflowClient client)
        {
            Client = client;
        }
        
        #region IStackOveflowExecute Members

        /// <summary>
        /// Execute WebRequest to StackOverflow
        /// </summary>
        /// <param name="url">URL to query</param>
        /// <param name="requestProcessor"></param>
        /// <returns>IList of returned objects</returns>
        public IList QueryStackOverflow(string url, IRequestProcessor requestProcessor)
        {
            Uri uri = new Uri(url);
            WebRequest req = WebRequest.Create(uri);
            //StringReader txtRdr = null;
            req.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            HttpWebResponse rsp = null;
            string strResponse = null;
            string respXml;

            if (!string.IsNullOrEmpty(Client.ProxyAddress) && !string.IsNullOrEmpty(Client.ProxyPassword)
                && !string.IsNullOrEmpty(Client.ProxyUserName))
            {
                WebProxy proxy = new WebProxy(Client.ProxyAddress);
                proxy.Credentials = new NetworkCredential() {UserName=Client.ProxyUserName, 
                    Password=Client.ProxyPassword};
                req.Proxy = proxy;
            }

            try
            {
                rsp = (HttpWebResponse)req.GetResponse();                
                strResponse = GetStringFromResponseStream(rsp);
            }
            catch (WebException ex)
            {
                string exception = GetStringFromResponseStream(ex.Response as HttpWebResponse);
                string exceptionXml = null;
                if (!string.IsNullOrEmpty(exception))
                {
                    exceptionXml = ConvertResponseToXml(exception);
                }
                else
                {
                    exceptionXml = ex.Message;
                }
                throw new LINQToStackOverflowQueryException().CreateException(XElement.Parse(exceptionXml));
            }
            finally
            {
                if (rsp != null) { rsp.Close(); }
            }    
       
            respXml = ConvertResponseToXml(strResponse);
            XElement responseElement = XElement.Parse(respXml);
            GetHeaderData(responseElement);

            //Always process results if total is greater than 0 or no total was returned
            if (Total > 0 || !totalReturned)
            {
                return ProcessResults(requestProcessor, responseElement);
            }
            else
            {
                return requestProcessor.EmptyResult();
            }
        }

        public int GetTotal()
        {
            return Total;
        }

        public int GetPage()
        {
            return Page;
        }

        public int GetPageSize()
        {
            return PageSize;
        }

        private string GetStringFromResponseStream(HttpWebResponse rsp)
        {
            try
            {
                string result;
                Stream responseStream = null;

                if (rsp.ContentEncoding.ToLower().Contains("gzip"))
                    responseStream = new GZipStream(rsp.GetResponseStream(), CompressionMode.Decompress);
                else if (rsp.ContentEncoding.ToLower().Contains("deflate"))
                    responseStream = new DeflateStream(rsp.GetResponseStream(), CompressionMode.Decompress);
                else
                    responseStream = rsp.GetResponseStream();

                using (var strm = responseStream)
                {
                    var strmRdr = new StreamReader(responseStream);
                    var txtReader = new StringReader(strmRdr.ReadToEnd());
                    result = txtReader.ReadToEnd();
                }
                return result;
            }
            catch (Exception e)
            {
                throw new LINQToStackOverflowQueryException("Error getting response string from stream.", e);
            }
            
        }

        private string ConvertResponseToXml(string s)
        {
            try
            {
                string respXml = null;
                var stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(Convert.ToString(s)));
                XmlDictionaryReader reader = JsonReaderWriterFactory.CreateJsonReader(stream, System.Text.Encoding.UTF8, XmlDictionaryReaderQuotas.Max, null);
                var doc = (XmlDocument)JsonConvert.DeserializeXmlNode(@"{""root"": " + s + "}");
                respXml = doc.OuterXml;
                return respXml;
            }
            catch (Exception e)
            {
                throw new LINQToStackOverflowQueryException("Error converting response to XML: " + s, e);
            }
            
        }


        private static IList ProcessResults(IRequestProcessor requestProcessor, XElement responseXml)
        {
            try
            { return requestProcessor.ProcessResults(responseXml); }
            catch (Exception e)
            {
                throw new LINQToStackOverflowQueryException("Error processing results into return collection.", e);
            }   
        }


        private void GetHeaderData(XElement responseXml)
        {
            if (responseXml.Element("total") != null)
            {
                Total = int.Parse(responseXml.Element("total").Value);
                totalReturned = true;
            }
            
            if (responseXml.Element("page") != null)
            {
                Page = int.Parse(responseXml.Element("page").Value);
            }

            if (responseXml.Element("pagesize") != null)
            {
                PageSize = int.Parse(responseXml.Element("pagesize").Value);
            }

        }

        #endregion
    }
}
