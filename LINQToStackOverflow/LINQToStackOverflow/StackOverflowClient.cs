﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace LINQToStackOverflow
{
    [Serializable]
    public class StackOverflowClient
    {
        public StackOverflowClient()
        { }

        public WebRequest Get(Uri requestUrl)
        {
            WebRequest req = WebRequest.Create(requestUrl);

            if (!string.IsNullOrEmpty(ProxyUserName) && !string.IsNullOrEmpty(ProxyPassword))
            {
                WebProxy proxy = new WebProxy(ProxyAddress);
                proxy.Credentials = new NetworkCredential(ProxyUserName, ProxyPassword);
                req.Proxy = proxy;
            }
            return req;
        }

        public string ProxyUserName {get;set;}
        public string ProxyAddress { get; set; }
        public string ProxyPassword { get; set; }
    }
}
