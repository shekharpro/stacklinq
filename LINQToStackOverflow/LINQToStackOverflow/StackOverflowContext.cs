﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace LINQToStackOverflow
{
    public class StackOverflowContext
    {
        #region Private Properties
        private string _baseURL;
        private string _apikey;
        private string _currentApiVersion = "/1.0/";
        private IStackOveflowExecute StackOverflowExecute { get; set; }
        #endregion

        #region Constructors

        private StackOverflowContext()
        {
            StackOverflowExecute = new StackOverflowExecutor(new StackOverflowClient());
        }

        private StackOverflowContext(StackOverflowClient client)
            : this(string.Empty, null, client)
        { }

        public StackOverflowContext(string apikey, Site site)
            : this(apikey, site, null)
        { }

        public StackOverflowContext(string apikey, StackOverflowClient client) 
            : this(apikey, null, client)
        {
           
        }

        public StackOverflowContext(string apikey, Site site, StackOverflowClient client)
        {
            _apikey = apikey;
            client = client ?? new StackOverflowClient();
            if (site != null)
            {
                _baseURL = site.Api_Endpoint + _currentApiVersion;
            }
           
            StackOverflowExecute = new StackOverflowExecutor(client);
        }

        
        #endregion


        #region Private Methods
        /// <summary>
        /// factory method for returning a request processor
        /// </summary>
        /// <typeparam name="T">type of request</typeparam>
        /// <returns>request processor matching type parameter</returns>
        private IRequestProcessor CreateRequestProcessor(Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("Expression passed to CreateRequestProcessor must not be null.");
            }

            string requestType =
                TypeSystem
                    .GetElementType(
                        (expression as MethodCallExpression)
                        .Arguments[0].Type)
                        .Name;

            IRequestProcessor req = null;

            switch (requestType)
            {
                case "Question":
                    req = new QuestionRequestProcessor() {BaseUrl = BaseURL, APIKey=_apikey };
                    break;

                case "Answer":
                    req = new AnswerRequestProcessor() {BaseUrl= BaseURL, APIKey=_apikey };
                    break;

                case "Badge":
                    req = new BadgeRequestProcessor() {BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "Comment":
                    req = new CommentRequestProcessor() {BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "Reputation":
                    req = new ReputationRequestProcessor() {BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "Statistics":
                    req = new StatsRequestProcessor() { BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "User":
                    req = new UserRequestProcessor() {BaseUrl=BaseURL, APIKey = _apikey };
                    break;

                case "UserTimeline":
                    req = new UserTimelineRequestProcessor() {BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "Tag":
                    req = new TagRequestProcessor() {BaseUrl=BaseURL, APIKey = _apikey };
                    break;

                case "UserMentions":
                    req = new UserMentionsRequestProcessor() { BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "QuestionTimeline":
                    req = new QuestionTimelineRequestProcessor() {BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "Revision":
                    req = new RevisionRequestProcessor() { BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "Site":
                    req = new SitesRequestProcessor() { BaseUrl = BaseURL, APIKey = _apikey };
                    break;

                case "AssociatedUser":
                    req = new AssociatedUsersRequestProcessor() { BaseUrl = BaseURL, APIKey = _apikey };
                    break;
            }
            return req;
        }    

        /// <summary>
        /// Search the where clause for query parameters
        /// </summary>
        /// <param name="expression">Input query expression tree</param>
        /// <param name="reqProc">Processor specific to this request type</param>
        /// <returns>Name/value pairs of query parameters</returns>
        private static Dictionary<string, string> GetRequestParameters(Expression expression, IRequestProcessor reqProc)
        {
            Dictionary<string, string> parameters = null;

            // the where clause holds query arguments
            var whereExpression = new FirstWhereClauseFinder().GetFirstWhere(expression);

            if (whereExpression != null)
            {
                var lambdaExpression = (LambdaExpression)
                    ((UnaryExpression)(whereExpression.Arguments[1])).Operand;

                // translate variable references in expression into constants
                lambdaExpression = (LambdaExpression)Evaluator.PartialEval(lambdaExpression);

                parameters = reqProc.GetParameters(lambdaExpression);
            }

            return parameters;
        }
        
        /// <summary>
        /// Called by QueryProvider to execute queries
        /// </summary>
        /// <param name="expression">ExpressionTree to parse</param>
        /// <returns>list of objects with query results</returns>
        internal object Execute<T>(Expression expression, bool isEnumerable)
        {
            // request processor is specific to request type
            var reqProc = CreateRequestProcessor(expression);

            // get input parameters that go on the REST query URL
            var parameters = GetRequestParameters(expression, reqProc);

            // construct REST endpoint, based on input parameters
            var url = reqProc.BuildURL(parameters);

            // StackOverFlowExecutor
            var queryableList = StackOverflowExecute.QueryStackOverflow(url, reqProc);
            
            RequestTotal = StackOverflowExecute.GetTotal();
            RequestPage = StackOverflowExecute.GetPage();
            RequestPageSize = StackOverflowExecute.GetPageSize();
            
            return queryableList;
        }
        #endregion

        #region Public Properties
        public string BaseURL { 
            get {return _baseURL;}
        }

        public int RequestTotal { get; set; }
        public int RequestPage { get; set; }
        public int RequestPageSize { get; set; }

        #endregion

        #region StackOverflowQueryable Objects
        /// <summary>
        /// 
        /// </summary>
        public IQueryable<Question> Questions
        {
           get
           {
               return new StackOverflowQueryable<Question>(this);
           }
        }

        /// <summary>
        /// 
        /// </summary>
        public IQueryable<Answer> Answers
        {
            get
            {
                return new StackOverflowQueryable<Answer>(this);
            }
        }

        public IQueryable<Badge> Badges
        {
            get
            {
                return new StackOverflowQueryable<Badge>(this);
            }
        }

        public IQueryable<Comment> Comments
        {
            get
            {
                return new StackOverflowQueryable<Comment>(this);
            }
        }

        public IQueryable<Reputation> Reputation
        {
            get
            {
                return new StackOverflowQueryable<Reputation>(this);
            }
        }

        public IQueryable<Statistics> Statistics
        {
            get
            {
                return new StackOverflowQueryable<Statistics>(this);
            }
        }

        public IQueryable<User> Users
        {
            get
            {
                return new StackOverflowQueryable<User>(this);
            }
        }

        public IQueryable<UserTimeline> UserTimeline
        {
            get
            {
                return new StackOverflowQueryable<UserTimeline>(this);
            }
        }

        public IQueryable<Tag> Tags
        {
            get 
            {
                return new StackOverflowQueryable<Tag>(this);
            }
        }

        public IQueryable<UserMentions> UserMentions
        {
            get
            {
                return new StackOverflowQueryable<UserMentions>(this);
            }
        }

        public IQueryable<QuestionTimeline> QuestionTimelines
        {
            get
            {
                return new StackOverflowQueryable<QuestionTimeline>(this);
            }
        }

        public IQueryable<Revision> Revisions
        {
            get
            {
                return new StackOverflowQueryable<Revision>(this);
            }
        }

        public static IQueryable<Site> Sites(StackOverflowClient client)
        {
            return new StackOverflowQueryable<Site>(new StackOverflowContext(client));
        }

        public static IQueryable<Site> Sites()
        {
            return new StackOverflowQueryable<Site>(new StackOverflowContext());
        }

        public static IQueryable<AssociatedUser> AssociatedUsers()
        {
            return new StackOverflowQueryable<AssociatedUser>(new StackOverflowContext());
        }

        public static IQueryable<AssociatedUser> AssociatedUsers(StackOverflowClient client)
        {
            return new StackOverflowQueryable<AssociatedUser>(new StackOverflowContext(client)); 
        }

        public string ProxyAddress
        { get; set; }
        #endregion
    }
}
