﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    public class UserTimeline
    {
        public int UserID { get; set; }
        public int[] UserIDList { get; set; }
        public TimelineType TimelineType { get; set; }
        public int PostID { get; set; }
        public string PostType { get; set; }
        public int CommentID { get; set; }
        public string Action { get; set; }
        public DateTime CreateDate { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public int PageSize { get; set; }
        public int Page { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public UserTimeline CreateUserTimeline(XElement timeline)
        {
            var timelineType = timeline.Element("timeline_type");
            var postID = timeline.Element("post_id") == null ? 0 : int.Parse(timeline.Element("post_id").Value); ;
            var postType = timeline.Element("post_type") == null ? string.Empty : timeline.Element("post_type").Value;
            var commentID = timeline.Element("comment_id") == null ? 0 : int.Parse(timeline.Element("comment_id").Value);
            var action = timeline.Element("action");
            var createDate = timeline.Element("creation_date");
            var description = timeline.Element("description");
            var detail = timeline.Element("detail") == null ? "" : timeline.Element("detail").Value;

            return new UserTimeline() 
            {
                TimelineType = (TimelineType)Enum.Parse(typeof(TimelineType),timelineType.Value, true),
                PostID = postID,
                PostType = postType,
                CommentID = commentID,
                Action = action.Value,
                CreateDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(double.Parse(createDate.Value)),
                Description = description.Value,
                Detail = detail
            };
        }
    }
}
