﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace LINQToStackOverflow
{
    public class UserTimelineRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public int UserID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
              new ParameterFinder<UserTimeline>(
                  lambdaExpression.Body,
                  new List<string> { 
                       "UserID",
                       "UserIDList",
                       "FromDate",
                       "ToDate",
                       "Page",
                       "PageSize"
                   })
                  .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = null;

            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("UserID required for User Timeline");
            }
            else
            {
                if (!parameters.ContainsKey("UserID") && !parameters.ContainsKey("UserIDList"))
                {
                    throw new LINQToStackOverflowQueryException("UserID required for User Timeline");    
                }
            }

            //route: /users/{id}/timeline
            if (parameters.ContainsKey("UserID"))
            {
                url = BaseUrl + "users/" + parameters["UserID"] + "/timeline";
            }
            if (parameters.ContainsKey("UserIDList"))
            {
                url = BaseUrl + "users/" + parameters["UserIDList"] + "/timeline";
            }

            url += "?key=" + APIKey;

            if (parameters.ContainsKey("FromDate"))
            {
                FromDate = DateTime.Parse(parameters["FromDate"]);
                double fromDate = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(FromDate);
                url += "&fromdate=" + fromDate.ToString();
            }

            if (parameters.ContainsKey("ToDate"))
            {
                ToDate = DateTime.Parse(parameters["ToDate"]);
                double toDate = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(ToDate);
                url += "&todate=" + toDate.ToString();
            }

            if (parameters.ContainsKey("Page"))
            {
                url += "&page=" + parameters["Page"];
            }
            if (parameters.ContainsKey("PageSize"))
            {
                url += "&pagesize=" + parameters["PageSize"];
            }

            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("user_timelines") == null)
            { throw new LINQToStackOverflowQueryException("Invalid response."); }

            var users = stackOverflowResponse.Elements("user_timelines");
            var userList = (from u in users
                            select new UserTimeline().CreateUserTimeline(u)).ToList();

            userList.ForEach(user =>
                {
                    UserID = UserID;
                    FromDate = FromDate;
                    ToDate = ToDate;
                });

            return userList;
        }

        public IList EmptyResult()
        {
            return new List<UserTimeline>();
        }

        #endregion
    }
}
