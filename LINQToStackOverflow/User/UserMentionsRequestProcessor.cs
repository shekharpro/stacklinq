﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    public class UserMentionsRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int OwnerID { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
             new ParameterFinder<UserMentions>(
                 lambdaExpression.Body,
                 new List<string> { 
                       "OwnerID",
                       "OwnerIDList",
                       "FromDate",
                       "ToDate"
                   })
                 .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = null;
            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("OwnerID required for User Mentions query");
            }

            //route: /users/{id}/mentioned
            if (parameters.ContainsKey("OwnerID"))
            {
                url = BaseUrl + "users/" + parameters["OwnerID"] + "/mentioned?key=" + APIKey;
            }
            if (parameters.ContainsKey("OwnerIDList"))
            {
                url = BaseUrl + "users/" + parameters["OwnerIDList"] + "/mentioned?key=" + APIKey;
            }

            if (parameters.ContainsKey("FromDate"))
            {
                FromDate = DateTime.Parse(parameters["FromDate"]);
                double fromDate = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(FromDate);
                url += "&fromdate=" + fromDate.ToString();
            }
            if (parameters.ContainsKey("ToDate"))
            {
                ToDate = DateTime.Parse(parameters["ToDate"]);
                double toDate = LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(ToDate);
                url += "&todate=" + toDate.ToString();
            }

            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("comments") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response");
            }

            var userMentions = stackOverflowResponse.Elements("comments");
            var userMentionList = (from u in userMentions
                                   select new UserMentions().CreateUserMentions(u)).ToList();

            userMentionList.ForEach(userMention =>
                {
                    FromDate = FromDate;
                    ToDate = ToDate;
                });

            return userMentionList;
        }

        public IList EmptyResult()
        {
            return new List<UserMentions>();
        }

        #endregion
    }
}
