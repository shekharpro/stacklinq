﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQToStackOverflow
{
    class AssociatedUsersRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl
        {
            get;
            set;
        }

        public string APIKey
        {
            get;
            set;
        }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
             new ParameterFinder<AssociatedUser>(
                 lambdaExpression.Body,
                 new List<string> { 
                      "AssociationID"
                   })
                 .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = "";
            if (parameters.ContainsKey("AssociationID"))
            {
                url = "http://stackauth.com/users/" + parameters["AssociationID"] + "/associated";
            }
            else
            {
                throw new Exception("AssociationID required for StackAuth users query");
            }
            return url;
            
        }

        public System.Collections.IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            var users = stackOverflowResponse.Elements("associated_users");
            var userList = (from u in users
                            select new AssociatedUser().CreateAssociatedUser(u)).ToList();

            return userList;
        }

        public System.Collections.IList EmptyResult()
        {
            return new List<AssociatedUser>();
        }

        #endregion
    }
}
