﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class User
    {
        public int UserID { get; set; }
        public int[] UserIDList { get; set; }
        public int[] BadgeIDList { get; set; }
        public int[] BadgeNameList { get; set; }
        public UserType UserType { get; set; }
        public DateTime CreateDate { get; set; }
        public string DisplayName { get; set; }
        public int Reputation { get; set; }
        public string EmailHash { get; set; }
        public int Age { get; set; }
        public DateTime LastAccess { get; set; }
        public string WebSiteURL { get; set; }
        public string Location { get; set; }
        public string AboutMe { get; set; }
        public int QuestionCount { get; set; }
        public int AnswerCount { get; set; }
        public int ViewCount { get; set; }
        public int UpvoteCount { get; set; }
        public int DownvoteCount { get; set; }
        public double AcceptRate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Filter { get; set; }
        public UserSortType SortBy { get; set; }
        public OrderType Order { get; set; }
        public bool Moderator { get; set; }
        public Guid AssociationID { get; set; }
        public int BronzeBadges { get; set; }
        public int SilverBadges { get; set; }
        public int GoldBadges { get; set; }

        //"user_id":1,
        //"user_type":"moderator",
        //"creation_date":1217514151,
        //"display_name":"Jeff Atwood",
        //"reputation":12880,
        //"email_hash":"51d623f33f8b83095db84ff35e15dbe8",
        //"age":39,
        //"last_access_date":1270437003,
        //"website_url":"http://www.codinghorror.com/blog/",
        //"location":"El Cerrito, CA",
        //"about_me":"<img src=\"http://img377.imageshack.us/img377/4074/wargames1xr6.jpg\" width=\"220\">\r\n<br>\r\n<br>\r\n<a href=\"http://www.codinghorror.com/blog/archives/001169.html\" rel=\"nofollow\">Stack Overflow Valued Associate #00001</a>\r\n",
        //"question_count":15,
        //"answer_count":163,
        //"view_count":24959,
        //"up_vote_count":2019,
        //"down_vote_count":432,
        //"accept_rate":100

        public User CreateUser(XElement user)
        {
            var userID = user.Element("user_id");
            var userType = (UserType)Enum.Parse(typeof(UserType), user.Element("user_type").Value, true);
            var createDate = user.Element("creation_date");
            var displayName = user.Element("display_name");
            var reputation = user.Element("reputation");
            var emailHash = user.Element("email_hash");
            var age = user.Element("age") == null ? 0 : int.Parse(user.Element("age").Value);
            var lastAccess = user.Element("last_access_date");
            var websiteURL = user.Element("website_url") == null ? string.Empty : user.Element("website_url").Value ;
            var location = user.Element("location") == null ? string.Empty : user.Element("location").Value ;
            var aboutMe = user.Element("about_me") == null ? string.Empty : user.Element("about_me").Value;
            var questionCount = user.Element("question_count");
            var answerCount = user.Element("answer_count");
            var viewCount = user.Element("view_count");
            var upVotecount = user.Element("up_vote_count");
            var downVotecount = user.Element("down_vote_count");
            var acceptRate = user.Element("accept_rate") == null ? 0 : int.Parse(user.Element("accept_rate").Value);
            var associationId = user.Element("association_id") == null ? Guid.Empty : new Guid(user.Element("association_id").Value);
            var bronzeBadgeCount = (user.Element("badge_counts") == null || user.Element("badge_counts").Elements().Count() == 0) ? 0 : int.Parse(user.Element("badge_counts").Element("bronze").Value);
            var silverBadgeCount = (user.Element("badge_counts") == null || user.Element("badge_counts").Elements().Count() == 0) ? 0 : int.Parse(user.Element("badge_counts").Element("silver").Value);
            var goldBadgeCount = (user.Element("badge_counts") == null || user.Element("badge_counts").Elements().Count() == 0) ? 0 : int.Parse(user.Element("badge_counts").Element("gold").Value);

            return new User() 
            { 
                UserID = int.Parse(userID.Value),
                UserType = userType,
                CreateDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(double.Parse(createDate.Value)),
                DisplayName = displayName.Value,
                Reputation =  int.Parse(reputation.Value),
                EmailHash = emailHash.Value,
                Age = age,
                LastAccess = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(double.Parse(lastAccess.Value)),
                WebSiteURL = websiteURL,
                Location = location,
                AboutMe = aboutMe,
                QuestionCount = int.Parse(questionCount.Value),
                AnswerCount = int.Parse(answerCount.Value),
                ViewCount = int.Parse(viewCount.Value),
                UpvoteCount = int.Parse(upVotecount.Value),
                DownvoteCount = int.Parse(downVotecount.Value),
                AcceptRate = acceptRate,
                AssociationID = associationId,
                BronzeBadges = bronzeBadgeCount,
                SilverBadges = silverBadgeCount,
                GoldBadges = goldBadgeCount
            };
        }
    }
}
