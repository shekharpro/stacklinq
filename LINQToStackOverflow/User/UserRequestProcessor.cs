﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    public class UserRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public bool TimeLine { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Filter { get; set; }
        public UserSortType SortBy { get; set; }
        public bool Moderator { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<User>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "UserID",
                       "UserIDList",
                       "BadgeIDList",
                       "TimeLine",
                       "Mentioned",
                       "Page",
                       "PageSize",
                       "Filter",
                       "SortBy",
                       "Order",
                       "Moderator"
                   })
                   .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            //route: /users
            string url = BaseUrl + "users";
            TimeLine = false;

            if (parameters == null)
            { url += "?key=" + APIKey; }
            else
            {
                Moderator = false;
                if (parameters.ContainsKey("moderators"))
                {
                    Moderator = true;
                    url = BaseUrl + "users/moderators";
                }

                if (parameters.ContainsKey("UserID") || parameters.ContainsKey("UserIDList") || parameters.ContainsKey("BadgeIDList"))
                {
                    //Single User
                    //route: /user/{id}
                    if (parameters.ContainsKey("UserID"))
                        url = BaseUrl + "users/" + parameters["UserID"];
                    //Vectorized UserID list
                    if (parameters.ContainsKey("UserIDList"))
                        url = BaseUrl + "users/" + parameters["UserIDList"];

                    //Vectorized BadgeID list
                    //route: /badges/{id}
                    if (parameters.ContainsKey("BadgeIDList"))
                        url = BaseUrl + "badges/" + parameters["BadgeIDList"];
                  
                    if (parameters.ContainsKey("TimeLine"))
                    {
                        if (parameters["TimeLine"] == "True")
                        {
                            TimeLine = true;
                            //Single User Timeline
                            url += "/timeline";
                        }
                    }
                }

                url += "?key=" + APIKey;
                if (parameters.ContainsKey("Page"))
                {
                    url += "&page=" + parameters["Page"];
                }

                if (parameters.ContainsKey("PageSize"))
                {
                    url += "&pagesize=" + parameters["PageSize"];
                }

                if (parameters.ContainsKey("Filter"))
                {
                    url += "&filter=" + parameters["Filter"];
                }

                if (parameters.ContainsKey("SortBy"))
                {
                    SortBy = RequestProcessorHelper.ParseQueryEnumType<UserSortType>(parameters["SortBy"]);

                    switch (SortBy)
                    { 
                        case UserSortType.Creation:
                            url += "&sort=creation";
                            break;

                        case UserSortType.Name:
                            url += "&sort=name";
                            break;

                        case UserSortType.Reputation:
                            url += "&sort=reputation";
                            break;
                    }
                }

                if (parameters.ContainsKey("Order"))
                {
                    string order = RequestProcessorHelper.ParseQueryEnumType<OrderType>(parameters["Order"]).ToString();

                    switch (order)
                    {
                        case "Ascending":
                            url += "&order=asc";
                            break;

                        case "Descending":
                            url += "&order=desc";
                            break;
                    }
                }
            }

            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("users") == null)
            { throw new LINQToStackOverflowQueryException("Invalid Response."); }

            var users = stackOverflowResponse.Elements("users");
            var userList = (from u in users
                            select new User().CreateUser(u)).ToList();



            userList.ForEach(user =>
                {
                    TimeLine = TimeLine;
                    Moderator = Moderator;
                });
            return userList;
            //return new Response<User>() { Items = userList };
        }


        public IList EmptyResult()
        {
            return new List<User>();
        }
        #endregion
    }
}
