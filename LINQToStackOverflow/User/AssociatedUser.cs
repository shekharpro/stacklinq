﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class AssociatedUser
    {
        #region ReturnFormat
        //"user_id": 34796,
        //"user_type": "registered",
        //"display_name": "Dave Swersky",
        //"reputation": 12681,
        //"on_site": {
        //"name": "Stack Overflow",
        //"logo_url": "http://sstatic.net/so/img/logo.png",
        //"api_endpoint": "http://api.stackoverflow.com",
        //"site_url": "http://stackoverflow.com",
        //"description": "Q&A for programmers",
        //"icon_url": "http://sstatic.net/so/apple-touch-icon.png",
        //"state": "normal",
        //"styling": {
        //"link_color": "#0077CC",
        //"tag_foreground_color": "#3E6D8E",
        //"tag_background_color": "#E0EAF1"
        //}
        //},
        //"email_hash": "9deb91da08d8ec815376a517793eccca"
        #endregion

        public int UserID { get; set; }
        public string UserType { get; set; }
        public string DisplayName { get; set; }
        public int Reputation { get; set; }
        public Site Site { get; set; }
        public Guid AssociationID { get; set; }
        public User SiteUser { get; set; }

        public AssociatedUser CreateAssociatedUser(XElement user)
        {
            return new AssociatedUser()
            {
                UserID = int.Parse(user.Element("user_id").Value),
                UserType = user.Element("user_type").Value,
                DisplayName = user.Element("display_name").Value,
                Reputation = int.Parse(user.Element("reputation").Value),
                Site = new Site()
                {
                    Name = user.Element("on_site").Element("name").Value,
                    Logo_Url = user.Element("on_site").Element("logo_url").Value,
                    Api_Endpoint = user.Element("on_site").Element("api_endpoint").Value,
                    Site_Url = user.Element("on_site").Element("site_url").Value,
                    Description = user.Element("on_site").Element("description").Value,
                    Icon_Url = user.Element("on_site").Element("icon_url").Value,
                    State = user.Element("on_site").Element("state").Value,
                    Styling = new Styling() {
                        Link_Color = user.Element("on_site").Element("styling").Element("link_color").Value,
                        Tag_Foreground_Color = user.Element("on_site").Element("styling").Element("tag_foreground_color").Value,
                        Tag_Background_Color = user.Element("on_site").Element("styling").Element("tag_background_color").Value
                    }
                }
            };
        }
    }
}
