﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    public class UserMentions
    {
        public int CommentID { get; set; }
        public DateTime CreateDate { get; set; }
        public int OwnerID { get; set; }
        public int[] OwnerIDList { get; set; }
        public string OwnerDisplayName { get; set; }
        public string OwnerType { get; set; }
        public int OwnerReputation { get; set; }
        public string OwnerEmailHash { get; set; }
        
        public int PostID { get; set; }
        public string PostType { get; set; }
        public int Score { get; set; }
        public string Body { get; set; }

        public int ReplyOwnerID { get; set; }
        public string ReplyOwnerName { get; set; }
        public string ReplyOwnerType { get; set; }
        public int ReplyOwnerReputation { get; set; }
        public string ReplyOwnerEmailHash { get; set; }

        //<comments>
        //  <comment_id>2955968</comment_id>
        //  <creation_date>1274800297</creation_date>
        //  <owner>
        //    <user_id>184882</user_id>
        //    <user_type>registered</user_type>
        //    <display_name>Pino</display_name>
        //    <reputation>1838</reputation>
        //    <email_hash>9dd799b17031d3cf201740612d6931b5</email_hash>
        //  </owner>
        //  <reply_to_user>
        //    <user_id>34796</user_id>
        //    <user_type>registered</user_type>
        //    <display_name>Dave Swersky</display_name>
        //    <reputation>12345</reputation>
        //    <email_hash>384019c7e3c678bdcf27803abb865e42</email_hash>
        //  </reply_to_user>
        //  <post_id>2905874</post_id>
        //  <post_type>question</post_type>
        //  <score>0</score>
        //  <body>@Dave Swersky - Return type is ProductOptionModel </body>
        //</comments>

        public UserMentions CreateUserMentions(XElement usermentions)
        {
            var commentID = usermentions.Element("comment_id");
            var createDate = usermentions.Element("creation_date");
            var postID = usermentions.Element("post_id");
            var postType = usermentions.Element("post_type");
            var score = usermentions.Element("score");
            var body = usermentions.Element("body");

            var ownerID = usermentions.Element("owner").Element("user_id");
            var ownerName = usermentions.Element("owner").Element("display_name");
            var userType = usermentions.Element("owner").Element("user_type");
            var userRep = usermentions.Element("owner").Element("reputation");
            var emailHash = usermentions.Element("owner").Element("email_hash");

            var replyownerID = usermentions.Element("reply_to_user").Element("user_id");
            var replyownerName = usermentions.Element("reply_to_user").Element("display_name");
            var replyuserType = usermentions.Element("reply_to_user").Element("user_type");
            var replyuserRep = usermentions.Element("reply_to_user").Element("reputation");
            var replyemailHash = usermentions.Element("reply_to_user").Element("email_hash");


            return new UserMentions() 
            {
                CommentID = int.Parse(commentID.Value),
                CreateDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(double.Parse(createDate.Value)),
                OwnerID = int.Parse(ownerID.Value),
                OwnerDisplayName = ownerName.Value,
                OwnerType = userType.Value,
                OwnerReputation = int.Parse(userRep.Value),
                OwnerEmailHash = emailHash.Value,
                ReplyOwnerID = int.Parse(replyownerID.Value),
                ReplyOwnerName = replyownerName.Value,
                ReplyOwnerType = replyuserType.Value,
                ReplyOwnerReputation = int.Parse(replyuserRep.Value),
                ReplyOwnerEmailHash = replyemailHash.Value,
                PostID = int.Parse(postID.Value),
                PostType = postType.Value,
                Score = int.Parse(score.Value),
                Body = body.Value
            };
        }
    }
}
