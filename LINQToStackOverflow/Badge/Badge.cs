﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Badge
    {
        public int BadgeID { get; set; }
        public string Rank { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AwardCount { get; set; }
        public bool TagBased { get; set; }
        public int UserID { get; set; }
        
       
        public Badge CreateBadge(XElement badge)
        {
            var badgeID = badge.Element("badge_id");
            var rank = badge.Element("rank");
            var name = badge.Element("name");
            var description = badge.Element("description");
            var awardCount = badge.Element("award_count");
            var tagBased = badge.Element("tag_based");
            
            return new Badge() 
            {
                BadgeID = int.Parse(badgeID.Value),
                Rank = rank.Value,
                Name = name.Value,
                Description = description.Value,
                AwardCount = int.Parse(awardCount.Value),
                TagBased = bool.Parse(tagBased.Value)
            };
        }
    }
}
