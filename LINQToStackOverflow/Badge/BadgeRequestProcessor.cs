﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    public class BadgeRequestProcessor : IRequestProcessor
    {

        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }
        public int UserID { get; set; }
       
        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<Badge>(
                   lambdaExpression.Body,
                   new List<string> { 
                        "TagBased",
                        "UserID"           
                   })
                   .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            //route: /badges
            string url = BaseUrl + "/badges";
            if (parameters != null)
            {
                if (parameters.ContainsKey("TagBased"))
                {
                    //route: /badges/tags
                    if (parameters["TagBased"] == "True")
                    {
                        
                        url += "/tags";   
                    }
                    else
                    //route: /badges/name
                    if (parameters["TagBased"] == "False")
                    {
                        url += "/name";   
                    }
                }

                //route: /users/{id}/badges
                if (parameters.ContainsKey("UserID"))
                {
                    UserID = int.Parse(parameters["UserID"]);
                    url = BaseUrl + "users/" + parameters["UserID"] + "/badges";
                }
            }
            url += "?key=" + APIKey;
            return url;
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("badges") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid Response.");
            }
            var badges = stackOverflowResponse.Elements("badges");

            var badgeList = (from b in badges
                             select new Badge().CreateBadge(b)).ToList();

            badgeList.ForEach(badge => { badge.UserID = UserID; });

            return badgeList;
        }

        public IList EmptyResult()
        {
            return new List<Badge>();
        }

        #endregion
    }
}
