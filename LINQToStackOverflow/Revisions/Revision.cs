﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    public class Revision
    {
        #region ReturnFormat
        //        {
//  "revisions": [
//    {
//      "body": {
//        "description": "body of the revision",
//        "values": "string",
//        "optional": true,
//        "suggested_buffer_size": 30000
//      },
//      "comment": {
//        "description": "comment on the revision",
//        "values": "string",
//        "optional": false,
//        "suggested_buffer_size": 400
//      },
//      "creation_date": {
//        "description": "when the revision occurred",
//        "values": "unix epoch date, range [0, 253,402,300,799]",
//        "optional": false
//      },
//      "is_question": {
//        "description": "if the post is a question",
//        "values": "boolean",
//        "optional": false
//      },
//      "is_rollback": {
//        "description": "if the revision is a rollback",
//        "values": "boolean",
//        "optional": false
//      },
//      "last_body": {
//        "description": "the previous body",
//        "values": "string",
//        "optional": true,
//        "suggested_buffer_size": 30000
//      },
//      "last_title": {
//        "description": "the previous title",
//        "values": "string",
//        "optional": true,
//        "suggested_buffer_size": 200
//      },
//      "last_tags": [
//        {
//          "values": "string"
//        }
//      ],
//      "revision_guid": {
//        "description": "the id of the revision",
//        "values": "guid, 8-4-4-4-12 format",
//        "optional": false
//      },
//      "revision_number": {
//        "description": "the revision number",
//        "values": "32-bit signed integer",
//        "optional": true
//      },
//      "tags": [
//        {
//          "values": "string"
//        }
//      ],
//      "title": {
//        "description": "the title of the revision",
//        "values": "string",
//        "optional": true,
//        "suggested_buffer_size": 200
//      },
//      "revision_type": {
//        "description": "the revision type",
//        "values": "one of single_user, or vote_based",
//        "optional": false
//      },
//      "set_community_wiki": {
//        "description": "if this revision set the post to community wiki",
//        "values": "boolean",
//        "optional": false
//      },
//      "user": {
//        "user_id": {
//          "description": "id of the user",
//          "values": "32-bit signed integer",
//          "optional": false
//        },
//        "user_type": {
//          "description": "type of the user",
//          "values": "one of anonymous, unregistered, registered, or moderator",
//          "optional": false
//        },
//        "display_name": {
//          "description": "displayable name of the user",
//          "values": "string",
//          "optional": false,
//          "suggested_buffer_size": 40
//        },
//        "reputation": {
//          "description": "reputation of the user",
//          "values": "32-bit signed integer",
//          "optional": false
//        },
//        "email_hash": {
//          "description": "email hash, suitable for fetching a gravatar",
//          "values": "string",
//          "optional": false,
//          "suggested_buffer_size": 32
//        }
//      },
//      "post_id": {
//        "description": "the post the revision is of",
//        "values": "32-bit signed integer",
//        "optional": false
//      }
//    }
//  ]
        //}
        #endregion

        public string Body { get; set; }
        public string Comment { get; set; }
        public DateTime CreationDate { get; set; }
        public bool isQuestion { get; set; }
        public bool isRollback { get; set; }
        public string LastBody { get; set; }
        public string LastTitle { get; set; }
        public List<string> LastTags { get; set; }
        public Guid RevisionGuid { get; set; }
        public int RevisionNumber { get; set; }
        public List<string> Tags { get; set; }
        public string Title { get; set; }
        public RevisionType RevisionType { get; set; }
        public bool CommunityWiki { get; set; }
        public User User { get; set; }
        public int PostID { get; set; }

        public Revision CreateRevision(XElement revision)
        {
            var body = revision.Element("body") == null ?  string.Empty : revision.Element("body").Value;
            var comment = revision.Element("comment").Value;
            var creationDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(int.Parse(revision.Element("creation_date").Value));
            var isquestion = bool.Parse(revision.Element("is_question").Value);
            var isrollback = bool.Parse(revision.Element("is_rollback").Value);
            var lastTitle = revision.Element("last_title") == null ? string.Empty : revision.Element("last_title").Value;
            var lastBody = revision.Element("last_body") == null ? string.Empty : revision.Element("last_body").Value;
           
            var lasttags = revision.Element("last_tags") == null ? new List<string>() : (from e in revision.Element("last_tags").Elements()
                                                                                         select e.Value).ToList();
            var revisionguid = new Guid(revision.Element("revision_guid").Value);
            var revisionNumber = revision.Element("revision_number") == null ? 0 : int.Parse(revision.Element("revision_number").Value);
            var tags = (from e in revision.Elements("tags").Elements()
                        select e.Value).ToList();
            var title = revision.Element("title") == null ? string.Empty : revision.Element("title").Value;
            var revisionType = (RevisionType)Enum.Parse(typeof(RevisionType), revision.Element("revision_type").Value, true);
            var setWiki = bool.Parse(revision.Element("set_community_wiki").Value);
            User user = new User() 
                        {
                            UserID = int.Parse(revision.Element("user").Element("user_id").Value),
                            UserType = (UserType)Enum.Parse(typeof(UserType), revision.Element("user").Element("user_type").Value, true),
                            DisplayName = revision.Element("user").Element("display_name").Value,
                            Reputation = int.Parse(revision.Element("user").Element("reputation").Value),
                            EmailHash = revision.Element("user").Element("email_hash").Value
                        };
            var postid = int.Parse(revision.Element("post_id").Value);

            return new Revision() 
            {
                Body = body,
                Comment = comment,
                CreationDate = creationDate,
                isQuestion = isquestion,
                isRollback = isrollback,
                LastTitle = lastTitle,
                LastTags = lasttags,
                RevisionGuid = revisionguid,
                RevisionNumber = revisionNumber,
                Tags = tags,
                Title = title,
                RevisionType = revisionType,
                CommunityWiki = setWiki,
                User = user,
                PostID = postid
            };
        }

    }
}
