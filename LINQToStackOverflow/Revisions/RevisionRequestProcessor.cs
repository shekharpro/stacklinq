﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace LINQToStackOverflow
{
    class RevisionRequestProcessor : IRequestProcessor
    {
        public string BaseUrl { get; set; }
        public string APIKey { get; set; }

        public Dictionary<string, string> GetParameters(System.Linq.Expressions.LambdaExpression lambdaExpression)
        {
            return
              new ParameterFinder<Revision>(
                  lambdaExpression.Body,
                  new List<string> { 
                      "PostID",
                      "PostIDList",
                      "FromDate",
                      "ToDate"
                   })
                  .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = null;

            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("PostID required for revisions query.");
            }

            url = BaseUrl + "revisions/";

            if (parameters.ContainsKey("PostID"))
            {
                url += parameters["PostID"];
            }
            if (parameters.ContainsKey("PostIDList")) 
            {
                url += parameters["PostIDList"];
            }

            if (parameters.ContainsKey("FromDate"))
            {
                url += "&fromdate=" + LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(DateTime.Parse(parameters["FromDate"])).ToString();
            }
            if (parameters.ContainsKey("ToDate"))
            {
                url += "&todate=" + LINQToStackOverflowQueryHelper.ConvertToUnixTimestamp(DateTime.Parse(parameters["ToDate"])).ToString();
            }


            url += "?key=" + APIKey;

            return url;
                
        }

        public IList ProcessResults(System.Xml.Linq.XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("revisions") == null)
            { throw new LINQToStackOverflowQueryException("Invalid Response."); }

            var revisions = stackOverflowResponse.Elements("revisions");
            var revisionList = (from r in revisions
                                select new Revision().CreateRevision(r)).ToList();

            return revisionList;
        }


        public IList EmptyResult()
        {
            return new List<Revision>();
        }
    }
}
