﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Collections;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    public class AnswerRequestProcessor : IRequestProcessor
    {
        #region IRequestProcessor Members

        public string BaseUrl { get; set; }
        public string APIKey { get; set; }

        public int Page { get; set; }
        public int PageSize { get; set; }


        public Dictionary<string, string> GetParameters(LambdaExpression lambdaExpression)
        {
            return
               new ParameterFinder<Answer>(
                   lambdaExpression.Body,
                   new List<string> { 
                       "OwnerID",
                       "OwnerIDList",
                       "AnswerID",
                       "AnswerIDList",
                       "QuestionID",
                       "QuestionIDList",
                       "IncludeBody",
                       "IncludeComments",
                       "Page",
                       "PageSize"
                   })
                   .Parameters;
        }

        public string BuildURL(Dictionary<string, string> parameters)
        {
            string url = string.Empty;

            if (parameters == null)
            {
                throw new LINQToStackOverflowQueryException("At least one selection parameter (AnswerID or OwnerID) required.");
            }

            if ((parameters.ContainsKey("OwnerID") && parameters.ContainsKey("AnswerID")) || 
                (parameters.ContainsKey("OwnerIDList") && parameters.ContainsKey("AnswerIDList")))
            { throw new LINQToStackOverflowQueryException("OwnerID and AnswerID cannot both be used in a single query"); }

            //By OwnerID(s)
            //route: /users/{id}/answers
            if (parameters.ContainsKey("OwnerID"))
            {
                url = BaseUrl + "users/" + parameters["OwnerID"].ToString() + "/answers" + "?key=" + APIKey;
            }

            if (parameters.ContainsKey("OwnerIDList"))
            {
                url = BaseUrl + "users/" + parameters["OwnerIDList"].ToString() + "/answers" + "?key=" + APIKey;
            }

            //By AnswerID(s)
            ///route: answers/{id} 
            if (parameters.ContainsKey("AnswerID"))
            {
                url = BaseUrl + "answers/" + parameters["AnswerID"] + "?key=" + APIKey;
            }


            if (parameters.ContainsKey("AnswerIDList"))
            {
                url = BaseUrl + "answers/" + parameters["AnswerIDList"] + "?key=" + APIKey;
            }

            //By QuestionID(s)
            //route: /questions/{id}/answers
            if (parameters.ContainsKey("QuestionIDList") || parameters.ContainsKey("QuestionID"))
            {
                if (parameters.ContainsKey("QuestionIDList"))
                    url = BaseUrl + "questions/" + parameters["QuestionIDList"] + "/answers?key=" + APIKey;

                if (parameters.ContainsKey("QuestionID"))
                    url = BaseUrl + "questions/" + parameters["QuestionID"] + "/answers?key=" + APIKey;
            }

            if (parameters.ContainsKey("IncludeComments"))
            {
                if (parameters["IncludeComments"] == "True")
                {
                    url += "&comments=true";
                }
            }

            if (parameters.ContainsKey("IncludeBody"))
            {
                if (parameters["IncludeBody"] == "True")
                {
                    url += "&body=true";
                }
            }

            if (parameters.ContainsKey("Page"))
            {
                Page = int.Parse(parameters["Page"]);
                url += "&page=" + parameters["Page"];
            }

            if (parameters.ContainsKey("PageSize"))
            {
                PageSize = int.Parse(parameters["PageSize"]);
                url += "&pagesize=" + parameters["PageSize"];
            }
            
            return url;
        }

        public IList ProcessResults(XElement stackOverflowResponse)
        {
            if (stackOverflowResponse.Element("answers") == null)
            {
                throw new LINQToStackOverflowQueryException("Invalid response");
            }

            var responseItems = stackOverflowResponse.Elements("answers");
            var answerList = (from a in responseItems
                              select new Answer().CreateAnswer(a)).ToList();

            answerList.ForEach(
                    answer =>
                    {
                        answer.Page = Page;
                        answer.PageSize = PageSize;
                    }
                );


            return answerList;
        }

        public IList EmptyResult()
        {
            return new List<Answer>();
        }

        #endregion
    }
}
