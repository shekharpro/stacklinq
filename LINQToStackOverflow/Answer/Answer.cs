﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LINQToStackOverflow
{
    [Serializable]
    public class Answer
    {
        public int AnswerID { get; set; }
        public int[] AnswerIDList { get; set; }
        public bool Accepted { get; set; }
        public int QuestionID { get; set; }
        public int[] QuestionIDList { get; set; }
        public int OwnerID { get; set; }
        public int[] OwnerIDList { get; set; }
        public string OwnerName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastEditDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
        public int Views { get; set; }
        public int Score { get; set; }
        public bool CommunityOwned { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public List<Comment> Comments { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public bool IncludeBody { get; set; }
        public bool IncludeComments { get; set; }
                

        public Answer CreateAnswer(XElement answer)
        {
            var answerID = answer.Element("answer_id");
            var accepted = answer.Element("accepted");
            var questionID = answer.Element("question_id");
            var ownerID = answer.Element("owner_user_id") == null ? 0 : int.Parse(answer.Element("owner_user_id").Value);
            var ownerName = answer.Element("owner_display_name") == null ? "" : answer.Element("owner_display_name").Value;
            var creationDate = LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(answer.Element("creation_date").Value));
            var lasteditDate = answer.Element("last_edit_date") == null ? DateTime.MinValue :
                LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(answer.Element("last_edit_date").Value));
            var lastactivityDate = answer.Element("last_activity_date") == null ? DateTime.MinValue : LINQToStackOverflowQueryHelper.ConvertFromUnixTimestamp(long.Parse(answer.Element("last_activity_date").Value));
            var upvotes = answer.Element("up_vote_count");
            var downvotes = answer.Element("down_vote_count");
            var views = answer.Element("view_count");
            var score = answer.Element("score");
            var communityOwned = answer.Element("community_owned");
            var title = answer.Element("title");
            var body = answer.Element("body") == null ? string.Empty : answer.Element("body").Value;
            List<Comment> comments = null;
            if (answer.Element("comments") != null)
            {
                var commentsList = answer.Elements("comments");
                comments = (from c in commentsList
                            select new Comment().CreateComment(c)).ToList();
            }

            return new Answer() {
                AnswerID = int.Parse(answerID.Value),
                Accepted = bool.Parse(accepted.Value),
                QuestionID = int.Parse(questionID.Value),
                OwnerID = ownerID,
                OwnerName = ownerName,
                CreationDate = creationDate,
                LastEditDate = lasteditDate,
                LastActivityDate = lastactivityDate,
                UpVotes = int.Parse(upvotes.Value),
                DownVotes = int.Parse(downvotes.Value),
                Views = int.Parse(views.Value),
                Score = int.Parse(score.Value),
                CommunityOwned = bool.Parse(communityOwned.Value),
                Title = title.Value,
                Body = body,
                Comments = comments
            };
        }
    }
}
