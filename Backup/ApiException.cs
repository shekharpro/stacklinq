//  
//  Project: SOAPI
//  http://soapi.info
// 
//  Copyright 2010, Sky Sanders
//  Dual licensed under the MIT or GPL Version 2 licenses.
//  http://soapi.codeplex.com/license
//  
//  Date: June 10 2010 
//  
//  
using System;
using System.Net;
using System.Text.RegularExpressions;

namespace Soapi
{
    ///<summary>
    /// Wraps exceptions that occur in Soapi and surfaces any errors returned from the Stack Overflow API
    ///</summary>
    public class ApiException : Exception
    {
        private readonly HttpStatusCode _statusCode;
        private readonly string _url;

        ///<summary>
        ///</summary>
        ///<param name="message"></param>
        ///<param name="statusCode"></param>
        ///<param name="url"></param>
        ///<param name="innerException"></param>
        public ApiException(string message, HttpStatusCode statusCode, string url, Exception innerException)
            : base(String.Format("{0}\r\nStatusCode:{1}\r\nUrl:{2}", message, statusCode, url), innerException)
        {
            _url = url;
            _statusCode = statusCode;
        }

        ///<summary>
        ///</summary>
        ///<param name="message"></param>
        ///<param name="statusCode"></param>
        ///<param name="innerException"></param>
        public ApiException(string message, HttpStatusCode statusCode, Exception innerException)
            : base(String.Format("{0}\r\nStatusCode:{1}", message, statusCode), innerException)
        {
            _statusCode = statusCode;
        }

        ///<summary>
        ///</summary>
        ///<param name="message"></param>
        ///<param name="innerException"></param>
        public ApiException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        ///<summary>
        ///</summary>
        ///<param name="message"></param>
        public ApiException(string message)
            : base(message)
        {
        }

        ///<summary>
        ///</summary>
        public HttpStatusCode StatusCode
        {
            get { return _statusCode; }
        }


        ///<summary>
        ///</summary>
        public string Url
        {
            get { return _url; }
        }

        ///<summary>
        ///</summary>
        ///<param name="innerException"></param>
        ///<returns></returns>
        public static ApiException Create(Exception innerException)
        {
            if (innerException is ApiException)
            {
                return (ApiException) innerException;
            }
            if (innerException is WebException)
            {
                return Create(((WebException) innerException), null);
            }
            return Create(innerException, null);
        }

        ///<summary>
        ///</summary>
        ///<param name="innerException"></param>
        ///<param name="url"></param>
        ///<returns></returns>
        public static ApiException Create(Exception innerException, string url)
        {
            if (innerException is ApiException)
            {
                return Create(((ApiException) innerException), url);
            }
            if (innerException is WebException)
            {
                return Create(((WebException) innerException), url);
            }

            string message = innerException.Message;
            return new ApiException(message, HttpStatusCode.NotHttpError, url, innerException);
        }

        ///<summary>
        ///</summary>
        ///<param name="innerException"></param>
        ///<param name="url"></param>
        ///<returns></returns>
        public static ApiException Create(ApiException innerException, string url)
        {
            return new ApiException(innerException.Message, innerException.StatusCode, url, innerException);
        }

        /// <summary>
        /// </summary>
        /// <param name="innerException"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static ApiException Create(WebException innerException, string url)
        {
            HttpStatusCode statusCode = HttpStatusCode.Unknown;
            string message = innerException.Message;

            try
            {
                string responseText = innerException.Response.GetResponseStream().Text();
                Match match = Regex.Match(responseText,
                                          @"""error"":\s*{\s*""code"":\s*(?<code>\d+)\s*,\s*""message"":\s*""(?<message>.*)""\s*}",
                                          RegexOptions.ExplicitCapture);
                if (match.Success)
                {
                    statusCode = (HttpStatusCode) int.Parse(match.Groups["code"].Value.Trim());
                    message = match.Groups["message"].Value.Trim();
                }
            }
            catch
            {
                // not sure there is any thing to do here. We are already in an exception
                // state.
            }
            return new ApiException(message, statusCode, url, innerException);
        }
    }
}