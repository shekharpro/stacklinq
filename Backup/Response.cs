using System.Collections.Generic;
using System.ComponentModel;

namespace Soapi.Responses
{

public interface IPagedResponse<TResponse> where TResponse : Response, new()
{
    int Page { get; set; }
    int Pagesize { get; set; }
    int Total { get; set; }
    List<TResponse> Items { get; set; }
}

    public class Response : INotifyPropertyChanged
    {
        public Response()
        {

        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}