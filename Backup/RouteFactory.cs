//  
//  Project: SOAPI
//  http://soapi.codeplex.com
//  
//  Copyright 2010, Sky Sanders
//  Dual licensed under the MIT or GPL Version 2 licenses.
//  http://soapi.codeplex.com/license
//  
//  Date: June 4 2010 
//  
//  

#if SILVERLIGHT
using System.Net;
using System.Net.Browser;
#endif

namespace Soapi
{
    ///<summary>
    /// Builds Routes
    ///</summary>
    public partial class RouteFactory
    {
        ///<summary>
        /// The api version to call. Typically set by generated code. e.g. '0.8'
        ///</summary>
        public static string ApiVersion { get; set; }

        ///<summary>
        /// The maximum number of requests that can be made per day using this ApiKey
        ///</summary>
        public static int MaximumRequests { get; internal set; }
        ///<summary>
        /// The number of request remaining in daily rate limit
        ///</summary>
        public static int RemainingRequests { get; internal set; }

#if SILVERLIGHT
        public static int DefaultTimeout { get; set; }        
#else
        ///<summary>
        /// User agent to set on the outgoing api call
        ///</summary>
        public static string UserAgent { get; set; }

#endif

        static RouteFactory()
        {
#if SILVERLIGHT
            //http://stackoverflow.com/questions/1278973/retrieve-response-headers-in-silverlight
            bool httpResult = WebRequest.RegisterPrefix("http://", WebRequestCreator.ClientHttp);
            DefaultTimeout = 10000;
#else


            UserAgent = "Soapi-CS";

#endif

            // TODO: move into codegen for client generated part
            ApiVersion = "0.8";
        }

        ///<summary>
        ///</summary>
        ///<param name="target">e.g. 'api.stackoverflow.com'</param>
        ///<param name="apiKey"></param>
        public RouteFactory(string target, string apiKey)
        {
            Target = target;
            ApiKey = apiKey;
        }

        ///<summary>
        /// The target api. e.g. 'api.stackoverflow.com', to use when this instance constructs RouteMaps
        ///</summary>
        public string Target { get; set; }

        ///<summary>
        /// The api key to use when this instance constructs RouteMaps
        ///</summary>
        public string ApiKey { get; set; }
    }
}