﻿//  
//  Project: SOAPI
//  http://soapi.info
// 
//  Copyright 2010, Sky Sanders
//  Dual licensed under the MIT or GPL Version 2 licenses.
//  http://soapi.codeplex.com/license
//  
//  Date: June 10 2010 
//  
//  
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Soapi;

namespace Soapi
{
    /// <summary>
    /// <para>ObjectStore provides a facility to persist generic lists of objects in IsolatedStorage.</para>
    /// <para>ObjectStore uses JSON serialization and Zip compression.</para>
    /// <para>In that the persisted lists are IEnumerable, they are Linq queryable providing a limited, ad-hoc local object database.</para>
    /// <para>ObjectStore uses an in-memory cache</para>
    /// </summary>
    /// <typeparam name="T">Any JSON serializable type with a public parameterless constructor</typeparam>
    public class ObjectStore<T> where T : new()
    {
        private static readonly string Filename = TypeName + ".json";
        private static readonly object Lock = new object();
        private static readonly string TypeName = typeof (T).Name;
        private static List<T> _cache;

        /// <summary>
        /// Reads and decompresses the storage file from IsolatedStorage and JSON deserializes it into the original collection.
        /// </summary>
        /// <param name="key">Is used to construct the storage file name. Allows for persistance of multiple collections of the same type.</param>
        /// <returns></returns>
        public static List<T> Load(string key)
        {
            if (_cache == null)
            {
                lock (Lock)
                {
                    if (_cache == null)
                    {
                        using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                        {
                            string fileName = String.Format("{0}.{1}", key, Filename);

                            if (isf.GetFileNames(fileName).Length==0)
                            {
                                _cache = new List<T>();
                            }
                            else
                            {
                                using (var stream = new IsolatedStorageFileStream(fileName,FileMode.Open, isf))
                                {
                                    using (ZipInputStream cstream = new ZipInputStream(stream))
                                    {
                                        cstream.GetNextEntry();
                                        using (StreamReader reader = new StreamReader(cstream))
                                        {
                                            _cache =
                                                (List<T>)
                                                JsonConvert.DeserializeObject(reader.ReadToEnd(), typeof (List<T>),
                                                                              new UnixDateTimeConverter(),
                                                                              new StringEnumConverter());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return _cache;
        }

        /// <summary>
        /// JSON serializes the collection and persists the Zip compressed file to local storage.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="key">The key used when persisting the collection.</param>
        public static void Save(List<T> collection, string key)
        {
            lock (Lock)
            {
                _cache = collection;
                string fileName = String.Format("{0}.{1}", key, Filename);
                using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (var stream = new IsolatedStorageFileStream(fileName, FileMode.Create,store))
                    {
                        using (ZipOutputStream cstream = new ZipOutputStream(stream))
                        {
                            cstream.SetLevel(9);
                            cstream.PutNextEntry(new ZipEntry(fileName));
                            using (StreamWriter writer = new StreamWriter(cstream))
                            {
                                writer.Write(JsonConvert.SerializeObject(collection, new UnixDateTimeConverter(), new StringEnumConverter()));
                            }
                        }
                    }
                }
            }
        }
    }
}