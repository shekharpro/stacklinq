using System.Collections.Generic;
using System.Linq;
using Soapi.Parameters;
using Soapi.Responses;

namespace Soapi.Routes
{
    public abstract class RouteWithKey<TParams, TResult> : Route<TParams, TResult>
        where TParams : Parameter, new()
        where TResult : Response, new()
    {
        protected RouteWithKey(string target, string apiKey, params object[] keys)
            : base(target, apiKey)
        {
            SetKeyParam(keys);
        }

        protected RouteWithKey(string target, string apiKey, TParams parameters, params object[] keys)
            : base(target, apiKey, parameters)
        {

            SetKeyParam(keys);
        }


        private void SetKeyParam(params object[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                var keyProp = typeof(TParams).GetProperties().First(
                    p =>
                    p.PropertyType == typeof(List<string>) && p.GetCustomAttributes(typeof(RequiredAttribute), true).Length != 0);

                keyProp.SetValue(Parameters, ids.Select(i => i.ToString()).ToList(), null);
            }

        }
    }
}