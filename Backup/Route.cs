//  
//  Project: SOAPI
//  http://soapi.codeplex.com
//  
//  Copyright 2010, Sky Sanders
//  Dual licensed under the MIT or GPL Version 2 licenses.
//  http://soapi.codeplex.com/license
//  
//  Date: June 4 2010 
//  
//  
using Soapi.Parameters;
using Soapi.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
#if !SILVERLIGHT
using System.Web;
#else
#if !PHONE7
using System.Windows.Browser;
#endif
#endif

// good post r.e. silverlight httpwebrequest with good comments
// http://www.cameronalbert.com/post/2008/03/HttpWebRequest-Helper-for-Silverlight-2.aspx

namespace Soapi.Routes
{
    ///<summary>
    /// Abstract base class for all RouteMaps
    ///</summary>
    ///<typeparam name="TParams"></typeparam>
    ///<typeparam name="TResult"></typeparam>
    public abstract class Route<TParams, TResult>
        where TParams : Parameter, new()
        where TResult : Response, new()
    {

        private string _lastResult;

        private string _lastUrl;

        protected Route(string target, string apiKey, TParams parameters)
        {
            Target = target;
            ApiKey = apiKey;
            Parameters = parameters;
        }

        protected Route(string target, string apiKey)
            : this(target, apiKey, new TParams())
        {
        }



        ///<summary>
        /// Adds type=jsontext to api call
        ///</summary>
        public bool JsonText { get; set; }

        ///<summary>
        /// JSON text of last successful call
        ///</summary>
        public string LastResult
        {
            get { return _lastResult; }
        }

        ///<summary>
        /// The url of the last call. Useful for diagnosis.
        ///</summary>
        public string LastUrl
        {
            get { return _lastUrl; }
        }

        ///<summary>
        /// The constructed path for this route and it's parameters
        ///</summary>
        public string Path
        {
            get
            {
                string path = BuildPath();
                string keyParam = string.IsNullOrEmpty(ApiKey)
                                      ? ""
                                      : (path.EndsWith("?") ? "" : "&") + "key=" +
                                        HttpUtility.UrlEncode(ApiKey);

                return String.Format("{0}{1}", path, keyParam);
            }
        }

        ///<summary>
        ///</summary>
        public TParams Parameters { get; set; }

        protected abstract string RouteFormat { get; }

        ///<summary>
        /// The api target site. e.g. 'api.stackoverflow.com'. Required.
        ///</summary>
        public string Target { get; set; }

        /// <summary>
        /// The api key to supply to the call. Optional.
        /// </summary>
        public string ApiKey { get; set; }


        protected string BuildBaseUrl(string target)
        {
            Uri uriOut;
            if (string.IsNullOrEmpty(target))
            {
                throw new ApiException("target cannot be null. an example of a valid target is 'stackoverflow' or 'meta.stackoverflow'");
            }
            var url = Utilities.NormalizeUrl("http://" + target, RouteFactory.ApiVersion);
            if (!Uri.TryCreate(url, UriKind.Absolute, out uriOut))
            {
                throw new ApiException("Invalid target");
            }
            string result = uriOut.ToString();
            return result;
        }

        ///<summary>
        /// Initiates an asyncronous api call.
        ///</summary>
        ///<param name="success">The action to invoke upon success of api call.</param>
        ///<param name="error">The action to invoke upon failure of api call.</param>
        ///<param name="context">User defined context object.</param>
        ///<exception cref="ApiException"></exception>
        public void GetResponseAsync(Action<ResponseState<TResult>> success, Action<ResponseState<TResult>> error, object context)
        {
            _lastUrl = Utilities.NormalizeUrl(BuildBaseUrl(Target), Path.TrimStart('/'));

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_lastUrl);
                webRequest.Method = "GET";
#if SILVERLIGHT
                // no gzip if using client stack, no headers if using browser stack
                // we choose headers for now - is set in static API ctor
                webRequest.Method = "GET";
                webRequest.Accept = "*/*";


#else
                webRequest.UserAgent = RouteFactory.UserAgent;
                webRequest.AutomaticDecompression = DecompressionMethods.GZip;
                webRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
#endif
                webRequest.BeginGetResponse(ReadCallback, new ResponseState<TResult>
                    {
                        Context = context,
                        OnError = error,
                        OnSuccess = success,
                        Request = webRequest
                    });
            }
            catch (Exception e)
            {
                throw ApiException.Create(e, _lastResult);
            }

        }
        ///<summary>
        /// Initiates an asyncronous api call.
        ///</summary>
        ///<exception cref="ApiException"></exception>
        public void GetResponseAsync()
        {
            GetResponseAsync(null);
        }

        ///<summary>
        /// Initiates an asyncronous api call.
        ///</summary>
        ///<param name="context">User defined context object.</param>
        ///<exception cref="ApiException"></exception>
        public void GetResponseAsync(object context)
        {
            GetResponseAsync(null, null, context);
        }

        ///<summary>
        /// Initiates an asyncronous api call.
        ///</summary>
        ///<param name="success">The action to invoke upon success of api call. </param>
        ///<param name="error">The action to invoke upon failure of api call.  </param>
        ///<exception cref="ApiException"></exception>
        public void GetResponseAsync(Action<ResponseState<TResult>> success, Action<ResponseState<TResult>> error)
        {
            GetResponseAsync(success, error, null);
        }




#if SILVERLIGHT

        public TResult GetResponse()
        {

            return GetResponse(RouteFactory.DefaultTimeout);
        }

        public TResult GetResponse(int timeout)
        {
            _lastUrl = Utilities.NormalizeUrl(BuildBaseUrl(Target), Path.TrimStart('/'));
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_lastUrl);

                webRequest.Method = "GET";
                webRequest.Accept = "*/*";





                using (WebResponse response = webRequest.GetResponse(timeout))
                {
                    _lastResult = response.GetResponseStream().Text();

                    ParseRateLimit(response);

                    return ObjectFromJson(_lastResult);
                }
            }
            catch (Exception e)
            {

                throw ApiException.Create(e, _lastUrl);
            }
        }
#endif


#if !SILVERLIGHT

        /// <returns></returns>
        public TResult GetResponse()
        {
            _lastUrl = Utilities.NormalizeUrl(BuildBaseUrl(Target), Path.TrimStart('/'));
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_lastUrl);

                webRequest.Method = "GET";
                webRequest.UserAgent = RouteFactory.UserAgent;
                webRequest.AutomaticDecompression = DecompressionMethods.GZip;
                webRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");

                using (WebResponse response = webRequest.GetResponse())
                {
                    _lastResult = response.GetResponseStream().Text();

                    ParseRateLimit(response);

                    return ObjectFromJson(_lastResult);
                }
            }
            catch (Exception e)
            {

                throw ApiException.Create(e, _lastUrl);
            }
        }

#endif


        /// <summary>
        /// TODO: maybe json.net can do this for me
        /// </summary>
        /// <returns></returns>
        private string BuildPath()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (JsonText)
            {
                parameters.Add("type", "jsontext");
            }

            string route = RouteFormat;

            PropertyInfo[] props = Parameters.GetType().GetProperties();

            if (props != null)
            {
                foreach (PropertyInfo pinfo in props)
                {

                    object value = pinfo.GetValue(Parameters, null);

                    if (value == null)
                    {
                        continue;
                    }

                    Type ptype = pinfo.PropertyType;

                    string stringValue = null;

                    if (ptype == typeof(int))
                    {
                        if ((int)value == 0)
                        {
                            continue;
                        }
                        stringValue = ((int)value).ToString();
                    }
                    else if (ptype == typeof(long))
                    {
                        if ((long)value == 0)
                        {
                            continue;
                        }
                        stringValue = ((long)value).ToString();
                    }
                    else if (ptype == typeof(bool))
                    {
                        if (!((bool)value))
                        {
                            continue;
                        }

                        stringValue = "true";
                    }

                    else if (ptype == typeof(string))
                    {
                        if (string.IsNullOrEmpty(value as string))
                        {
                            continue;
                        }
                        stringValue = value as string;
                    }
                    else if (ptype == typeof(Guid))
                    {
                        if ((Guid)value == Guid.Empty)
                        {
                            continue;
                        }
                        stringValue = ((Guid)value).ToString();
                    }
                    else if (ptype == typeof(DateTime))
                    {
                        if ((DateTime)value == DateTime.MinValue)
                        {
                            continue;
                        }

                        // does this need to go to universal time in ToUnixTime?
                        stringValue = ((DateTime)value).ToUnixTime().ToString();
                    }
                    //else if (ptype == typeof(string[]))
                    //{
                    //    if (((string[])value).Length == 0)
                    //    {
                    //        continue;
                    //    }
                    //}
                    else if (ptype == typeof(List<string>))
                    {
                        if (((List<string>)value).Count == 0)
                        {
                            continue;
                        }
                    }
                    else if (ptype.IsEnum)
                    {
                        if ((int)value == 0)
                        {
                            continue;
                        }
                        stringValue = value.ToString();
                    }
                    else
                    {
                        throw new ArgumentException(String.Format("{0} is not supported", ptype.FullName));
                    }

                    string encodedValue;

                    if (value.GetType() == typeof(List<string>))
                    {
                        List<string> values = new List<string>();

                        foreach (string item in ((List<string>)value))
                        {
                            values.Add(HttpUtility.UrlEncode(item));
                        }
                        encodedValue = string.Join(";", values.ToArray());
                    }
                    else
                    {
                        encodedValue = HttpUtility.UrlEncode(stringValue);
                    }

                    var jprop = pinfo.GetCustomAttribute<JsonPropertyAttribute>().PropertyName;

                    string token = string.Format("{{{0}}}", jprop);

                    if (route.IndexOf(token) > -1)
                    {
                        route = route.Replace(token, encodedValue);
                    }
                    else
                    {
                        parameters.Add(jprop, encodedValue);
                    }
                }
            }

            return string.Format("{0}?{1}", route, parameters.ToQueryString());
        }

        private static TResult ObjectFromJson(string json)
        {
            using (StringReader reader = new StringReader(json))
            {
                return (TResult)new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ObjectCreationHandling = ObjectCreationHandling.Replace,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }.Deserialize(new JsonTextReader(reader), typeof(TResult));
            }
        }

        private static void ParseRateLimit(WebResponse response)
        {


            if (response.Headers != null)
            {
                if (response.Headers.AllKeys.Contains("X-RateLimit-Current"))
                {
                    RouteFactory.RemainingRequests = Int32.Parse(response.Headers["X-RateLimit-Current"]);
                }
                if (response.Headers.AllKeys.Contains("X-RateLimit-Max"))
                {
                    RouteFactory.MaximumRequests = Int32.Parse(response.Headers["X-RateLimit-Max"]);
                }
            }

        }


        private void ReadCallback(IAsyncResult asynchronousResult)
        {
            var state = (ResponseState<TResult>)asynchronousResult.AsyncState;

            try
            {
                using (WebResponse response = state.Request.EndGetResponse(asynchronousResult))
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        _lastResult = stream.Text();
                    }

                    ParseRateLimit(response);

                    state.Response = ObjectFromJson(_lastResult);

                    OnEndResponse(new EndResponseEventArgs<TResult>(state));

                    if (state.OnSuccess != null)
                    {

                        state.OnSuccess(state);
                    }

                }
            }
            catch (Exception e)
            {
                ApiException apiException = ApiException.Create(e);
                state.Exception = apiException;
                OnEndResponse(new EndResponseEventArgs<TResult>(state));

                if (state.OnError != null)
                {
                    state.OnError(state);
                }
            }
        }


        ///<summary>
        /// Fires upon completion of an asynchronous api call. Check event args Exception property.
        ///</summary>
        public event EventHandler<EndResponseEventArgs<TResult>> EndResponse;

        protected virtual void OnEndResponse(EndResponseEventArgs<TResult> e)
        {
            EventHandler<EndResponseEventArgs<TResult>> response = EndResponse;
            if (response != null) response(this, e);
        }
    }
}