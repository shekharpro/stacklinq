//  
//  Project: SOAPI
//  http://soapi.info
// 
//  Copyright 2010, Sky Sanders
//  Dual licensed under the MIT or GPL Version 2 licenses.
//  http://soapi.codeplex.com/license
//  
//  Date: June 10 2010 
//  
//  
using System;
using System.Net;
using Soapi.Responses;

namespace Soapi
{
    /// <summary>
    /// Encapsulates components of an asynchronous request.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class ResponseState<TResult> where TResult : Response, new()
    {
        ///<summary>
        /// <para>An arbitrary object field that may be used to provide context for use when handling the event.</para>
        /// <para>An example would be a centralized handler that determines action from the context object</para>
        ///</summary>
        public object Context { get; set; }

        ///<summary>
        /// The exception, if any, that was thrown during this request.
        ///</summary>
        public ApiException Exception { get; internal set; }

        internal Action<ResponseState<TResult>> OnError;
        internal Action<ResponseState<TResult>> OnSuccess;

        ///<summary>
        /// The request that raised the event
        ///</summary>
        public HttpWebRequest Request { get; internal set; }

        ///<summary>
        /// The response, if any, from the api call.
        ///</summary>
        public TResult Response { get; internal set; }
    }
}