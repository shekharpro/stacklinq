﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
#if SILVERLIGHT
using System.Threading;
#if !PHONE7
using System.Windows.Browser;
#endif
#else
using System.Web;
#endif


namespace Soapi
{
#if SILVERLIGHT
    internal static class RequestHelper
    {
        /// <summary>
        /// A blocking operation that does not continue until a response has been
        /// received for a given <see cref="HttpWebRequest"/>, or the request
        /// timed out.
        /// </summary>
        /// <param name="request">The request to be sent.</param>
        /// <param name="timeout">An optional timeout.</param>
        /// <returns>The response that was received for the request.</returns>
        /// <exception cref="TimeoutException">If the <paramref name="timeout"/>
        /// parameter was set, and no response was received within the specified
        /// time.</exception>
        public static HttpWebResponse GetResponse(this HttpWebRequest request, int? timeout)
        {
            if (request == null) throw new ArgumentNullException("request");

            if (System.Windows.Deployment.Current.Dispatcher.CheckAccess())
            {
                const string msg = "Invoking this method on the UI thread is forbidden.";
                throw new InvalidOperationException(msg);
            }

            AutoResetEvent waitHandle = new AutoResetEvent(false);
            HttpWebResponse response = null;
            Exception exception = null;

            AsyncCallback callback = ar =>
            {
                try
                {
                    //get the response
                    response = (HttpWebResponse)request.EndGetResponse(ar);
                }
                catch (Exception e)
                {
                    exception = e;
                }
                finally
                {
                    //setting the handle unblocks the loop below
                    waitHandle.Set();
                }
            };

            //request response async
            var asyncResult = request.BeginGetResponse(callback, null);
            if (asyncResult.CompletedSynchronously) return response;

            bool hasSignal = waitHandle.WaitOne(timeout ?? Timeout.Infinite);
            if (!hasSignal)
            {
                throw new TimeoutException("No response received in time.");
            }

            //bubble exception that occurred on worker thread
            if (exception != null) throw exception;

            return response;
        }
    }

#endif

    internal static class Utilities
    {
#if SILVERLIGHT
        /// <summary>
        /// Combine the RootUrl of the running web application with the relative url
        /// specified.
        /// TODO: handle relative path prefixes on relativeUrl
        /// </summary>
        /// <param name="rootUrl"></param>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string NormalizeUrl(string rootUrl, string relativeUrl)
        {
            relativeUrl = relativeUrl.TrimStart(new[] { '/' });

            if (!rootUrl.EndsWith("/"))
            {
                rootUrl += "/";
            }
            return new Uri(rootUrl + relativeUrl).ToString();
        }

#else

        /// <summary>
        /// TODO: this is a candidate for replacing NetworkUtils.NormalizeUrl
        /// This method will recognize rooted relatives and return expected results
        /// e.g. abs is culled from the page source or elsewhere and is not rooted
        /// abs = http://foo.com/dir/this/dir
        /// rel = /dir/this/foo.htm
        /// ret = http://foo.com/dir/this/foo.htm
        /// </summary>
        /// <param name="absoluteUrl"></param>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string NormalizeUrl(string absoluteUrl, string relativeUrl)
        {
            Uri returnValue;

            Uri absoluteUri = new Uri(absoluteUrl, UriKind.Absolute);

            if (string.IsNullOrEmpty(relativeUrl))
            {
                returnValue = absoluteUri;
            }
            else
            {
                returnValue = new Uri(relativeUrl, UriKind.RelativeOrAbsolute);

                if (!returnValue.IsAbsoluteUri)
                {
                    if (returnValue.ToString().StartsWith("/"))
                    {
                        // is rooted
                        returnValue = new Uri(absoluteUri.GetLeftPart(UriPartial.Authority) + returnValue);
                    }
                    else if (returnValue.ToString().StartsWith("./"))
                    {
                        // not likely but could happen
                        returnValue = new Uri(absoluteUri + returnValue.ToString());
                    }
                    else if (returnValue.ToString().StartsWith("../"))
                    {
                        // not likely but could happen and we need to do some parsing
                        // sooo...
                        throw new NotImplementedException("reverse relative path parsing");
                    }
                    else
                    {
                        // one of two things could be happening here...
                        // 1- the relative url is pointing to the same page as the absolute path
                        string absolutePath = new Uri(absoluteUri.GetLeftPart(UriPartial.Path)).ToString();

                        string[] segments = returnValue.ToString().Split(new[] { '?' }, 2);
                        string actionPath = segments[0];
                        string query = segments.Length > 1 ? segments[1] : string.Empty;

                        if (absolutePath.EndsWith(actionPath, StringComparison.InvariantCultureIgnoreCase))
                        {
                            returnValue = new Uri(String.Format("{0}?{1}", absolutePath, query));
                        }
                        else
                        {
                            // 2- the relative url is diving deeper
                            // just concatenate them

                            relativeUrl = relativeUrl.TrimStart(new[] { '/' });

                            if (!absoluteUrl.EndsWith("/"))
                            {
                                absoluteUrl += "/";
                            }

                            returnValue = new Uri(absoluteUrl + relativeUrl);
                        }
                    }
                }
            }
            return returnValue.ToString();
        }
#endif
        // <summary>
        /// <para>Truncates a DateTime to a specified resolution.</para>
        /// <para>A convenient source for resolution is TimeSpan.TicksPerXXXX constants.</para>
        /// </summary>
        /// <param name="date">The DateTime object to truncate</param>
        /// <param name="resolution">e.g. to round to nearest second, TimeSpan.TicksPerSecond</param>
        /// <returns>Truncated DateTime</returns>
        public static DateTime Truncate(this DateTime date, long resolution)
        {
            return new DateTime(date.Ticks - (date.Ticks % resolution), date.Kind);
        }

        /// <summary>
        /// Reads stream into a string, which is returned.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static string Text(this Stream stream)
        {
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            return reader.ReadToEnd();
        }

        /// <summary>
        /// Constructs a QueryString (string).
        /// Consider this method to be the opposite of "System.Web.HttpUtility.ParseQueryString"
        /// </summary>
        /// <param name="parameters">NameValueCollection</param>
        /// <returns>String</returns>
        public static String ToQueryString(this Dictionary<string, string> parameters)
        {
            List<String> items = new List<String>();

            foreach (var kvp in parameters)
            {
                items.Add(String.Concat(kvp.Key, "=", HttpUtility.UrlEncode(kvp.Value)));
            }

            return String.Join("&", items.ToArray());
        }
        /// <summary>
        /// Convert a long into a DateTime
        /// Need to double check that UTC is not required
        /// </summary>
        public static DateTime FromUnixTime(this Int64 self)
        {
            DateTime ret = new DateTime(1970, 1, 1);
            return ret.AddSeconds(self);
        }

        /// <summary>
        /// Convert a DateTime into a long
        /// Need to double check that UTC is not required
        /// </summary>
        public static Int64 ToUnixTime(this DateTime self)
        {
            DateTime epoc = new DateTime(1970, 1, 1);
            TimeSpan delta = self - epoc;

            if (delta.TotalSeconds < 0) throw new ArgumentOutOfRangeException("Unix epoc starts January 1st, 1970");

            return (long)delta.TotalSeconds;
        }


    }

    internal static class AttributeHelpers
    {
        /// <summary>
        /// Returns first non-inherited custom attribute of type T
        /// </summary>
        public static T GetCustomAttribute<T>(this ICustomAttributeProvider provider)
            where T : Attribute
        {
            return GetCustomAttribute<T>(provider, false);
        }

        /// <summary>
        /// Returns first custom attribute of type T in the inheritance chain
        /// </summary>
        public static T GetCustomAttribute<T>(this ICustomAttributeProvider provider, bool inherited)
            where T : Attribute
        {
            return provider.GetCustomAttributes<T>(inherited).FirstOrDefault();
        }

        /// <summary>
        /// Returns all non-inherited custom attributes of type T
        /// </summary>
        public static List<T> GetCustomAttributes<T>(this ICustomAttributeProvider provider)
            where T : Attribute
        {
            return GetCustomAttributes<T>(provider, false);
        }

        /// <summary>
        /// Returns all custom attributes of type T in the inheritance chain
        /// </summary>
        public static List<T> GetCustomAttributes<T>(this ICustomAttributeProvider provider, bool inherited)
            where T : Attribute
        {
            return provider.GetCustomAttributes(typeof(T), inherited).Cast<T>().ToList();
        }
    }
}
