//  
//  Project: SOAPI
//  http://soapi.info
// 
//  Copyright 2010, Sky Sanders
//  Dual licensed under the MIT or GPL Version 2 licenses.
//  http://soapi.codeplex.com/license
//  
//  Date: June 10 2010 
//  
//  
using System;
using Soapi.Responses;

namespace Soapi
{
    ///<summary>
    ///</summary>
    ///<typeparam name="TResult"></typeparam>
    public class EndResponseEventArgs<TResult> : EventArgs where TResult : Response, new()
    {
        ///<summary>
        ///</summary>
        public ResponseState<TResult> ResponseState;

        ///<summary>
        ///</summary>
        ///<param name="responseState"></param>
        public EndResponseEventArgs(ResponseState<TResult> responseState)
        {
            ResponseState = responseState;
        }
    }
}