using System;

namespace Soapi.Parameters
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredAttribute:Attribute
    {
        
    }
}