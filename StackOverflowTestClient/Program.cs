﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LINQToStackOverflow;

namespace StackOverflowTestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //TODO:
            //***Questions***
            //  Set Comments when IncludeComments = true                                                                                        DONE
            //  Set Tags array                                                                                                                  DONE
            //  Single-Question: include Answers, Comments                                                                                      DONE 
            //  Unanswered                                                                                                                      DONE
            //  Favorites                                                                                                                       DONE- sorting doesn't match site
            //  From/To Dates                                                                                                                   DONE
            //  SortOrder (order parameter)                                                                                                     DONE
            //  QueryByTag http://api.stackoverflow.com/{version}/questions/tagged                                                              DONE
            //
            //***Answers***
            //  By User         http://api.stackoverflow.com/{version}/users/{id}/answers                                                       DONE
            //  Single Answer   http://api.stackoverflow.com/{version}/answers/{id}                                                             DONE
            //***Badges***
            //  http://api.stackoverflow.com/{version}/badges - default is name                                                                 DONE
            //  http://api.stackoverflow.com/{version}/badges/name - badges listed by name                                                      DONE
            //  http://api.stackoverflow.com/{version}/badges/tags - tag based badges earned                                                    DONE
            //  http://api.stackoverflow.com/{version}/users/{id}/badges - badges awarded a user                                                DONE
            //***Comments***
            //  Comments created by a specific user {id}:                                                                                       DONE
            //  http://api.stackoverflow.com/{version}/users/{id}/comments                                                                      
            //  Comments created by a specific user {id} directed at {toid}:                                                                    DONE
            //  http://api.stackoverflow.com/{version}/users/{id}/comments/{toid}
            //  Single Comment Method - Topic Here                                                                                              DONE
            //  http://api.stackoverflow.com/{version}/comments/{id}
            //***Reputation***                                                                                                                  
            //  ByUser : http://api.stackoverflow.com/{version}/users/{id}/reputation                                                           DONE
            //***Stats***
            //  Stats                                                                                                                           DONE
            //***Tag***
            //  http://api.stackoverflow.com/{version}/tags                                                                                     DONE
            //  http://api.stackoverflow.com/{version}/users/{id}/tags - tags that a user {id} participated                                     DONE
            //***User***
            //  http://api.stackoverflow.com/{version}/users - default is reputation                            
            //  User Timeline Method - Topic Here   http://dev.meta.stackoverflow.com/questions/34670/user-timeline-api-method                  
            //  http://api.stackoverflow.com/{version}/users/{id}/timeline                                                                      DONE
            //  User @Mentions Method - Topic Here  http://dev.meta.stackoverflow.com/questions/34647/user-mentions-api-method
            //  http://api.stackoverflow.com/{version}/users/{id}/mentioned                                                                     DONE
            //  Single User Method - Topic Here
            //  http://api.stackoverflow.com/{version}/users/{id}                                                                               DONE
            //***ErrorCodes*** http://dev.meta.stackoverflow.com/questions/34868/api-error-codes                                                DONE
            //
            //***MISC***
            //  LINQToStackOverFlowQueryException (convert all)                                                                                 DONE
            //  Tests                                                                                                                           STARTED
            //  Vectorized Requests http://dev.meta.stackoverflow.com/questions/34940/vectorized-requests                                       NOT DONE
            //  "Embedded" URLs  http://dev.meta.stackoverflow.com/questions/34660/can-we-have-embedded-links-or-at-least-links-endpoints       NOT DONE
            //  Triggered error testing http://dev.meta.stackoverflow.com/questions/34944/method-to-trigger-error-response                      NOT DONE
            //  Related entities (Users.Badges, User.Tags)                                                                                      NOT DONE
            //  Questions Answers API http://api.stackoverflow.com/0.8/help/method?method=questions/{id}/answers                                NOT DONE
            //  Revisions                                                                                                                       NOT DONE
            //  Search                                                                                                                          NOT DONE
            //  Total/Page/Pagsize                                                                                                              NOT DONE


            //StackOverflowContext ctx = new StackOverflowContext("ombo9iKYFEGmgsexsK2C9A", new StackOverflowClient()
            //{
            //    ProxyAddress = "authproxy.ntl-city.com:8080",
            //    ProxyPassword = "@gent008",
            //    ProxyUserName = "XDHSG3S"
            //}); 

            var site = (from s in  StackOverflowContext.Sites select s).ToList().Where(x=>x.Name=="Stack Overflow").First();
            StackOverflowContext ctx = new StackOverflowContext("ombo9iKYFEGmgsexsK2C9A", site);  


            //***Questions***
            var questions = from q in ctx.Questions
                            where q.OwnerID == 34796
                            select q;

            

            var questionbyId = from q in ctx.Questions
                               where q.QuestionID == 369558
                               select q;

            var multiQuestion = from q in ctx.Questions
                                where q.QuestionIDList == new[] { 369558, 936804 }
                                select q;

            var favorites = from f in ctx.Questions
                            where f.OwnerID == 34796 && f.Favorite == true && f.SortBy == QuestionSortType.UserActivity && f.Min == DateTime.Parse("7/1/2010")
                            select f;

            //***QuestionTimeline***
            var timelines = from qt in ctx.QuestionTimelines
                            where qt.PostID == 369558 && qt.PageSize == 10
                            select qt;

            //timelines
            

            //***Answers***
            //By user
            //multiple userid: new int[] {22656, 34796}
            var answers = from a in ctx.Answers
                          where a.OwnerID == 34796 && a.IncludeBody == true
                          select a;

            //By ID
            //multiple answerID: new int[] { 369677, 247623 }
            var answersbyID = from a in ctx.Answers
                              where a.AnswerID == 369677
                              select a;

            //By QuestionID
            //multi: new int[] { 369558, 936804 }
            var answersbyQuestionID = from a in ctx.Answers
                                      where a.QuestionID == 369558 && a.IncludeBody == true
                                      select a;

            //***Badges***
            var badges = from b in ctx.Badges
                         select b;



            //***Comments***
            //multiple ownerID = new int[] {22656, 34796}
            var comments = from c in ctx.Comments
                           where c.OwnerID == 34796
                           select c;

            //By Question
            //multi: 
            var commentsByQuestion = from c in ctx.Comments
                                     where c.QuestionID == 936804
                                     select c;

            //By Answer
            var commentsByAnswer = from c in ctx.Comments
                                   where c.AnswerIDList == new int[] { 2962188, 2962182 }
                                   select c;

            //By Multiple Posts
            var commentsByPost = from c in ctx.Comments
                                 where c.PostIDList == new int[] { 936804, 952777 }
                                 select c;


            //multiple commentID: new int[] {3074549, 2654189}
            var commentByID = from c in ctx.Comments
                              where c.CommentIDList == new int[] { 3074549, 2654189 }
                              select c;

            //multiple ownerID = new int[] {22656, 34796}
            var commentToUser = from c in ctx.Comments
                                where c.ToID == 34813 && c.OwnerID == 22656
                                select c;

            //***Reputation***
            //multiple ownerID = new int[] {22656, 34796}
            var rep = from r in ctx.Reputation
                      where r.UserIDList == new int[] { 22656, 34796 }
                      select r;

            //***Statistics***
            var stats = from s in ctx.Statistics
                        select s;

            //***Users***
            //multiple ownerID = new int[] {22656, 34796}
            var users = from u in ctx.Users
                        where u.UserID == 0
                        select u;
            
          
            
            var userbyBadge = from u in ctx.Users
                              where u.BadgeIDList == new int[] {204, 41}
                              select u;

            var userTimeline = from u in ctx.UserTimeline
                               where u.UserIDList == new int[] { 22656, 34796 }
                               select u;

        

            var userMentions = from u in ctx.UserMentions
                               where u.OwnerIDList == new int[] { 22656, 34796 }
                               select u;

            var moderators = from u in ctx.Users
                             where u.Moderator == true && u.PageSize == 100 && u.Page == 5
                             select u;

                              
            //***Tags***
            var tags = from t in ctx.Tags
                       where t.UserID == 34796
                       select t;

            var alltags = from t in ctx.Tags
                          select t;

            //***Revisions***
            var revisions = from r in ctx.Revisions
                            where r.PostID == 9033
                            select r;

            //***AssociatedUsers***
            var associatedUsers = from u in StackOverflowContext.AssociatedUsers
                                  where u.AssociationID == new Guid("3f9f8ab9-c46c-47f9-8173-474ec787fb00")
                                  select u;
                                  

            //***Sites***
            //var sites = from s in StackOverflowContext.Sites
            //            select s;

            //Console.WriteLine("***Sites***");
            //foreach (Site s in sites)
            //{
            //    Console.WriteLine(s.Name);
            //}

            Console.WriteLine("***AssociatedUsers:***");
            foreach (AssociatedUser u in associatedUsers)
            {
                Console.WriteLine(u.DisplayName + ", " + u.UserID.ToString());
            }

            //Console.WriteLine("***Revisions:***");
            //foreach (Revision r in revisions)
            //{
            //    Console.WriteLine(r.Title + ": " + r.LastTitle);
            //}

            //Console.WriteLine("***Questions:***");
            //foreach (Question q in questions)
            //{
            //    Console.WriteLine(q.Title);
            //}

            //foreach (Question q in favorites)
            //{
            //    Console.WriteLine(q.Title);
            //}


            //Console.WriteLine("_BY ID_");
            //foreach (Question q in questionbyId)
            //{
            //    Console.WriteLine(q.Title);
            //}
            //Console.WriteLine("_VECTORIZED_");
            //foreach (Question q in multiQuestion)
            //{
            //    Console.WriteLine(q.Title);
            //}

            //Console.WriteLine("***QuestionsTimelines:***");
            //foreach (QuestionTimeline qt in timelines)
            //{
            //    Console.WriteLine(qt.TimelineType.ToString());
            //}

            //Console.WriteLine("***Answers:***");
            //foreach (Answer a in answers)
            //{
            //    Console.WriteLine(a.Title);
            //}

            //foreach (Answer a in answersbyQuestionID)
            //{
            //    Console.WriteLine(a.Title);
            //}

            //Console.WriteLine("ByID:");
            //foreach (Answer a in answersbyID)
            //{
            //    Console.WriteLine(a.Title);
            //}

            //Console.WriteLine("***Badges:***");
            //foreach (Badge b in badges)
            //{
            //    Console.WriteLine(b.Name);
            //}



            //Console.WriteLine("***Comments:***");
            //foreach (Comment c in comments)
            //{
            //    Console.WriteLine(c.Body);
            //}

            //foreach (Comment c in commentsByQuestion)
            //{
            //    Console.WriteLine(c.Body);
            //}

            //foreach (Comment c in commentsByAnswer)
            //{
            //    Console.WriteLine(c.Body);
            //}

            //foreach (Comment c in commentsByPost)
            //{
            //    Console.WriteLine(c.Body);
            //}

            //Console.WriteLine("***Comment By ID:***");
            //foreach (Comment c in commentByID)
            //{
            //    Console.WriteLine(c.Body);
            //}

            //Console.WriteLine("***Comment To ID:***");
            //foreach (Comment c in commentToUser)
            //{
            //    Console.WriteLine(c.Body);
            //}

            //Console.WriteLine("***Reputation:***");
            //foreach (Reputation r in rep)
            //{
            //    Console.WriteLine(r.Title);
            //    Console.WriteLine(string.Format("{0}PositiveRep: {1}", (char)9, r.PositiveRep.ToString()));
            //    Console.WriteLine(string.Format("{0}NegativeRep: {1}", (char)9, r.NegativeRep.ToString()));
            //}

            //Console.WriteLine("***Statistics:***");
            //foreach (Statistics s in stats)
            //{
            //    Console.WriteLine(s.TotalAnswers.ToString());
            //}

            //Console.WriteLine("***Users:***");
            //foreach (User u in moderators)
            //{
            //    Console.WriteLine(u.DisplayName + "," + u.Reputation);
            //}
            

            //foreach (User u in users)
            //{
            //    Console.WriteLine(u.DisplayName + "," + u.AssociationID.ToString());
            //}

            //foreach (User u in userbyBadge)
            //{
            //    Console.WriteLine(u.DisplayName);
            //}

            //foreach (UserTimeline u in userTimeline)
            //{
            //    Console.WriteLine(u.Action);
            //}

            //Console.WriteLine("***Tags:***");
            //foreach (Tag t in tags)
            //{
            //    Console.WriteLine(t.Name);
            //}

            //Console.WriteLine("***All Tags:***");
            //foreach (Tag t in alltags)
            //{
            //    Console.WriteLine(t.Name);
            //}

            //Console.WriteLine("***UserMentions***");
            //foreach (UserMentions u in userMentions)
            //{
            //    Console.WriteLine(u.Body);
            //}

            Console.WriteLine("Total: " + ctx.RequestTotal.ToString());
            Console.WriteLine("Page: " + ctx.RequestPage.ToString());
            Console.WriteLine("PageSize: " + ctx.RequestPageSize.ToString());
            Console.ReadLine();
        }
    }
}
